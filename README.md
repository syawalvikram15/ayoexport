#  Simple Calculator

## **Description**
Berikut adalah sebuah kalkulator sederhana yang dapat digunakan untuk melalakukan operasi perhitungan dasar seperti penjumlahan, pengurangan, perkalian dan pembagian.



## **Requirements**
- PHP 5.6 
- Apache


## **Installation**
- Simpan semua file di /var/www/data/html
- composer install
- php vendor/kenjis/ci-phpunit-test/install.php


## **Runing Unit Test**
- cd application/tests
-  ../../vendor/bin/phpunit -c phpunit_pl.xml
- hasilnya clover.xml di document root


## **Environment Variable**
- DB_USER
- DB_PASS
- DB_HOST
- DATABASE

Title
Badges
Description
Requirements
Installation
Usage
Credits (Optional)
 