<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//$route['default_controller'] = 'welcome';
//$route['404_override'] = '';
//$route['translate_uri_dashes'] = FALSE;
//$route['default_controller'] = 'pages/view';
$route['default_controller'] = 'home';

$route['api/form-input-khusus/get-nohp'] = 'forminputkhusus/get_nohp';
$route['api/form-input-khusus/cek-data-tabel-update'] = 'forminputkhusus/cek_tabelUpdate';
$route['api/form-input-khusus/update'] = 'forminputkhusus/update';
$route['api/form-input-khusus/get-data-table'] = 'forminputkhusus/get_dataTable';

$route['api/tracking-user/get-profile'] = 'trackinguser/get_profile';
$route['api/tracking-user/get-data-table'] = 'trackinguser/get_data_table';
$route['api/tracking-user/get-data-table-mac'] = 'trackinguser/get_data_table_mac';
$route['api/tracking-user/get-kelurahan'] = 'trackinguser/get_kelurahan';
$route['api/tracking-user/get-zona'] = 'trackinguser/get_zona'; 
$route['api/tracking-user/get-latlong'] = 'trackinguser/get_latlong';


$route['api/form-input-suspect/get-data-table'] = 'forminputsuspect/get_dataTable';
$route['api/form-input-suspect/cek-data-tabel-update'] = 'forminputsuspect/cek_tabelUpdate';
$route['api/form-input-suspect/update'] = 'forminputsuspect/update';
$route['api/form-input-suspect/form'] = 'forminputsuspect/form';
$route['api/form-input-suspect/import'] = 'forminputsuspect/import';

$route['api/tracking-odp-kemenkes/get-data-table'] = 'trackingodpkemenkes/get_dataTable';