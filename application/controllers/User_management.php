<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_management extends CI_Controller
{
    public function get_data_user()
    {
        $this->load->model('Usermanagement_model');
        $data = $this->Usermanagement_model->get_data_user();
        echo json_encode($data);
    }

    public function check_username()
    {
        $this->load->model('Usermanagement_model');
        $username = $this->input->post('username');
        $res = $this->Usermanagement_model->check_username($username);
        echo json_encode($res);
    }

    public function check_data_username()
    {
        $this->load->model('Usermanagement_model');
        $username = $this->input->post('username');
        $res = $this->Usermanagement_model->check_data_username($username);
        echo json_encode($res);
    }

    public function get_bumn()
    {
        $this->load->model('Usermanagement_model');
        $username = $this->input->post('username');
        $res = $this->Usermanagement_model->get_bumn($username);
        echo json_encode($res);
    }

    public function delete_data_user()
    {
        $this->load->model('Usermanagement_model');
        $username = $this->input->post('username');
        $res = $this->Usermanagement_model->delete_data_user($username);
        echo json_encode($res);
    }
    public function update_data_user()
    {
        $this->load->model('Usermanagement_model');
        $username   = $this->input->post('username');
        // $password   = $this->input->post('password');
        $level      = $this->input->post('level');
        $unit       = $this->input->post('unit');
        $active     = $this->input->post('active');
        $name       = $this->input->post('name');
        $address    = $this->input->post('address');
        $nohp       = $this->input->post('nohp');
        $data = [
            'email'     => $username,
            // 'password'  => md5($password),
            'level'     => $level,
            'active'    => $active,
            'nama'      => $name,
            'alamat'    => $address,
            'hp'        => $nohp,
            'unit'      => $unit,
        ];
        $res = $this->Usermanagement_model->update_data_user($data);
        echo json_encode($res);
    }


    public function store_data_user()
    {
        $this->load->model('Usermanagement_model');
        $username   = $this->input->post('username');
        // $password   = $this->input->post('password');
        $level      = $this->input->post('level');
        $unit       = $this->input->post('unit');
        $active     = $this->input->post('active');
        $name       = $this->input->post('name');
        $address    = $this->input->post('address');
        $nohp       = $this->input->post('nohp');
        $data = [
            'email'     => $username,
            // 'password'  => md5($password),
            'level'     => $level,
            'active'    => $active,
            'nama'      => $name,
            'alamat'    => $address,
            'hp'        => $nohp,
            'unit'      => $unit,
        ];
        $res = $this->Usermanagement_model->store_data_user($data);
        echo json_encode($res);
    }
    // public function get_user_level()
    // {
    //     $this->load->model('Usermanagement_model');
    //     $res = $this->Usermanagement_model->get_user_level();
    //     echo json_encode($res);
    // }
    public function add_level()
    {   
        $level_name = $this->input->post('lev_name');
        $level_display = $this->input->post('lev_display');
        $this->load->model('Usermanagement_model');
        $data= $this->Usermanagement_model->add_level($level_name,$level_display);
    }
}
