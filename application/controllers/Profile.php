<?php
class Profile extends CI_Controller {

    public function get_profile()
    {
        $email_user = $this->session->email;
        $this->load->model('profile_model');
        $profile = $this->profile_model->get_profile($email_user);
        echo json_encode($profile);
    }

    public function edit_profile()
    {
            $user_email = $this->input->post('input_email');    

            $input_nama = $this->input->post('input_nama');
            $input_hp = $this->input->post('input_hp');
            $input_website = $this->input->post('input_website');
            $input_alamat = $this->input->post('input_alamat');
            $input_city = $this->input->post('input_city');
            $input_province = $this->input->post('input_province');
            $input_jenis_usaha = $this->input->post('input_jenis_usaha');
            $input_jenis_produk = $this->input->post('input_jenis_produk');
            $input_daftar_produk = $this->input->post('input_daftar_produk');
            $input_kapasitas_produk = $this->input->post('input_kapasitas_produk');
            $input_satuan = $this->input->post('input_satuan');
            $input_gudang= $this->input->post('input_gudang');
            $input_pabrik = $this->input->post('input_pabrik');
            $input_ekspor = $this->input->post('input_ekspor');
            $input_lama_produk = $this->input->post('input_lama_produk');
            $input_kendala = $this->input->post('input_kendala');
            $input_tahun_berdiri = $this->input->post('input_tahun_berdiri');
            $input_negara_tujuan = $this->input->post('input_negara_tujuan');
            $input_jumlah_karyawan = $this->input->post('input_jumlah_karyawan');
            $input_sertifikasi = $this->input->post('input_sertifikasi');
            $input_oem = $this->input->post('input_oem');
            $input_term_pembayaran = $this->input->post('input_term_pembayaran');
            $input_term_harga = $this->input->post('input_term_harga');
            $input_logistik = $this->input->post('input_logistik');
            
            $this->load->model('profile_model');
            $data = [          
                    'nama' => $input_nama,
                    'hp' => $input_hp,
                    'website' => $input_website,
                    'alamat' => $input_alamat,
                    'city' => $input_city,
                    'province' => $input_province,
                    'jen_usaha' => $input_jenis_usaha,
                    'jen_produk' => $input_jenis_produk,
                    'daftar_produk' => $input_daftar_produk,
                    'kapasitas_produk' => $input_kapasitas_produk,
                    'satuan_produk' => $input_satuan,
                    'gudang' => $input_gudang,
                    'pabrik' => $input_pabrik,
                    'ekspor' => $input_ekspor,
                    'lama_produk' => $input_lama_produk,
                    'kendala' => $input_kendala,
                    'tahun_berdiri' => $input_tahun_berdiri,
                    'negara_tujuan' => $input_negara_tujuan,
                    'jml_karyawan' => $input_jumlah_karyawan,
                    'sertifikasi' => $input_sertifikasi,
                    'oem' => $input_oem,
                    'term_pembayaran' => $input_term_pembayaran,
                    'term_harga' => $input_term_harga,
                    'logistik' => $input_logistik,
                    'verified' => true
            ];
        $res = $this->profile_model->update_data_user($user_email, $data);
        echo json_encode($res);
    }
}
?>