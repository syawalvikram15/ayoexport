<?php

defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

class Register extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';

        $this->load->model('M_register');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->form_validation->set_error_delimiters('<div style="color:#f9243f; margin-bottom: 5px; margin-top:2px">', '</div>');

        $this->data['users'] = $this->M_register->getAllUsers();
        $this->data = array('user' => $this->M_register->get_user());
    }


    public function index()
    {
        $this->load->view('pages/register', $this->data);
    }

    public function auth()
    {
        $this->form_validation->set_rules('nama', 'Email', 'required');
        $this->form_validation->set_rules('email', 'Email', 'valid_email|required');
        $this->form_validation->set_rules('hp', 'No HP', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[7]|max_length[30]');
        $this->form_validation->set_rules('level', 'Level', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('pages/register', $this->data);
        } else {
            //get user inputs

            $nama = $this->input->post('nama');
            $email = $this->input->post('email');
            $hp = $this->input->post('hp');
            $unit = $this->input->post('unit');
            $password = $this->input->post('password');
            $level = $this->input->post('level');
            $alamat = $this->input->post('alamat');

            //generate simple random code
            $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $code = substr(str_shuffle($set), 0, 12);

            //insert user to users table and get id
            $users['nama'] = $nama;
            $users['email'] = $email;
            $users['hp'] = $hp;
            $users['unit'] = $unit;
            $users['password'] = $password;
            $users['level'] = $level;
            $users['alamat'] = $alamat;
            $users['code'] = $code;
            $users['active'] = false;

            //VALIDATE EMAIL, MAKE SURE NOT YET AXIST
            $get_email =  $this->M_register->get_email($email);
            $is_axist = $get_email['email'];

            if ($is_axist != NULL ) {
                $this->session->set_flashdata('msg', 'Email already registered, try to login.');
                $this->load->view('pages/register', $this->data);
            } else {

            
            $this->M_register->insert($users);
            $get_id = $this->M_register->getUser_id($email);
            $id = $get_id['id'];



            $mail = new PHPMailer(true);
            try {
                $mail->SMTPDebug = SMTP::DEBUG_SERVER;
                $mail->isSMTP();
           
                $mail->SMTPAuth   = true;                  // enable SMTP authentication
                $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
                $mail->Host       = "smtp.mail.yahoo.com";      // sets YAHOO as the SMTP server
                $mail->Port       = 465;                   // set the SMTP port for the GMAIL server
                $mail->Username   = "raihan.rnj@yahoo.com";  // GMAIL username
                $mail->Password   = "lzicbyargczcjhhn";            // GMAIL password

                $mail->SetFrom('raihan.rnj@yahoo.com', 'Activasi Account Ayoexport');
                $mail->addReplyTo( 'raihan.rnj@yahoo.com', '' );
                $mail->addAddress( $email, '' );

                $mail->isHTML(true);
                $mail->Subject = "Email Konfirmasi";
                $mailContent = "<html>
                                <head>
                                <title>Verification Code</title>
                                </head>
                                <body>
                                <h2>Thank you for Registering.</h2>
                                <p>Your Account:</p>
                                <p>Email: " . $email . "</p>
                                <p>Password: " . $password . "</p>
                                <p>Please click the link below to activate your account.</p>
                                <h4><a href='" . base_url() . "register/activate/" . $id . "/" . $code . "'>Activate My Account</a></h4>
                                </body>
                                </html>";
                $mail->Body = $mailContent;
                $mail->send();
                $this->session->set_flashdata('msg', 'Activation code sent to email');
            } catch (\Throwable $th) {
                $this->session->set_flashdata('msg', 'Gagal mendaftarkan, Pastikan email benar');
            }
            redirect('register/');
         }
        }
    }

    public function activate()
    {
        $id =  $this->uri->segment(3);
        $code = $this->uri->segment(4);

        //fetch user details
        $user = $this->M_register->getUser($id);
   
        //if code matches
        if ($user['code'] == $code) {
            //update user active status
            // $data['active'] = 1;
            $query = $this->M_register->activate($id);

            if ($query== true) {
                $this->session->set_flashdata('msg', 'User activated successfully, please login or register other account.');
            } else {
                $this->session->set_flashdata('msg', 'Something went wrong in activating account ');
            }
        } else {
            $this->session->set_flashdata('msg', 'Cannot activate account. Code didnt match');
        }
        redirect('register/auth');
    }

}
