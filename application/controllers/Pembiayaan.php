<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

 
class Pembiayaan extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';
    }

    public function add_transaction_pembiayaan()
    {

        $email_user = $this->session->email;
        $nama_produk = $this->input->post('nama_produk');    
        $jumlah_produk = $this->input->post('jumlah_produk'); 
        $asal_produk = $this->input->post('asal_produk'); 
        $tujuan_pengiriman = $this->input->post('tujuan_pengiriman'); 
        $estimasi_modal = $this->input->post('estimasi_modal'); 
        $d=time();
        $tanggal_pengiriman = date("Y-m-d h:i:s", $d);
        
        $this->load->model('pembiayaan_model');
        $data = [          
                
                'email' => $email_user,
                'nama_produk' => $nama_produk, 
                'jumlah_produk' => $jumlah_produk , 
                'asal_produk' => $asal_produk ,
                'tujuan_pengiriman' => $tujuan_pengiriman,
                'estimasi_modal' => $estimasi_modal ,
                'tanggal_pengiriman' => $tanggal_pengiriman,
                'status' => 'on-proggress' 
        ];
        $res = $this->pembiayaan_model->add_transaction_pembiayaan($data);
        echo json_encode($res);
    }

    public function get_transaction_pembiayaan()
    {
        // $email_user = $this->session->email;
        $this->load->model('pembiayaan_model');
        $data = $this->pembiayaan_model->get_transaction_pembiayaan();
        echo json_encode($data);
    }
    public function get_transaction_pembiayaan_done()
    {
        // $email_user = $this->session->email;
        $this->load->model('pembiayaan_model');
        $data = $this->pembiayaan_model->get_transaction_pembiayaan_done();
        echo json_encode($data);
    }
    public function get_transaction_pembiayaan_reject()
    {
        // $email_user = $this->session->email;
        $this->load->model('pembiayaan_model');
        $data = $this->pembiayaan_model->get_transaction_pembiayaan_reject();
        echo json_encode($data);
    }
    public function get_detail_pembiayaan()
    {
        $id = $this->input->post('id'); 
        $this->load->model('pembiayaan_model');
        $data = $this->pembiayaan_model->get_detail_pembiayaan($id);
        echo json_encode($data);
    }
    public function get_action_pembiayaan()
    {
        $id = $this->input->post('id'); 
        $status = $this->input->post('action'); 
        $this->load->model('pembiayaan_model');
        $data = $this->pembiayaan_model->get_action_pembiayaan($id,$status);
        echo json_encode($data);
    }

    public function send_respond_pembiayaan()
    {
        $this->load->model('user_model');

        $user_id = $this->input->post('id_user'); 
        $respon = $this->input->post('respon'); 
        $status = $this->input->post('status'); 
        $id_pembiayaan = $this->input->post('id_pembiayaan'); 
        $date = date("Y-m-d h:i:s", time());


        if ($status == 'done'){
            $massage_value = "Selamat Request Anda dengan ID {$id_pembiayaan } berhasil dilakukan";
        } else  if ($status == 'reject') {
            $massage_value = "Maaf Request Anda dengan ID {$id_pembiayaan } ditolak";
        } else  if ($status == 'offer') {
            $massage_value = "Selamat, Anda mendapatkan penawaran baru";
        } 
        $mail = new PHPMailer(true);

            // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
            $mail->isSMTP();
       
            $mail->SMTPAuth   = true;                  // enable SMTP authentication
            $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
            $mail->Host       = "smtp.mail.yahoo.com";      // sets YAHOO as the SMTP server
            $mail->Port       = 465;                   // set the SMTP port for the GMAIL server
            $mail->Username   = "noreplay_ayoexport@yahoo.com";  // GMAIL username
            $mail->Password   = "hukhcvjhsryauupx";            // GMAIL password

            $mail->SetFrom('noreplay_ayoexport@yahoo.com', 'Funding Request Information');
            $mail->addReplyTo( 'noreplay_ayoexport@yahoo.com', '' );
            $mail->addAddress( $user_id, '' );

            $mail->isHTML(true);
            $mail->Subject = "Funding Request Information Ayoexport";
            $mailContent = "
                            <center>
                            <title>Funding Request Information</title>
                            <img src='https://www.ayoexport.com/ayoexport/asset/zeedapp/assets/img/icon/icon.png' style='height:66px; margin-bottom:10px;'>
                            </head>
                            <body>
                            <h2>". $massage_value ."</h2>
                            <p>Hello " . $user_id . "</p>
                            <p>". $massage_value. ". Silahkan klik link berikut untuk melihat detail</p>
                          
                            <h4><a href='" . base_url() . "page/view/pembiayaan'>Show Detail Transaction</a></h4>
                            <h2>Thanks!</h2>
                            <h2>The Ayo Export Team </h2>
                            </center>
                           ";
            $mail->Body = $mailContent;
            $mail->send();
        
    
        $this->load->model('pembiayaan_model');
        $data = $this->pembiayaan_model->send_respond_pembiayaan($user_id,$respon, $status,$date,$id_pembiayaan);
        echo json_encode($data);
    }

     
    public function get_own_transaction_pembiayaan()
    {
        $email = $this->session->email;
        $this->load->model('pembiayaan_model');
        $data = $this->pembiayaan_model->get_own_transaction_pembiayaan($email);
        echo json_encode($data);
    }
    public function get_own_transaction_pembiayaan_done()
    {
        $email = $this->session->email;
        $this->load->model('pembiayaan_model');
        $data = $this->pembiayaan_model->get_own_transaction_pembiayaan_done($email);
        echo json_encode($data);
    }
    public function get_own_transaction_pembiayaan_reject()
    {
        $email = $this->session->email;
        $this->load->model('pembiayaan_model');
        $data = $this->pembiayaan_model->get_own_transaction_pembiayaan_reject($email);
        echo json_encode($data);
    }
    
    public function update_own_transaction_pembiayaan()
    {
        $id = $this->input->post('id');  
        $nama_produk = $this->input->post('nama_produk');    
        $jumlah_produk = $this->input->post('jumlah_produk'); 
        $asal_produk = $this->input->post('asal_produk'); 
        $tujuan_pengiriman = $this->input->post('tujuan_pengiriman'); 
        $estimasi_modal = $this->input->post('estimasi_modal'); 

        $this->load->model('pembiayaan_model');
        $res = $this->pembiayaan_model->update_own_transaction_pembiayaan($id,$nama_produk,	$jumlah_produk,	$asal_produk,	$tujuan_pengiriman,	$estimasi_modal);
        echo json_encode($res);
    }

    public function delete_own_transaction_pembiayaan()
    {
        $id_pembiayaan = $this->input->post('id');  
        $this->load->model('pembiayaan_model');
        $data = $this->pembiayaan_model->delete_own_transaction_pembiayaan($id_pembiayaan);
        echo json_encode($data);
    }

    public function get_own_detail_pembiayaan(){

        $id_pembiayaan = $this->input->post('id');  
        $this->load->model('pembiayaan_model');
        $data = $this->pembiayaan_model->get_own_detail_pembiayaan($id_pembiayaan);
        echo json_encode($data);
    }
    public function get_respon_pembiayaan(){

        $email = $this->session->email; 
        $this->load->model('pembiayaan_model');
        $data = $this->pembiayaan_model->get_respon_pembiayaan($email);
        echo json_encode($data);
    }
    public function get_respon_pembiayaan_admin(){

        $this->load->model('pembiayaan_model');
        $data = $this->pembiayaan_model->get_respon_pembiayaan_admin();
        echo json_encode($data);
    }
    public function confirm_offer_pembiayaan(){
        $id = $this->input->post('id'); 
        $respon = $this->input->post('respon'); 
        $this->load->model('pembiayaan_model');
        $data = $this->pembiayaan_model->confirm_offer_pembiayaan($id,$respon);
        echo json_encode($data);
    }
    
}
?>