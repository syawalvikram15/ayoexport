<?php
class Api extends CI_Controller {

    public function filter(){
        foreach($_POST as $k=>$v){
            $_SESSION['filter'][$k]=$v;
        }
    }
    public function tblUser(){
        $this->load->model('datatables_model');
		//error_reporting(1);
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 0);
		$fields = ['user_id','nama','no_hp','status','tgl_terdaftar'];
		$search = ['user_id','nama','no_hp','status'];
        $list = $this->datatables_model->get_datatables('mart_user',$fields,$search);
        $data = array();
        $no = !empty($_POST['start'])?$_POST['start']:0;
        $draw = !empty($_POST['draw'])?$_POST['draw']:0;
        foreach ($list as $l) {
            $no++;
            $row = array();
			$row[] = $no;
			$row[] = $l->user_id;
			$row[] = $l->nama;
			$row[] = $l->no_hp;
			$row[] = $l->status;
			$row[] = $l->tgl_terdaftar;
			$data[] = $row;
        }
        $output = array(
                        "draw" => $draw,
                        "recordsTotal" => $this->datatables_model->count_all(),
                        "recordsFiltered" => $this->datatables_model->count_filtered(),
                        "data" => $data,
						"query" => $this->datatables_model->echo_x()
                );
        echo json_encode($output);
    }

    public function cont_datascan()
    {
        $this->load->model('api_model');
        $cont_datascan= $this->api_model->cont_datascan();
        echo json_encode($cont_datascan);
    }

    public function activeuser_scan()
    {
        $this->load->model('api_model');
        $activeuser_scan= $this->api_model->activeuser_scan();
        echo json_encode($activeuser_scan);
    }

    public function unique_scanned_dev()
    {
        $this->load->model('api_model');
        $unique_scanned_dev= $this->api_model->unique_scanned_dev();
        echo json_encode($unique_scanned_dev);
    }
        public function total_user()
    {
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $this->load->model('api_model');
        $data= $this->api_model->total_user($tgl_awal,$tgl_akhir);
        echo json_encode($data);
    }
   

      public function user_status()
    {
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $this->load->model('api_model');
        $data= $this->api_model->user_status($tgl_awal,$tgl_akhir);
        echo json_encode($data);
    }
   
    
    public function user_maps()
    {
        $this->load->model('api_model');
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $user_maps= $this->api_model->user_maps($tgl_awal,$tgl_akhir);
        echo json_encode($user_maps);
    }
       public function user_PerODP()
    {
        $this->load->model('api_model');
        $user_PerODP= $this->api_model->user_PerODP();
        echo json_encode($user_PerODP);
    }
    public function user_PerPend()
    {
        $this->load->model('api_model');
        $user_PerPend= $this->api_model->user_PerPend();
        echo json_encode($user_PerPend);
    }
    public function Update_Kabupaten()
    {       
        $this->load->model('api_model');
        $res_kabupaten = $this->input->post('res_kabupaten');
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $data		= $this->api_model->Update_Kabupaten($res_kabupaten,$tgl_awal,$tgl_akhir);
        echo json_encode($data);
    }
}
?>