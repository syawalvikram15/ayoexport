<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

 
class Sales extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';
        
        $this->load->helper(array('form', 'url'));
    }

    public function do_upload()
    {   
        $this->load->helper(array('form', 'url'));
        $config['upload_path']="./asset/upload";
        $config['allowed_types']='gif|jpg|png';
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        
        
        // if ( ! $this->upload->do_upload("file"))
        //     {
        //           echo $this->upload->display_errors('<p>', '</p>');
        //     }
        if($this->upload->do_upload("file")){
         
    
            $email_user = $this->session->email;
            $nama_produk = $this->input->post('nama_produk');    
            $jumlah_produk = $this->input->post('jumlah_produk'); 
            $spesifikasi_produk = $this->input->post('spesifikasi_produk'); 
            $asal_produk = $this->input->post('asal_produk'); 
            $target_pasar = $this->input->post('target_pasar'); 
            $harga_produk = $this->input->post('harga_produk'); 
            $d=time();
            $tanggal_pengiriman = date("Y-m-d h:i:s", $d);
            
            $this->load->model('sales_model');
            $data = array('upload_data' => $this->upload->data());
            // echo $data['upload_data']['file_name'];
            // die();
            $data1 = [          
                    
                    'email' => $email_user,
                    'nama_produk' => $nama_produk, 
                    'jumlah_produk' => $jumlah_produk , 
                    'spesifikasi_produk' => $spesifikasi_produk,
                    'asal_produk' => $asal_produk ,
                    'target_pasar' => $target_pasar,
                    'harga_produk' => $harga_produk ,
                    'tanggal_pengiriman' => $tanggal_pengiriman,
                    'status' => 'on-progress',
                    'nama_gambar' => $data['upload_data']['file_name']
            ];
           
            // $data1 = array(
            //     'menu_id' => $this->input->post('selectmenuid'),
            //     'submenu_id' => $this->input->post('selectsubmenu'),
            //     'imagetitle' => $this->input->post('imagetitle'),
              
            // );  
            $result= $this->sales_model->add_transaction_sales($data1);
            // $res = $this->sales_model->add_transaction_sales($data);
            echo json_encode($result);
         } 

        
       
    }

    public function get_transaction_sales()
    {
        // $email_user = $this->session->email;
        $this->load->model('sales_model');
        $data = $this->sales_model->get_transaction_sales();
        echo json_encode($data);
    }
    public function get_transaction_sales_done()
    {
        // $email_user = $this->session->email;
        $this->load->model('sales_model');
        $data = $this->sales_model->get_transaction_sales_done();
        echo json_encode($data);
    }
    public function get_transaction_sales_reject()
    {
        // $email_user = $this->session->email;
        $this->load->model('sales_model');
        $data = $this->sales_model->get_transaction_sales_reject();
        echo json_encode($data);
    }
    public function get_detail_sales()
    {
        $id = $this->input->post('id'); 
        $this->load->model('sales_model');
        $data = $this->sales_model->get_detail_sales($id);
        echo json_encode($data);
    }
    public function get_action_sales()
    {
        $id = $this->input->post('id'); 
        $status = $this->input->post('action'); 
        $this->load->model('sales_model');
        $data = $this->sales_model->get_action_sales($id,$status);
        echo json_encode($data);
    }

    public function send_respond_sales()
    {
        $this->load->model('user_model');

        $user_id = $this->input->post('id_user'); 
        $respon = $this->input->post('respon'); 
        $status = $this->input->post('status'); 
        $id_sales = $this->input->post('id_sales'); 
        $date = date("Y-m-d h:i:s", time());


        if ($status == 'done'){
            $massage_value = "Selamat Transaksi Anda dengan ID {$id_sales } berhasil dilakukan";
        } else  if ($status == 'reject') {
            $massage_value = "Maaf Transaksi Anda dengan ID {$id_sales } ditolak";
        } else  if ($status == 'offer') {
            $massage_value = "Selamat, Anda mendapatkan penawaran baru";
        } 
        $mail = new PHPMailer(true);

            // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
            $mail->isSMTP();
       
            $mail->SMTPAuth   = true;                  // enable SMTP authentication
            $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
            $mail->Host       = "smtp.mail.yahoo.com";      // sets YAHOO as the SMTP server
            $mail->Port       = 465;                   // set the SMTP port for the GMAIL server
            $mail->Username   = "noreplay_ayoexport@yahoo.com";  // GMAIL username
            $mail->Password   = "hukhcvjhsryauupx";            // GMAIL password

            $mail->SetFrom('noreplay_ayoexport@yahoo.com', 'Sales Request Information');
            $mail->addReplyTo( 'noreplay_ayoexport@yahoo.com', '' );
            $mail->addAddress( $user_id, '' );

            $mail->isHTML(true);
            $mail->Subject = "Sales Request Information Ayoexport";
            $mailContent = "
                            <center>
                            <title>Sales Request Information</title>
                            <img src='https://www.ayoexport.com/ayoexport/asset/zeedapp/assets/img/icon/icon.png' style='height:66px; margin-bottom:10px;'>
                            </head>
                            <body>
                            <h2>". $massage_value ."</h2>
                            <p>Hello " . $user_id . "</p>
                            <p>". $massage_value. ". Silahkan klik link berikut untuk melihat detail</p>
                          
                            <h4><a href='" . base_url() . "page/view/sales'>Show Detail Transaction</a></h4>
                            <h2>Thanks!</h2>
                            <h2>The Ayo Export Team </h2>
                            </center>
                           ";
            $mail->Body = $mailContent;
            $mail->send();
        
    
        $this->load->model('sales_model');
        $data = $this->sales_model->send_respond_sales($user_id,$respon, $status,$date,$id_sales);
        echo json_encode($data);
    }

     
    public function get_own_transaction_sales()
    {
        $email = $this->session->email;
        $this->load->model('sales_model');
        $data = $this->sales_model->get_own_transaction_sales($email);
        echo json_encode($data);
    }
    public function get_own_transaction_sales_done()
    {
        $email = $this->session->email;
        $this->load->model('sales_model');
        $data = $this->sales_model->get_own_transaction_sales_done($email);
        echo json_encode($data);
    }
    public function get_own_transaction_sales_reject()
    {
        $email = $this->session->email;
        $this->load->model('sales_model');
        $data = $this->sales_model->get_own_transaction_sales_reject($email);
        echo json_encode($data);
    }
    
    public function update_own_transaction_sales()
    {
        $id = $this->input->post('id');  
        $nama_produk = $this->input->post('nama_produk');    
        $jumlah_produk = $this->input->post('jumlah_produk'); 
        $spesifikasi_produk = $this->input->post('spesifikasi_produk');
        $asal_produk = $this->input->post('asal_produk'); 
        $target_pasar = $this->input->post('target_pasar'); 
        $harga_produk = $this->input->post('harga_produk'); 

        $this->load->model('sales_model');
        $res = $this->sales_model->update_own_transaction_sales($id,$nama_produk,	$jumlah_produk,	$spesifikasi_produk, $asal_produk,	$target_pasar,	$harga_produk);
        echo json_encode($res);
    }

    public function delete_own_transaction_sales()
    {
        $id_sales = $this->input->post('id');  
        $this->load->model('sales_model');
        $data = $this->sales_model->delete_own_transaction_sales($id_sales);
        echo json_encode($data);
    }

    public function get_own_detail_sales(){

        $id_sales = $this->input->post('id');  
        $this->load->model('sales_model');
        $data = $this->sales_model->get_own_detail_sales($id_sales);
        echo json_encode($data);
    }
    public function get_respon_sales(){

        $email = $this->session->email; 
        $this->load->model('sales_model');
        $data = $this->sales_model->get_respon_sales($email);
        echo json_encode($data);
    }
    public function get_respon_sales_admin(){

        $this->load->model('sales_model');
        $data = $this->sales_model->get_respon_sales_admin();
        echo json_encode($data);
    }
    public function confirm_offer_sales(){
        $id = $this->input->post('id'); 
        $respon = $this->input->post('respon'); 
        $this->load->model('sales_model');
        $data = $this->sales_model->confirm_offer_sales($id,$respon);
        echo json_encode($data);
    }
    
}
?>