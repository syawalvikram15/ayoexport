<?php
class Page extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->helper(array('captcha', 'url'));
	}

	public function view($page = 'dashboard')
	{
		if (!$_SESSION['loged_in']) {
			redirect('page/belumlogin');
		}
		if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
			// Whoops, we don't have a page for that!
			show_404();
		}
		$this->load->model('user_model');
		$data['menu'] = $this->user_model->getMenu();
		$data['title'] = ucfirst($page); // Capitalize the first letter
		$data['page'] = $page;
		switch ($page) {
			case 'user_management':
				$data['menus'] = $this->user_model->getAllMenu();
				$data['levels'] = $this->user_model->getAllLevel();
				$data['usermenu'] = $this->user_model->getAllUserMenu();
				break;
		}
		$this->load->view('templates/header', $data);
		$this->load->view('pages/' . $page, $data);
		$this->load->view('templates/footer', $data);
	}
	public function user_level()
	{
		$data_arr = array();
		foreach ($_POST as $k => $p) {
			foreach ($p as $c) {
				$data_arr[] = "('$k',$c)";
			}
		}
		$this->load->model('user_model');
		$q =  "insert into user_menu values " . implode(',', $data_arr);
		$this->user_model->saveUserMenu($q);
		redirect(base_url() . 'index.php/page/view/user_management');
	}
	public function index()
	{
		if ($this->session->userdata('loged_in') !== null) {
			$this->load->model('user_model');
			$list_menu = $this->user_model->getMenu();
			// redirect('page/view/' . $list_menu[0]['link']);
			redirect('page/view/dashboard/');
			// redirect('page/view');
		} else {
			//posisi folder untuk menyimpan gambar captcha
			$path = './asset/captcha/';

			//membuat folder apabila folder captcha tidak ada
			if (!file_exists($path)) {
				$create = mkdir($path, 0777);
				if (!$create)
					return;
			}
			//Menampilkan huruf acak untuk dijadikan captcha
			$word = array_merge(range('0', '9'), range('A', 'Z'));
			$acak = shuffle($word);
			$str  = substr(implode($word), 0, 5);

			//Menyimpan huruf acak tersebut kedalam session
			$data_ses = array('captcha_str' => $str);
			$this->session->set_userdata($data_ses);

			//array untuk menampilkan gambar captcha
			$vals = array(
				'word'  => $str, //huruf acak yang telah dibuat diatas
				'img_path'  => $path, //path untuk menyimpan gambar captcha
				'img_url'   => base_url() . 'asset/captcha/', //url untuk menampilkan gambar captcha
				'img_width' => '150', //lebar gambar captcha
				'img_height' => 40, //tinggi gambar captcha
				'expiration' => 7200 //expired time per captcha
			);

			$cap = create_captcha($vals);
			$data['captcha_image'] = $cap['image']; //variable array untuk menampilkan captcha pada view
			$this->load->view('pages/login', $data);
		}
	}

	public function login()
	{

		$data['user'] = $this->input->post('username');
		$data['pass'] = $this->input->post('password');

		$this->load->model('user_model');

		$cek = $this->user_model->login($data);

		if (array_key_exists('email', $cek)) {
			$this->session->set_userdata(array(
				'username' => $data['user'],
				'nama' => $cek['nama'],
				'email' => $cek['email'],
				'hp' => $cek['hp'],
				'level' => $cek['level'],
				'loged_in' => true
			));
		}
		$_SESSION['photo'] = base_url() . "asset/user.jpg";
		$_SESSION['filter']['tgl_awal'] = isset($_SESSION['filter']['tgl_awal']) ? $_SESSION['filter']['tgl_awal'] : date("Y-m-d", strtotime("-7 day"));
		$_SESSION['filter']['tgl_akhir'] = isset($_SESSION['filter']['tgl_akhir']) ? $_SESSION['filter']['tgl_akhir'] : date("Y-m-d", strtotime("-1 day"));

		if ($this->session->userdata('loged_in') == true) {
			redirect('page/berhasillogin');
		} else {
			redirect('page/gagallogin');
		}
	}

	function berhasillogin()
	{
		redirect('page/index');
	}

	function gagallogin()
	{
		$url = base_url('page');
		echo $this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Username Atau Password Salah</div>');
		redirect($url);
	}

	function belumlogin()
	{
		$url = base_url('page');
		echo $this->session->set_flashdata('msg', '<div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button>Silahkan Login Terlebih dahulu</div>');
		redirect($url);
	}

	public function logout()
	{
		session_destroy();
		redirect('page/index');
	}

	// public function register_web($page = 'register_web')
	// {
	// 	if (!$_SESSION['loged_in']) {
	// 		redirect('page/belumlogin');
	// 	}
	// 	if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
	// 		// Whoops, we don't have a page for that!
	// 		show_404();
	// 	}
	// 	$this->load->model('user_model');
	// 	$this->load->model('M_register_web');
	// 	$data = array('user_website' => $this->M_register_web->get_register_web());
	// 	$data['menu'] = $this->user_model->getMenu();
	// 	$data['title'] = ucfirst($page); // Capitalize the first letter
	// 	$data['page'] = $page;
	// 	switch ($page) {
	// 		case 'user_management':
	// 			$data['menus'] = $this->user_model->getAllMenu();
	// 			$data['levels'] = $this->user_model->getAllLevel();
	// 			$data['usermenu'] = $this->user_model->getAllUserMenu();
	// 			break;
	// 	}

	// 	$this->load->view('templates/header', $data);
	// 	$this->load->view('pages/' . $page, $data);
	// 	$this->load->view('templates/footer', $data);
	// }

	// public function register_web_add($page = 'register_web_add')
	// {
	// 	if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
	// 		// Whoops, we don't have a page for that!
	// 		show_404();
	// 	}

	// 	$this->load->model('M_register_web');
	// 	$this->load->model('user_model');

	// 	$dariDB = $this->M_register_web->cekidtracker();
	// 	$nourut = substr($dariDB, 5, 4);
	// 	$idtrackersekarang = $nourut + 1;
	// 	$data = array('id_tracker' => $idtrackersekarang);

	// 	$data['kodeweb'] = $this->M_register_web->get_code();

	// 	$data['menu'] = $this->user_model->getMenu();
	// 	$data['title'] = ucfirst($page); // Capitalize the first letter
	// 	$data['page'] = $page;
	// 	switch ($page) {
	// 		case 'user_management':
	// 			$data['menus'] = $this->user_model->getAllMenu();
	// 			$data['levels'] = $this->user_model->getAllLevel();
	// 			$data['usermenu'] = $this->user_model->getAllUserMenu();
	// 			break;
	// 	}

	// 	$this->load->view('templates/header', $data);
	// 	$this->load->view('pages/' . $page, $data);
	// 	$this->load->view('templates/footer', $data);
	// }

	// public function register_web_act()
	// {
	// 	$this->load->model('M_register_web');
	// 	$id_tracker = $this->input->post('id_tracker');
	// 	$id_website = $this->input->post('id_website');
	// 	$website_name = $this->input->post('website_name');

	// 	$this->form_validation->set_rules('id_tracker', 'ID Tracker', 'required');
	// 	$this->form_validation->set_rules('id_website', 'id web', 'required');
	// 	$this->form_validation->set_rules('website_name', 'Nama Website', 'required');

	// 	if ($this->form_validation->run() != false) {
	// 		$data = array(
	// 			'id_tracker' => $id_tracker,
	// 			'id_website' => $id_website,
	// 			'website_name' => $website_name,
	// 			'tgl_registrasi' => date('Y-m-d'),
	// 		);
	// 		$this->M_register_web->insert_data($data, 'user_website');
	// 		$url = base_url('page/register_web');
	// 		echo $this->session->set_flashdata('msg', '<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Pendaftaran Berhasil</div>');
	// 		redirect($url);
	// 	} else {
	// 		redirect('page/re_web_gagal');
	// 	}
	// }
	// function re_web_gagal()
	// {
	// 	$url = base_url('page');
	// 	echo $this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Pendaftaran gagal</div>');
	// 	redirect($url);
	// }

	// public function tracking($page = 'tracking')
	// {
	// 	$this->load->model('M_tracking');
	// 	if (!$_SESSION['loged_in']) {
	// 		redirect('page/belumlogin');
	// 	}
	// 	if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
	// 		// Whoops, we don't have a page for that!
	// 		show_404();
	// 	}
	// 	$this->load->model('user_model');
	// 	$this->load->model('M_register_web');
	// 	$data = array('tracking' => $this->M_tracking->get_tracking());
	// 	$data['menu'] = $this->user_model->getMenu();
	// 	$data['title'] = ucfirst($page); // Capitalize the first letter
	// 	$data['page'] = $page;
	// 	switch ($page) {
	// 		case 'user_management':
	// 			$data['menus'] = $this->user_model->getAllMenu();
	// 			$data['levels'] = $this->user_model->getAllLevel();
	// 			$data['usermenu'] = $this->user_model->getAllUserMenu();
	// 			break;
	// 	}

	// 	$this->load->view('templates/header', $data);
	// 	$this->load->view('pages/' . $page, $data);
	// 	$this->load->view('templates/footer', $data);
	// }

	// public function tracking_detail($id, $page = 'tracking_detail')
	// {
	// 	$this->load->model('user_model');
	// 	$this->load->model('M_tracking');

	// 	$where = array('id' => $id);


	// 	if (!$_SESSION['loged_in']) {
	// 		redirect('page/belumlogin');
	// 	}
	// 	if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
	// 		// Whoops, we don't have a page for that!
	// 		show_404();
	// 	}
	// 	$data['tracking'] = $this->M_tracking->detail($where, 'tracking')->result();
	// 	$data['menu'] = $this->user_model->getMenu();
	// 	$data['title'] = ucfirst($page); // Capitalize the first letter
	// 	$data['page'] = $page;
	// 	switch ($page) {
	// 		case 'user_management':
	// 			$data['menus'] = $this->user_model->getAllMenu();
	// 			$data['levels'] = $this->user_model->getAllLevel();
	// 			$data['usermenu'] = $this->user_model->getAllUserMenu();
	// 			break;
	// 	}

	// 	$this->load->view('templates/header', $data);
	// 	$this->load->view('pages/' . $page, $data);
	// 	$this->load->view('templates/footer', $data);
	// }
}