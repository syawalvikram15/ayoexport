<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

 
class Regulasi extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';
    }

    public function add_transaction_regulasi()
    {

        $email_user = $this->session->email;
        $nama_produk = $this->input->post('nama_produk');    
        $jumlah_produk = $this->input->post('jumlah_produk'); 
        $asal_produk = $this->input->post('asal_produk'); 
        $tujuan_pengiriman = $this->input->post('tujuan_pengiriman'); 
        $d=time();
        $tanggal_pengiriman = date("Y-m-d h:i:s", $d);
        
        $this->load->model('regulasi_model');
        $data = [          
                
                'email' => $email_user,
                'nama_produk' => $nama_produk, 
                'jumlah_produk' => $jumlah_produk , 
                'asal_produk' => $asal_produk ,
                'tujuan_pengiriman' => $tujuan_pengiriman,
                'tanggal_pengiriman' => $tanggal_pengiriman,
                'status' => 'on-proggress' 
        ];
        $res = $this->regulasi_model->add_transaction_regulasi($data);
        echo json_encode($res);
    }

    public function get_transaction_regulasi()
    {
        // $email_user = $this->session->email;
        $this->load->model('regulasi_model');
        $data = $this->regulasi_model->get_transaction_regulasi();
        echo json_encode($data);
    }
    public function get_transaction_regulasi_done()
    {
        // $email_user = $this->session->email;
        $this->load->model('regulasi_model');
        $data = $this->regulasi_model->get_transaction_regulasi_done();
        echo json_encode($data);
    }
    public function get_transaction_regulasi_reject()
    {
        // $email_user = $this->session->email;
        $this->load->model('regulasi_model');
        $data = $this->regulasi_model->get_transaction_regulasi_reject();
        echo json_encode($data);
    }
    public function get_detail_regulasi()
    {
        $id = $this->input->post('id'); 
        $this->load->model('regulasi_model');
        $data = $this->regulasi_model->get_detail_regulasi($id);
        echo json_encode($data);
    }
    public function get_action_regulasi()
    {
        $id = $this->input->post('id'); 
        $status = $this->input->post('action'); 
        $this->load->model('regulasi_model');
        $data = $this->regulasi_model->get_action_regulasi($id,$status);
        echo json_encode($data);
    }

    public function send_respond_regulasi()
    {
        $this->load->model('user_model');

        $user_id = $this->input->post('id_user'); 
        $respon = $this->input->post('respon'); 
        $status = $this->input->post('status'); 
        $id_regulasi = $this->input->post('id_regulasi'); 
        $date = date("Y-m-d h:i:s", time());


        if ($status == 'done'){
            $massage_value = "Selamat Request Anda dengan ID {$id_regulasi } berhasil dilakukan";
        } else  if ($status == 'reject') {
            $massage_value = "Maaf Request Anda dengan ID {$id_regulasi } ditolak";
        } else  if ($status == 'offer') {
            $massage_value = "Selamat, Anda mendapatkan penawaran baru";
        } 
        $mail = new PHPMailer(true);

            // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
            $mail->isSMTP();
       
            $mail->SMTPAuth   = true;                  // enable SMTP authentication
            $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
            $mail->Host       = "smtp.mail.yahoo.com";      // sets YAHOO as the SMTP server
            $mail->Port       = 465;                   // set the SMTP port for the GMAIL server
            $mail->Username   = "noreplay_ayoexport@yahoo.com";  // GMAIL username
            $mail->Password   = "hukhcvjhsryauupx";            // GMAIL password

            $mail->SetFrom('noreplay_ayoexport@yahoo.com', 'Regulation Request Information');
            $mail->addReplyTo( 'noreplay_ayoexport@yahoo.com', '' );
            $mail->addAddress( $user_id, '' );

            $mail->isHTML(true);
            $mail->Subject = "Regulation Request Information Ayoexport";
            $mailContent = "
                            <center>
                            <title>Regulation Request Information</title>
                            <img src='https://www.ayoexport.com/ayoexport/asset/zeedapp/assets/img/icon/icon.png' style='height:66px; margin-bottom:10px;'>
                            </head>
                            <body>
                            <h2>". $massage_value ."</h2>
                            <p>Hello " . $user_id . "</p>
                            <p>". $massage_value. ". Silahkan klik link berikut untuk melihat detail</p>
                          
                            <h4><a href='" . base_url() . "page/view/regulasi'>Show Detail Transaction</a></h4>
                            <h2>Thanks!</h2>
                            <h2>The Ayo Export Team </h2>
                            </center>
                           ";
            $mail->Body = $mailContent;
            $mail->send();
        
    
        $this->load->model('regulasi_model');
        $data = $this->regulasi_model->send_respond_regulasi($user_id,$respon, $status,$date,$id_regulasi);
        echo json_encode($data);
    }

     
    public function get_own_transaction_regulasi()
    {
        $email = $this->session->email;
        $this->load->model('regulasi_model');
        $data = $this->regulasi_model->get_own_transaction_regulasi($email);
        echo json_encode($data);
    }
    public function get_own_transaction_regulasi_done()
    {
        $email = $this->session->email;
        $this->load->model('regulasi_model');
        $data = $this->regulasi_model->get_own_transaction_regulasi_done($email);
        echo json_encode($data);
    }
    public function get_own_transaction_regulasi_reject()
    {
        $email = $this->session->email;
        $this->load->model('regulasi_model');
        $data = $this->regulasi_model->get_own_transaction_regulasi_reject($email);
        echo json_encode($data);
    }
    
    public function update_own_transaction_regulasi()
    {
        $id = $this->input->post('id');  
        $nama_produk = $this->input->post('nama_produk');    
        $jumlah_produk = $this->input->post('jumlah_produk'); 
        $asal_produk = $this->input->post('asal_produk'); 
        $tujuan_pengiriman = $this->input->post('tujuan_pengiriman'); 

        $this->load->model('regulasi_model');
        $res = $this->regulasi_model->update_own_transaction_regulasi($id,$nama_produk,	$jumlah_produk,	$asal_produk,	$tujuan_pengiriman);
        echo json_encode($res);
    }

    public function delete_own_transaction_regulasi()
    {
        $id_regulasi = $this->input->post('id');  
        $this->load->model('regulasi_model');
        $data = $this->regulasi_model->delete_own_transaction_regulasi($id_regulasi);
        echo json_encode($data);
    }

    public function get_own_detail_regulasi(){

        $id_regulasi = $this->input->post('id');  
        $this->load->model('regulasi_model');
        $data = $this->regulasi_model->get_own_detail_regulasi($id_regulasi);
        echo json_encode($data);
    }
    public function get_respon_regulasi(){

        $email = $this->session->email; 
        $this->load->model('regulasi_model');
        $data = $this->regulasi_model->get_respon_regulasi($email);
        echo json_encode($data);
    }
    public function get_respon_regulasi_admin(){

        $this->load->model('regulasi_model');
        $data = $this->regulasi_model->get_respon_regulasi_admin();
        echo json_encode($data);
    }
    public function confirm_offer_regulasi(){
        $id = $this->input->post('id'); 
        $respon = $this->input->post('respon'); 
        $this->load->model('regulasi_model');
        $data = $this->regulasi_model->confirm_offer_regulasi($id,$respon);
        echo json_encode($data);
    }
    
}
?>