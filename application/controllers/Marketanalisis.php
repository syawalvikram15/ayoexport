<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

 
class Marketanalisis extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';
    }

    public function add_transaction_marketanalisis()
    {

        $email_user = $this->session->email;
        $nama_produk = $this->input->post('nama_produk');    
        $kapasitas_produksi = $this->input->post('kapasitas_produksi'); 
        $asal_produk = $this->input->post('asal_produk'); 
        $target_pasar = $this->input->post('target_pasar'); 
        $d=time();
        $tanggal_pengiriman = date("Y-m-d h:i:s", $d);
        
        $this->load->model('marketanalisis_model');
        $data = [          
                
                'email' => $email_user,
                'nama_produk' => $nama_produk,
                'kapasitas_produksi' => $kapasitas_produksi,
                'asal_produk' => $asal_produk,
                'target_pasar' => $target_pasar,
                'tanggal_pengiriman' => $tanggal_pengiriman,
                'status' => 'on-progress' 
        ];
        $res = $this->marketanalisis_model->add_transaction_marketanalisis($data);
        echo json_encode($res);
    }

    public function get_transaction_marketanalisis()
    {
        // $email_user = $this->session->email;
        $this->load->model('marketanalisis_model');
        $data = $this->marketanalisis_model->get_transaction_marketanalisis();
        echo json_encode($data);
    }
    public function get_transaction_marketanalisis_done()
    {
        // $email_user = $this->session->email;
        $this->load->model('marketanalisis_model');
        $data = $this->marketanalisis_model->get_transaction_marketanalisis_done();
        echo json_encode($data);
    }
    public function get_transaction_marketanalisis_reject()
    {
        // $email_user = $this->session->email;
        $this->load->model('marketanalisis_model');
        $data = $this->marketanalisis_model->get_transaction_marketanalisis_reject();
        echo json_encode($data);
    }
    public function get_detail_marketanalisis()
    {
        $id = $this->input->post('id'); 
        $this->load->model('marketanalisis_model');
        $data = $this->marketanalisis_model->get_detail_marketanalisis($id);
        echo json_encode($data);
    }
    public function get_action_marketanalisis()
    {
        $id = $this->input->post('id'); 
        $status = $this->input->post('action'); 
        $this->load->model('marketanalisis_model');
        $data = $this->marketanalisis_model->get_action_marketanalisis($id,$status);
        echo json_encode($data);
    }

    public function send_respond_marketanalisis()
    {
        $this->load->model('user_model');

        $user_id = $this->input->post('id_user'); 
        $respon = $this->input->post('respon'); 
        $status = $this->input->post('status'); 
        $id_marketanalisis = $this->input->post('id_marketanalisis'); 
        $date = date("Y-m-d h:i:s", time());


        if ($status == 'done'){
            $massage_value = "Selamat Request Anda dengan ID {$id_marketanalisis } berhasil dilakukan";
        } else  if ($status == 'reject') {
            $massage_value = "Maaf Request Anda dengan ID {$id_marketanalisis } ditolak";
        } else  if ($status == 'offer') {
            $massage_value = "Selamat, Anda mendapatkan penawaran baru";
        } 
        $mail = new PHPMailer(true);

            // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
            $mail->isSMTP();
       
            $mail->SMTPAuth   = true;                  // enable SMTP authentication
            $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
            $mail->Host       = "smtp.mail.yahoo.com";      // sets YAHOO as the SMTP server
            $mail->Port       = 465;                   // set the SMTP port for the GMAIL server
            $mail->Username   = "noreplay_ayoexport@yahoo.com";  // GMAIL username
            $mail->Password   = "hukhcvjhsryauupx";            // GMAIL password

            $mail->SetFrom('noreplay_ayoexport@yahoo.com', 'Market Analysis Request Information');
            $mail->addReplyTo( 'noreplay_ayoexport@yahoo.com', '' );
            $mail->addAddress( $user_id, '' );

            $mail->isHTML(true);
            $mail->Subject = "Market Analysis Request Information Ayoexport";
            $mailContent = "
                            <center>
                            <title>Market Analysis Request Information</title>
                            <img src='https://www.ayoexport.com/ayoexport/asset/zeedapp/assets/img/icon/icon.png' style='height:66px; margin-bottom:10px;'>
                            </head>
                            <body>
                            <h2>". $massage_value ."</h2>
                            <p>Hello " . $user_id . "</p>
                            <p>". $massage_value. ". Silahkan klik link berikut untuk melihat detail</p>
                          
                            <h4><a href='" . base_url() . "page/view/marketanalisis'>Show Detail Request</a></h4>
                            <h2>Thanks!</h2>
                            <h2>The Ayo Export Team </h2>
                            </center>
                           ";
            $mail->Body = $mailContent;
            $mail->send();
        
    
        $this->load->model('marketanalisis_model');
        $data = $this->marketanalisis_model->send_respond_marketanalisis($user_id,$respon, $status,$date,$id_marketanalisis);
        echo json_encode($data);
    }

     
    public function get_own_transaction_marketanalisis()
    {
        $email = $this->session->email;
        $this->load->model('marketanalisis_model');
        $data = $this->marketanalisis_model->get_own_transaction_marketanalisis($email);
        echo json_encode($data);
    }
    public function get_own_transaction_marketanalisis_done()
    {
        $email = $this->session->email;
        $this->load->model('marketanalisis_model');
        $data = $this->marketanalisis_model->get_own_transaction_marketanalisis_done($email);
        echo json_encode($data);
    }
    public function get_own_transaction_marketanalisis_reject()
    {
        $email = $this->session->email;
        $this->load->model('marketanalisis_model');
        $data = $this->marketanalisis_model->get_own_transaction_marketanalisis_reject($email);
        echo json_encode($data);
    }
    
    public function update_own_transaction_marketanalisis()
    {
        $id = $this->input->post('id');  
        $nama_produk = $this->input->post('nama_produk');    
        $kapasitas_produksi = $this->input->post('kapasitas_produksi');
        $asal_produk = $this->input->post('asal_produk'); 
        $target_pasar = $this->input->post('target_pasar');

        $this->load->model('marketanalisis_model');
        $res = $this->marketanalisis_model->update_own_transaction_marketanalisis($id,$nama_produk,	$kapasitas_produksi, $asal_produk, $target_pasar);
        echo json_encode($res);
    }

    public function delete_own_transaction_marketanalisis()
    {
        $id_marketanalisis = $this->input->post('id');  
        $this->load->model('marketanalisis_model');
        $data = $this->marketanalisis_model->delete_own_transaction_marketanalisis($id_marketanalisis);
        echo json_encode($data);
    }

    public function get_own_detail_marketanalisis(){

        $id_marketanalisis = $this->input->post('id');  
        $this->load->model('marketanalisis_model');
        $data = $this->marketanalisis_model->get_own_detail_marketanalisis($id_marketanalisis);
        echo json_encode($data);
    }
    public function get_respon_marketanalisis(){

        $email = $this->session->email; 
        $this->load->model('marketanalisis_model');
        $data = $this->marketanalisis_model->get_respon_marketanalisis($email);
        echo json_encode($data);
    }
    public function get_respon_marketanalisis_admin(){

        $this->load->model('marketanalisis_model');
        $data = $this->marketanalisis_model->get_respon_marketanalisis_admin();
        echo json_encode($data);
    }
    public function confirm_offer_marketanalisis(){
        $id = $this->input->post('id'); 
        $respon = $this->input->post('respon'); 
        $this->load->model('marketanalisis_model');
        $data = $this->marketanalisis_model->confirm_offer_marketanalisis($id,$respon);
        echo json_encode($data);
    }
    
}
?>