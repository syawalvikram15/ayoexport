<?php


 
class Dashboard extends CI_Controller {
    function __construct()
    {
        parent::__construct();
  
    }

 
    public function get_card_logistik()
    {
        // $email_user = $this->session->email;
        $this->load->model('dashboard_model');
        $data = $this->dashboard_model->get_card_logistik();
        echo json_encode($data);
    }

 
    public function get_card_asuransi()
    {
        // $email_user = $this->session->email;
        $this->load->model('dashboard_model');
        $data = $this->dashboard_model->get_card_asuransi();
        echo json_encode($data);
    }

 
    public function get_card_konsultasi()
    {
        // $email_user = $this->session->email;
        $this->load->model('dashboard_model');
        $data = $this->dashboard_model->get_card_konsultasi();
        echo json_encode($data);
    }

 
    public function get_card_marketanalisis()
    {
        // $email_user = $this->session->email;
        $this->load->model('dashboard_model');
        $data = $this->dashboard_model->get_card_marketanalisis();
        echo json_encode($data);
    }

 
    public function get_card_pembiayaan()
    {
        // $email_user = $this->session->email;
        $this->load->model('dashboard_model');
        $data = $this->dashboard_model->get_card_pembiayaan();
        echo json_encode($data);
    }

 
    public function get_card_regulasi()
    {
        // $email_user = $this->session->email;
        $this->load->model('dashboard_model');
        $data = $this->dashboard_model->get_card_regulasi();
        echo json_encode($data);
    }

 
    public function get_card_sales()
    {
        // $email_user = $this->session->email;
        $this->load->model('dashboard_model');
        $data = $this->dashboard_model->get_card_sales();
        echo json_encode($data);
    }

 
    public function get_card_sertifikasi()
    {
        // $email_user = $this->session->email;
        $this->load->model('dashboard_model');
        $data = $this->dashboard_model->get_card_sertifikasi();
        echo json_encode($data);
    }

    public function get_line_chart()
    {
        // $email_user = $this->session->email;
        $this->load->model('dashboard_model');
        $data = $this->dashboard_model->get_line_chart();
        echo json_encode($data);
    }

    
    
}




?>


