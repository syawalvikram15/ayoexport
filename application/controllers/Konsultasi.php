<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

 
class Konsultasi extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';
    }

    public function add_transaction_konsultasi()
    {

        $email_user = $this->session->email;
        $nama_produk = $this->input->post('nama_produk'); 
        $asal_produk = $this->input->post('asal_produk'); 
        $target_pasar = $this->input->post('target_pasar'); 
        $d=time();
        $tanggal_pengiriman = date("Y-m-d h:i:s", $d);
        
        $this->load->model('konsultasi_model');
        $data = [          
                
                'email' => $email_user,
                'nama_produk' => $nama_produk,
                'asal_produk' => $asal_produk,
                'target_pasar' => $target_pasar,
                'tanggal_pengiriman' => $tanggal_pengiriman,
                'status' => 'on-progress' 
        ];
        $res = $this->konsultasi_model->add_transaction_konsultasi($data);
        echo json_encode($res);
    }

    public function get_transaction_konsultasi()
    {
        // $email_user = $this->session->email;
        $this->load->model('konsultasi_model');
        $data = $this->konsultasi_model->get_transaction_konsultasi();
        echo json_encode($data);
    }
    public function get_transaction_konsultasi_done()
    {
        // $email_user = $this->session->email;
        $this->load->model('konsultasi_model');
        $data = $this->konsultasi_model->get_transaction_konsultasi_done();
        echo json_encode($data);
    }
    public function get_transaction_konsultasi_reject()
    {
        // $email_user = $this->session->email;
        $this->load->model('konsultasi_model');
        $data = $this->konsultasi_model->get_transaction_konsultasi_reject();
        echo json_encode($data);
    }
    public function get_detail_konsultasi()
    {
        $id = $this->input->post('id'); 
        $this->load->model('konsultasi_model');
        $data = $this->konsultasi_model->get_detail_konsultasi($id);
        echo json_encode($data);
    }
    public function get_action_konsultasi()
    {
        $id = $this->input->post('id'); 
        $status = $this->input->post('action'); 
        $this->load->model('konsultasi_model');
        $data = $this->konsultasi_model->get_action_konsultasi($id,$status);
        echo json_encode($data);
    }

    public function send_respond_konsultasi()
    {
        $this->load->model('user_model');

        $user_id = $this->input->post('id_user'); 
        $respon = $this->input->post('respon'); 
        $status = $this->input->post('status'); 
        $id_konsultasi = $this->input->post('id_konsultasi'); 
        $date = date("Y-m-d h:i:s", time());


        if ($status == 'done'){
            $massage_value = "Selamat Request Anda dengan ID {$id_konsultasi } berhasil dilakukan";
        } else  if ($status == 'reject') {
            $massage_value = "Maaf Request Anda dengan ID {$id_konsultasi } ditolak";
        } else  if ($status == 'offer') {
            $massage_value = "Selamat, Anda mendapatkan penawaran baru";
        } 
        $mail = new PHPMailer(true);

            // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
            $mail->isSMTP();
       
            $mail->SMTPAuth   = true;                  // enable SMTP authentication
            $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
            $mail->Host       = "smtp.mail.yahoo.com";      // sets YAHOO as the SMTP server
            $mail->Port       = 465;                   // set the SMTP port for the GMAIL server
            $mail->Username   = "noreplay_ayoexport@yahoo.com";  // GMAIL username
            $mail->Password   = "hukhcvjhsryauupx";            // GMAIL password

            $mail->SetFrom('noreplay_ayoexport@yahoo.com', 'Consultation Request Information');
            $mail->addReplyTo( 'noreplay_ayoexport@yahoo.com', '' );
            $mail->addAddress( $user_id, '' );

            $mail->isHTML(true);
            $mail->Subject = "Consultation Request Information Ayoexport";
            $mailContent = "
                            <center>
                            <title>Consultation Request Information</title>
                            <img src='https://www.ayoexport.com/ayoexport/asset/zeedapp/assets/img/icon/icon.png' style='height:66px; margin-bottom:10px;'>
                            </head>
                            <body>
                            <h2>". $massage_value ."</h2>
                            <p>Hello " . $user_id . "</p>
                            <p>". $massage_value. ". Silahkan klik link berikut untuk melihat detail</p>
                          
                            <h4><a href='" . base_url() . "page/view/konsultasi'>Show Detail Transaction</a></h4>
                            <h2>Thanks!</h2>
                            <h2>The Ayo Export Team </h2>
                            </center>
                           ";
            $mail->Body = $mailContent;
            $mail->send();
        
    
        $this->load->model('konsultasi_model');
        $data = $this->konsultasi_model->send_respond_konsultasi($user_id,$respon, $status,$date,$id_konsultasi);
        echo json_encode($data);
    }

     
    public function get_own_transaction_konsultasi()
    {
        $email = $this->session->email;
        $this->load->model('konsultasi_model');
        $data = $this->konsultasi_model->get_own_transaction_konsultasi($email);
        echo json_encode($data);
    }
    public function get_own_transaction_konsultasi_done()
    {
        $email = $this->session->email;
        $this->load->model('konsultasi_model');
        $data = $this->konsultasi_model->get_own_transaction_konsultasi_done($email);
        echo json_encode($data);
    }
    public function get_own_transaction_konsultasi_reject()
    {
        $email = $this->session->email;
        $this->load->model('konsultasi_model');
        $data = $this->konsultasi_model->get_own_transaction_konsultasi_reject($email);
        echo json_encode($data);
    }
    
    public function update_own_transaction_konsultasi()
    {
        $id = $this->input->post('id');  
        $nama_produk = $this->input->post('nama_produk'); 
        $asal_produk = $this->input->post('asal_produk'); 
        $target_pasar = $this->input->post('target_pasar'); 

        $this->load->model('konsultasi_model');
        $res = $this->konsultasi_model->update_own_transaction_konsultasi($id,$nama_produk,	$asal_produk, $target_pasar);
        echo json_encode($res);
    }

    public function delete_own_transaction_konsultasi()
    {
        $id_konsultasi = $this->input->post('id');  
        $this->load->model('konsultasi_model');
        $data = $this->konsultasi_model->delete_own_transaction_konsultasi($id_konsultasi);
        echo json_encode($data);
    }

    public function get_own_detail_konsultasi(){

        $id_konsultasi = $this->input->post('id');  
        $this->load->model('konsultasi_model');
        $data = $this->konsultasi_model->get_own_detail_konsultasi($id_konsultasi);
        echo json_encode($data);
    }
    public function get_respon_konsultasi(){

        $email = $this->session->email; 
        $this->load->model('konsultasi_model');
        $data = $this->konsultasi_model->get_respon_konsultasi($email);
        echo json_encode($data);
    }
    public function get_respon_konsultasi_admin(){

        $this->load->model('konsultasi_model');
        $data = $this->konsultasi_model->get_respon_konsultasi_admin();
        echo json_encode($data);
    }
    public function confirm_offer_konsultasi(){
        $id = $this->input->post('id'); 
        $respon = $this->input->post('respon'); 
        $this->load->model('konsultasi_model');
        $data = $this->konsultasi_model->confirm_offer_konsultasi($id,$respon);
        echo json_encode($data);
    }
    
}
?>