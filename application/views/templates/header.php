<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Responsive sidebar template with sliding effect and dropdown menu based on bootstrap 3">
    <title>AYO EXPORT</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>/asset/zeedapp/assets/img/icon/icon.png">
    <!-- Custom CSS -->
    <!-- <link href="<?= base_url() ?>asset/style-dashboard.css" rel="stylesheet"> -->
    <link href="<?= base_url() ?>asset/css/style.css" rel="stylesheet">
    <link href="<?= base_url() ?>asset/css/chatbot.css" rel="stylesheet">
    <!-- color CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <link type="text/css" rel="stylesheet" media="all" href="<?= base_url() ?>asset/css/chosen.min.css" />
    <link type="text/css" rel="stylesheet" media="all" href="<?= base_url() ?>asset/css/screen.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <link href="<?= base_url() ?>asset/css/flags.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link href="<?= base_url() ?>asset/css/page/<?= $page ?>.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.1/css/all.min.css" rel="stylesheet">    

 
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
    <!-- <link rel="stylesheet" href="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.css" /> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet-routing-machine/3.2.12/leaflet-routing-machine.css" />
    <link rel="stylesheet" href="<?= base_url() ?>asset/css/floating-wpp.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

</head>

<body>
    <div class="page-wrapper chiller-theme toggled">
        <a id="show-sidebar" class="btn btn-sm btn-biru" href="#">
            <i class="fas fa-bars"></i>
        </a>
        <nav id="sidebar" class="sidebar-wrapper">
            <div class="sidebar-content">
                <div class="sidebar-brand">
                    <!-- <a href="#" style="text-align:center;font-size: 15px;">Ayoexport.com</a> -->
                    <a href="<?= base_url() ?>" > <img src="<?= base_url('asset/logo-white.png') ?>"  alt="Ayoexport" style="height:70px;" > </a>
                    <div id="close-sidebar">
                        <i class="fas fa-times"></i>
                    </div>
                </div>
                <div class="sidebar-header">
                    <div class="user-pic">
                        <img class="img-responsive img-rounded" style="width:50px;height:50px;" src="<?= $_SESSION['photo'] ?>" alt="User picture">
                    </div>
                    <div class="user-info">
                        <span class="user-name"><?= $this->session->nama; ?>
                        </span>
                        <span class="user-role"><?= $this->session->level; ?></span>
                        <span class="user-status">
                            <i class="fa fa-circle"></i>
                            <span>Online</span>
                        </span>
                    </div>
                </div>
                <!-- sidebar-header  -->
                <div class="sidebar-search">
                    <div>
                        <div class="input-group">
                            <input type="text" class="form-control search-menu" placeholder="Search...">
                            <!-- <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </span>
                            </div> -->
                        </div>
                    </div>
                </div>
                <!-- sidebar-search  -->
                <div class="sidebar-menu">
                     <ul>
                        <li class="header-menu">
                            <span>General</span>
                        </li>
                       <?php
                        //$mnu_level = array_unique($menu['level']);
                        $n = sizeof($menu);
                        $n--;
                        for ($i = 0; $i <= $n; $i++) {
                            $lvl = strlen($menu[$i]['level']) - strlen(str_replace('.', '', $menu[$i]['level']));
                            $next = $i;
                            if ($n != $i) {
                                $next++;
                                $lvl2 = strlen($menu[$next]['level']) - strlen(str_replace('.', '', $menu[$next]['level']));
                            }
                            $str = "";
                            $class = '';


                            $link = ($menu[$i]['link'] == '#') ? '#' : base_url() . "page/view/" . $menu[$i]['link'];
                            switch ($lvl) {
                                case 0:
                                    $str .= "<li><a href='$link' class='waves-effect'><i class='" . $menu[$i]['icon'] . "'></i> <span class='hide-menu'>" . $menu[$i]['name'] . "</span></a>";
                                    $class = 'nav nav-second-level collapse';
                                    break;
                                case 1:
                                    $str .= "<li><a  href='$link' class='waves-effect'><span class='hide-menu'>" . $menu[$i]['name'] . "</span></a>";
                                    $class = 'nav nav-third-level collapse';
                                    break;
                                case 2:
                                    $str .= "<li><a  href='$link' class='waves-effect'><span class='hide-menu'>" . $menu[$i]['name'] . "</span></a>";
                                    $class = 'nav nav-empat-level collapse';
                                    break;
                                case 3:
                                    $str .= "<li><a  href='$link' class='waves-effect'><span class='hide-menu'>" . $menu[$i]['name'] . "</span></a>";
                                    break;
                            }
                            echo $str;
                        }
                        ?>
                    </ul>
                </div>
                <!-- sidebar-menu  -->
            </div>
            <!-- sidebar-content  -->
            <div class="sidebar-footer" style="background-image:url('https://skala.analytic.id/assets/landing_pages/img/background/low_poly_background.jpg');background-size:cover;height:40px !important;line-height: 38px;">
                <a href="<?= base_url() ?>page/logout" style="margin-top: 4px;">
                    <i class="fa fa-power-off fa-2x"></i>
                </a>
            </div>
        </nav>

        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <!-- sidebar-wrapper  -->
        <div class="header-banner" style="background-image:url('https://skala.analytic.id/assets/landing_pages/img/background/low_poly_background.jpg');background-size:cover;height:70px !important;line-height: 38px;">
            <ul class="right">
                <!-- Example single danger button -->
                <div class="btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Hello <?= $this->session->nama; ?> 
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?= base_url() ?>page/view/profile">Show Profile</a>
                        <a class="dropdown-item" href="<?= base_url() ?>page/logout">Logout</a>
                    </div>
                </div>
            </ul>
            <main class="page-content">
                <div class="container-fluid">


                    <style>
                        .right {
                            float: right !important;
                            margin: 15px 15px;
                        }

                        .btn-group,
                        .btn-group-vertical {
                            position: relative;
                            display: -webkit-inline-box;
                            display: -ms-inline-flexbox;
                            display: inline-flex;
                            vertical-align: middle;
                            border: white solid 1px;
                        }

                        .btn-info {
                            color: #fff;
                            background-color: #3498DB;
                            border-color: #17a2b8;
                        }

                        .page-content {
                            background-color: #ffff;
                        }
                    </style>