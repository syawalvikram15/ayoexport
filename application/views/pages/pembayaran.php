<div class="d-sm-flex justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
    <div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                <li class="breadcrumb-item">
                    <a href="index.html">Dashboard</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    Pembayaran
                </li>
            </ol>
        </nav>
        <h4 class="mg-b-0 tx-spacing--1">Please click the whatsapp icon below for Pembayaran transaction.</h4>
    </div>
    <div class="floating-wpp"></div>

</div>

<script>

$(function () {
  $('.floating-wpp').floatingWhatsApp({
    phone: '+6283832328001',
    popupMessage: 'Please type your text below.',
    showPopup: true,
    message: 'Hello ayoexport, I want to make transaction Pembayaran for ....',
    headerTitle: 'Pembayaran Transaction'
  });
});
</script>

<style>
.container, .container-fluid {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    width: 100vw;
    height: 100vh;
}
</style>
