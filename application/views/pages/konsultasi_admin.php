<div class="container" style="width:100% !important">
   <div class="main-body">
      <div class="d-sm-flex justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
         <div>
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                  <li class="breadcrumb-item">
                     <a href="index.html">Dashboard</a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">
                     Konsultasi Admin
                  </li>
               </ol>
            </nav>
         </div>
      </div>
      <!-- Nav tabs -->
      <div class="row">
         <div class="col-md-12">
            <ul class="nav nav-tabs" role="tablist">
               <li role="presentation" class="active">
                  <a href="#1" aria-controls="1" role="tab" data-toggle="tab">On Progress</a>
               </li>
               <li role="presentation">
                  <a href="#2" aria-controls="2" role="tab" data-toggle="tab">Done</a>
               </li>
               <li role="presentation">
                  <a href="#3" aria-controls="3" role="tab" data-toggle="tab">Rejected</a>
               </li>
               <li role="presentation">
                  <a href="#4" aria-controls="4" role="tab" data-toggle="tab">Notification</a>
               </li>
            </ul>
         </div>
         <div class="col-md-12">
            <!-- Tab panes -->
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane in active" id="1">
                  <br>
                  <div class="row" style="margin : 15px;">
                     <div class="table-responsive">
                        <table class="table table-hover table-striped table-bordered w-100" id="data-table"></table>
                     </div>
                  </div>
               </div>
               <div role="tabpanel" class="tab-pane" id="2">
                  <br>
                  <div class="row" style="margin : 15px;">
                     <div class="table-responsive">
                        <table class="table table-hover table-striped table-bordered w-100" id="data-table2"></table>
                     </div>
                  </div>
               </div>
               <div role="tabpanel" class="tab-pane" id="3">
                  <br>
                  <div class="row" style="margin : 15px;">
                     <div class="table-responsive">
                        <table class="table table-hover table-striped table-bordered w-100" id="data-table3"></table>
                     </div>
                  </div>
               </div>
               <div role="tabpanel" class="tab-pane" id="4">
                  <br>
                  <div class="row" style="margin : 15px;">
                     <div id="notification-admin" style="width: 100%;"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="show_detail" tabindex="-1" role="dialog" aria-labelledby="show_detail" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title custom_align" id="Heading">Detail Request</h4>
            <button type="button" id="close-modal-detail" class="close" data-dismiss="modal" aria-hidden="true">
            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
            </button>
         </div>
         <div class="modal-body">
            <div class="col-md-6">
               <div class="form-group">
                  <label for="exampleInputEmail1">Nama Perusahaan</label>
                  <h5 class="font-detail">
                     <span id="detail_nama"> - </span> 
                  </h5>
                  <hr>
               </div>
               <div class="form-group">
                  <label for="exampleInputEmail1">Nama Barang</label>
                  <h5 class="font-detail">
                     <span id="detail_nama_produk">-</span> 
                  </h5>
                  <hr>
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label for="exampleInputEmail1">Asal Produk</label>
                  <h5 class="font-detail">
                     <span id="detail_asal_produk">-</span> 
                  </h5>
                  <hr>
               </div>
               <div class="form-group">
                  <label for="exampleInputEmail1">Target Pasar</label>
                  <h5 class="font-detail">
                     <span id="detail_target_pasar">-</span> 
                  </h5>
                  <hr>
               </div>
               <div class="form-group">
                  <label for="exampleInputEmail1">Tanggal Request </label>
                  <h5 class="font-detail">
                     <span id="detail_tanggal_pengiriman">-</span> 
                  </h5>
                  <hr>
               </div>
            </div>
         </div>
         <div id="modal-footer">
            <div class="modal-footer">
               <button type="button" class="btn btn-primary btn-lg" data-toggle="collapse"  data-target="#offer_message">
               <span class="glyphicon glyphicon-ok-sign"></span> Send Offer
               </button>
               <button type="button" class="btn btn-warning btn-lg" data-toggle="collapse"  data-target="#reject_message">
               <span class="glyphicon glyphicon-ok-sign"></span> Reject
               </button>
               <button type="button" class="btn btn-success btn-lg"  onclick="transaction_action('done')">
               <span class="glyphicon glyphicon-ok-sign"></span> Confirm
               </button>
               <br>
            </div>
            <div class="modal-footer" style="border-top: none !important;">
               <div class="collapse" id="reject_message" style="width: 80%;">
                  <div class="form-group">
                     <label for="exampleFormControlTextarea1">Tuliskan alasan direject :</label>
                     <textarea class="form-control" id="input_reject"></textarea>
                  </div>
                  <button type="button" class="btn btn-danger btn-lg" onclick="transaction_action('reject')" value="Request anda ditolak karena">
                  <span class="glyphicon glyphicon-remove"></span> Reject Now
                  </button>   
               </div>
               <div class="collapse" id="offer_message" style="width: 80%;">
                  <div class="form-group">
                     <label for="exampleFormControlTextarea1">Tuliskan Penawaran anda :</label>
                     <textarea class="form-control" id="input_offer"></textarea>
                  </div>
                  <button type="button" class="btn btn-primary btn-lg" onclick="transaction_action('offer')" value="Kami menawarkan.....">
                  <span class="glyphicon glyphicon-ok-sign"></span> Kirim Penawaran
                  </button>   
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<div class="modal fade" id="show_detail_offer" tabindex="-1" role="dialog" aria-labelledby="show_detail" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title custom_align" id="Heading">Detail Penawaran</h4>
            <button type="button" id="close-modal-detail-offer" class="close" data-dismiss="modal" aria-hidden="true">
            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
            </button>
         </div>
         <div class="modal-body">
            <div class="col-md-12">
            <p id="text-detail-offer"> </p>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Here is Icon Loader !!!!!! -->
<div id="loader" class="loader-rm">
   <img src="https://www.drupal.org/files/issues/throbber_12.gif" class="img-gif-rm" alt="Loading..." />
</div>