
    <div class="container-fluid">
        <h2 class="mb-5">Dashboard Overview</h2>
        <div class="header-body">
          <div class="row">
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0"> Market Analisis</h5>
                        <hr>
                        <p class="mt-3 mb-0 text-muted text-sm">
                            <div class="row">
                                <div class="col-xl-4 col-lg-4">
                                     <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-marketanalisis-done">0</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                     <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-marketanalisis-reject">0</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-marketanalisis-on-progress">0</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">Done</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">Rejected</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">On-progress</span>
                                </div>
                            </div>
                        </p>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                        <i class="fas fa-chart-bar"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Sales</h5>
                      <hr>
                        <p class="mt-3 mb-0 text-muted text-sm">
                            <div class="row">
                                <div class="col-xl-4 col-lg-4">
                                     <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-sales-done">0</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                     <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-sales-reject">0</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-sales-on-progress">0</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">Done</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">Rejected</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">On-progress</span>
                                </div>
                            </div>
                        </p>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                        <i class="fas fa-chart-pie"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Pembiayaan</h5>
                      <hr>
                        <p class="mt-3 mb-0 text-muted text-sm">
                            <div class="row">
                                <div class="col-xl-4 col-lg-4">
                                     <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-pembiayaan-done">0</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                     <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-pembiayaan-reject">0</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-pembiayaan-on-progress">0</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">Done</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">Rejected</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">On-progress</span>
                                </div>
                            </div>
                        </p>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                        <i class="fas fa-users"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Asuransi</h5>
                      <hr>
                        <p class="mt-3 mb-0 text-muted text-sm">
                            <div class="row">
                                <div class="col-xl-4 col-lg-4">
                                     <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-asuransi-done">0</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                     <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-asuransi-reject">0</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-asuransi-on-progress">0</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">Done</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">Rejected</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">On-progress</span>
                                </div>
                            </div>
                        </p>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                        <i class="fas fa-percent"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-------- -->
          <br>
          <div class="row">
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Konsultasi Ekspor</h5>
                        <hr>
                        <p class="mt-3 mb-0 text-muted text-sm">
                            <div class="row">
                                <div class="col-xl-4 col-lg-4">
                                     <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-konsultasi-done">0</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                     <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-konsultasi-reject">0</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-konsultasi-on-progress">0</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">Done</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">Rejected</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">On-progress</span>
                                </div>
                            </div>
                        </p>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                        <i class="fas fa-chart-bar"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Regulasi</h5>
                      <hr>
                        <p class="mt-3 mb-0 text-muted text-sm">
                            <div class="row">
                                <div class="col-xl-4 col-lg-4">
                                     <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-regulasi-done">0</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                     <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-regulasi-reject">0</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-regulasi-on-progress">0</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">Done</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">Rejected</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">On-progress</span>
                                </div>
                            </div>
                        </p>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                        <i class="fas fa-chart-pie"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Logistik</h5>
                      <hr>
                        <p class="mt-3 mb-0 text-muted text-sm">
                            <div class="row">
                                <div class="col-xl-4 col-lg-4">
                                     <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-logistik-done">0</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                     <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-logistik-reject">0</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-logistik-on-progress">0</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">Done</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">Rejected</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">On-progress</span>
                                </div>
                            </div>
                        </p>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                        <i class="fas fa-users"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Sertifikasi</h5>
                      <hr>
                        <p class="mt-3 mb-0 text-muted text-sm">
                            <div class="row">
                                <div class="col-xl-4 col-lg-4">
                                     <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-sertifikasi-done">0</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                     <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-sertifikasi-reject">0</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="h2 font-weight-bold mb-0" style="color: black;" id="card-sertifikasi-on-progress">0</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">Done</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">Rejected</span>
                                </div>
                                <div class="col-xl-4 col-lg-4">
                                    <span class="text-nowrap text-muted">On-progress</span>
                                </div>
                            </div>
                        </p>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                        <i class="fas fa-percent"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xl-8 col-lg-8">
                    <div id="container-line-chart" style="width:100%;width: 100% !important;overflow: hidden;border: 1px solid #ccc;border-radius: .375rem;background-color: #fff;background-clip: border-box;"></div>
            </div>
            <div class="col-xl-4 col-lg-4">
                     <div id="container-pie-chart" style="width:100%;width: 100% !important;overflow: hidden;border: 1px solid #ccc;border-radius: .375rem;background-color: #fff;background-clip: border-box;"></div>
            </div>
        </div>

    </div>
