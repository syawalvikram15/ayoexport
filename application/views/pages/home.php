<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Ayo export</title>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <link rel="stylesheet" href="<?= base_url() ?>asset/zeedapp/assets/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?= base_url() ?>asset/zeedapp/css/jquery-ui.css">
      <link rel="stylesheet" href="<?= base_url() ?>asset/zeedapp/assets/css/font-awesome.min.css">
      <link rel="stylesheet" href="<?= base_url() ?>asset/zeedapp/assets/css/owl.carousel.min.css">
      <link rel="stylesheet" href="<?= base_url() ?>asset/zeedapp/assets/css/slicknav.min.css">
      <link rel="stylesheet" href="<?= base_url() ?>asset/zeedapp/assets/css/magnificpopup.css">
      <link rel="stylesheet" href="<?= base_url() ?>asset/zeedapp/assets/css/typography.css">
      <link rel="stylesheet" href="<?= base_url() ?>asset/zeedapp/assets/css/style.css">
      <link rel="stylesheet" href="<?= base_url() ?>asset/zeedapp/assets/css/responsive.css">
      <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>asset/zeedapp/assets/img/icon/icon.png">
      <link rel="stylesheet" href="<?= base_url() ?>asset/css/floating-wpp.min.css">
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div id="preloader">
         <div class="spinner"></div>
      </div>
      <header id="header">
         <div class="header-area">
            <div class="container">
               <div class="row">
                  <div class="menu-area">
                     <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="logo">
                           <a href="<?= base_url() ?>">
                              <img src="<?= base_url() ?>asset/zeedapp/assets/img/icon/icon.png" alt="Ayoexport" style="height: 70px;">
                              <!-- <h1 style="color: #fff;"> Ayoexport.com</h1> -->
                           </a>
                        </div>
                     </div>
                     <div class="col-md-9 hidden-xs hidden-sm">
                        <div class="main-menu">
                           <nav class="nav-menu">
                              <ul>
                                 <li class="active"><a href="#home">Beranda</a></li>
                                 <li><a href="#feature">Fitur</a></li>
                                 <li><a href="http://product.ayoexport.com">Produk</a></li>
                                 <li><a href="#about">Tentang kami</a></li>
                                 <li><a href="#contact">Kontak</a></li>
                                 <li><a href="<?= base_url() ?>no_access">Masuk</a></li>
                              </ul>
                           </nav>
                        </div>
                     </div>
                     <div class="col-sm-12 col-xs-12 visible-sm visible-xs">
                        <div class="mobile_menu"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </header>
      <section id="home" class="slider-area image-background parallax" data-speed="3" data-bg-image="<?= base_url() ?>asset/zeedapp/assets/img/slider-bg.png">
         <div class="container">
            <div class="col-md-6 col-sm-6 hidden-xs">
               <div class="row">
                  <div class="slider-img">
                     <!-- <img src="<?= base_url() ?>asset/zeedapp/assets/img/slider-left-img.png" alt="slider image"> -->
                  </div>
               </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
               <div class="row">
                  <div class="slider-inner text-right">
                  
                     <h2>Market Place Serta Virtual Asisten</h2>
                     <h5>Membantu Anda Melakukan Ekspor Ke Pasar Internasional</h5>
                     <a  href="<?= base_url() ?>chatbot">
                        <!-- <i class="fa fa-whatsapp"></i> -->
                        Mulai Ekspor
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <div class="service-area">
         <div class="container">
            <div class="row" style="margin-top:20px;">
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="service-single">
                     <img src="<?= base_url() ?>asset/zeedapp/assets/img/service/easy.png" alt="service image">
                     <h2>Asistensi</h2>
                     <p>Membantu Anda untuk melakukan ekspor dengan memberi konsultasi, analisa pasar dan informasi regulasi ekspor.</p>
                  </div>
               </div>
               <div class="col-md-4 col-sm-4 col-xs-12 col-6">
                  <div class="service-single">
                     <img src="<?= base_url() ?>asset/zeedapp/assets/img/service/sales.jpg" alt="service image">
                     <h2>Sales</h2>
                     <p>Kami membantu meningkatkan penjualan produk Anda di pasar internasional melalui Market Place dan juga kemitraan strategis. </p>
                  </div>
               </div>
               <div class="col-md-4 col-sm-4 col-xs-12 col-6">
                  <div class="service-single">
                     <img src="<?= base_url() ?>asset/zeedapp/assets/img/service/rekomendasi.jpg" alt="service image">
                     <h2>Rekomendasi</h2>
                     <p> Virtual asisten kami membantu memberikan rekomendasi logistik, surveyor, asuransi, pembayaran ekspor dan pembiayaan ekspor. </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="about-area ptb--100" id="about">
         <div class="container">
            <div class="section-title">
               <h2>Tentang Ayoexport</h2>
               <p>Kami percaya bahwa ekspor bisa menciptakan dampak serta keberlanjutan ekonomi, membuka lapangan pekerjaan dan meningkatkan daya saing global bagi generasi ke generasi.</p>
            </div>
            <div class="row d-flex flex-center">
               <div class="col-md-6 col-sm-6 hidden-xs">
                  <div class="about-left-img">
                     <img src="<?= base_url() ?>asset/zeedapp/assets/img/about/foto ayoexport.png" alt="image">
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-12 d-flex flex-center">
                  <div class="about-content">
                     <h2> TENTANG KAMI </h2>
                     <br>
                     <p>
                     Ayoexport.com merupakan layanan Market Place dan virtual assistant yang membantu pelaku usaha untuk melakukan ekspor dengan lebih mudah, efisien dan terintegrasi melalui aplikasi website. 
                     </p>
                     <p>
                     Kami hadir sebagai solusi atas tantangan dalam proses ekspor yang dihadapi oleh pelaku usaha dimana proses ekspor sebelumnya adalah kompleks, tidak efisien dan tidak terintegrasi. Dengan solusi ini pelaku usaha dapat lebih mudah melakukan ekspor, menghemat biaya dan juga waktu serta mendorong kemandirian bagi pelaku usaha untuk melakukan ekspor. Dimana pada setiap proses ekspor dapat dibantu didalam satu aplikasi. Fokus kami adalah membantu meningkatkan keuntungan perusahaan, meningkatkan akses ke pasar internasional dan membuka lapangan usaha yang lebih luas. Layanan yang kami berikan meliputi virtual assistant, pengembangan pasar, market place dan kemitraan strategis.
                     </p>
                     <h2> VISI KAMI </h2>
                     <br>
                     <p>Membuat ekspor menjadi mudah dan berkelanjutan. </p>
                     <h2> MISI KAMI </h2>
                     <br>
                     <p>
                        Memberikan solusi untuk membuat proses ekspor menjadi mudah, efisen, dan terintegrasi.
                        Memberikan solusi untuk meningkatkan penjualan produk ke pasar internasional.
                        Dan menciptakan hubungan jangka panjang yang saling menguntungkan.
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="video-area ptb--100">
         <div class="container">
            <div class="row">
               <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">
                  <h2>Video Profil</h2>
                  <p>Berikut adalah profil singkat ayoexport</p>
                  <a class="expand-video" href="https://www.youtube.com/watch?v=joB6LUPKYpE&ab_channel=Alfamart"><i class="fa fa-play"></i></a>
               </div>
            </div>
         </div>
      </div>
      <div class="service-area" style="margin-top:80px !important;margin-bottom:80px !important;">
         <div class="container">
         <div class="section-title">
               <h2>Why Us?</h2>
            </div>
      <div class="row">
         <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="service-single">
               <img src="<?= base_url() ?>asset/zeedapp/assets/img/service/supplier.jpg" alt="service image">
               <h2>Terintegrasi</h2>
               <p>Proses ekspor yang terintegrasi di sistem kami membantu proses ekspor menjadi lebih mudah.</p>
            </div>
         </div>
         <div class="col-md-4 col-sm-4 col-xs-12 col-6">
            <div class="service-single">
               <img src="<?= base_url() ?>asset/zeedapp/assets/img/service/eficient.jpg" alt="service image">
               <h2>Efisien</h2>
               <p>Hemat lebih banyak biaya dan waktu Anda dengan proses ekspor yang lebih efisien.</p>
            </div>
         </div>
         <div class="col-md-4 col-sm-4 col-xs-12 col-6">
            <div class="service-single">
               <img src="<?= base_url() ?>asset/zeedapp/assets/img/service/chatbot.jpg" alt="service image">
               <h2>Mudah</h2>
               <p>Mulai ekspor sekarang juga secara mandiri bersama virtual asisten kami yang bisa diakses kapan dan dimana saja.</p>
            </div>
         </div>
      </div>
      </div>
      </div>
      <section class="feature-area bg-gray ptb--100" id="feature">
         <div class="container">
            <div class="section-title">
               <h2>Fitur</h2>
               <p>Proses ekspor terintegrasi ke dalam satu sistem, temukan masalah ekspor Anda dan virtual asisten kami siap membantu.</p>
            </div>
            <div class="row">
               <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="ft-content rtl">
                     <div class="ft-single">
                        <img src="<?= base_url() ?>asset/zeedapp/assets/img/icon/feature/1.png" alt="icon">
                        <div class="meta-content">
                           <h2>Konsultasi Ekspor</h2>
                           <p>Virtual assistant kami siap membantu untuk pertanyaan terkait ekspor yang anda inginkan. Virtual assistant kami dapat diakses 24/7</p>
                        </div>
                     </div>
                     <div class="ft-single">
                        <img src="<?= base_url() ?>asset/zeedapp/assets/img/icon/feature/2.png" alt="icon">
                        <div class="meta-content">
                           <h2>Market Analisis</h2>
                           <p>Anda dapat mengetahui bagaimana daya saing produk yang Anda miliki di pasar internasional. </p>
                        </div>
                     </div>
                     <div class="ft-single">
                        <img src="<?= base_url() ?>asset/zeedapp/assets/img/icon/feature/3.png" alt="icon">
                        <div class="meta-content">
                           <h2>Regulasi</h2>
                           <p>Dapatkan informasi terkait regulasi dalam melakukan ekspor suatu produk dengan tujuan Negara tertentu.</p>
                        </div>
                     </div>
                     <div class="ft-single">
                        <img src="<?= base_url() ?>asset/zeedapp/assets/img/icon/feature/3.png" alt="icon">
                        <div class="meta-content">
                           <h2>Pembiayaan</h2>
                           <p>Virtual asisten kami membantu usaha Anda yang menghadapi masalah dalam pembiayaan ekspor. Virtual assistant kami memberikan rekomendasi pembiayaan yang bisa anda ajukan.</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 hidden-sm col-xs-12">
                  <div class="ft-screen-img" style="margin-top:105px;">
                     <img src="<?= base_url() ?>asset/zeedapp/assets/img/mobile/ft-screen-img.png" alt="image">
                  </div>
                  <div class="ft-single" style="margin-top:55px;">
                        <!-- <div class="meta-content">
                        <img src="<?= base_url() ?>asset/zeedapp/assets/img/icon/feature/3.png" alt="icon"> 
                           <h2>Sertifikasi</h2>
                           <p>Virtual asisten kami membantu Anda mendapatkan layanan sertifikasi terpercaya untuk kebutuhan expor Anda.</p> 
                        </div> -->
                  </div>
               </div>
               <div class="col-md-4 col-sm-6 col-xs-12">
                     
                  <div class="ft-content">
                     <div class="ft-single">
                        <img src="<?= base_url() ?>asset/zeedapp/assets/img/icon/feature/4.png" alt="icon">
                        <div class="meta-content">
                           <h2>Sales</h2>
                           <p>Virtual assistant kami menyediakan market place yang membantu Anda dalam hal penjualan produk ke pasar Internasional.</p>
                        </div>
                     </div>
                     <div class="ft-single">
                     <img src="<?= base_url() ?>asset/zeedapp/assets/img/icon/feature/3.png" alt="icon">
                        <!-- <img src="<?= base_url() ?>asset/zeedapp/assets/img/icon/feature/5.png" alt="icon"> -->
                        <div class="meta-content">
                        <h2>Sertifikasi</h2>
                           <p>Virtual asisten kami membantu Anda mendapatkan layanan sertifikasi terpercaya untuk kebutuhan expor Anda.</p>
                           <!-- <h2>Pembayaran</h2>
                           <p>Virtual asisten kami membantu anda dalam menentukan jenis pembayaran yang tepat yang bisa anda gunakan.</p> -->
                        </div>
                     </div>
                     <div class="ft-single">
                        <img src="<?= base_url() ?>asset/zeedapp/assets/img/icon/feature/6.png" alt="icon">
                        <div class="meta-content">
                           <h2>Logistik</h2>
                           <p>Virtual asisten kami menghubungkan Anda dengan penyedia logistic dan memberikan rekomendasi untuk logistik yang bisa Anda gunakan dalam melakukan ekspor.</p>
                        </div>
                     </div>
                     <div class="ft-single">
                        <img src="<?= base_url() ?>asset/zeedapp/assets/img/icon/feature/6.png" alt="icon">
                        <div class="meta-content">
                           <h2>Asuransi</h2>
                           <p>Virtual asisten kami menghubungkan Anda dengan layanan asuransi ekspor dan memberikan rekomendasi untuk asuransi ekspor yang bisa Anda gunakan dalam melakukan ekspor.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <div class="achivement-area ptb--100">
         <div class="container">
            <div class="row">
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="ach-single">
                     <div class="icon"><i class="fa fa-users"></i></div>
                     <p><span class="counter">120</span></p>
                     <h5>Negara</h5>
                  </div>
               </div>
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="ach-single">
                     <div class="icon"><i class="fa fa-book"></i></div>
                     <span class="counter">7.200</span>
                     <h5>Buyer</h5>
                  </div>
               </div>
               <!-- <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="ach-single">
                     <div class="icon"><i class="fa fa-coffee"></i></div>
                     <p><span class="counter">150</span>k</p>
                     <h5>Kegiatan amal</h5>
                  </div>
               </div> -->
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="ach-single">
                     <div class="icon"><i class="fa fa-trophy"></i></div>
                     <span class="counter">1</span>
                     <h5>Memenangkan penghargaan</h5>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="clinet-area bg-gray ptb--100">
         <div class="container">
            <div class="client-carousel owl-carousel">
               <!-- <img src="<= base_url() ?>asset/zeedapp/assets/img/client/client-img.png" alt="client image"> -->
               <img src="<?= base_url() ?>asset/zeedapp/assets/img/client/client-img1.png" alt="client image" style="height: 115px;">
               <img src="<?= base_url() ?>asset/zeedapp/assets/img/client/client-img2.jpg" alt="client image">
               <img src="<?= base_url() ?>asset/zeedapp/assets/img/client/client-img1.png" alt="client image" style="height: 115px;">
               <img src="<?= base_url() ?>asset/zeedapp/assets/img/client/client-img2.jpg" alt="client image">
               <img src="<?= base_url() ?>asset/zeedapp/assets/img/client/client-img1.png" alt="client image" style="height: 115px;">
               <img src="<?= base_url() ?>asset/zeedapp/assets/img/client/client-img2.jpg" alt="client image">
               <!-- <img src="<= base_url() ?>asset/zeedapp/assets/img/client/client-img3.png" alt="client image">
               <img src="<= base_url() ?>asset/zeedapp/assets/img/client/client-img1.png" alt="client image"> -->
            </div>
         </div>
      </div>
      <section class="contact-area ptb--100" id="contact">
         <div class="container">
            <div class="section-title">
               <h2>Hubungi Kami</h2>
               <!-- <p></p> -->
            </div>
            <div class="row">
               <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="contact-form">
                     <form action="#">
                        <input type="text" name="name" placeholder="Enter Your Name">
                        <input type="text" name="email" placeholder="Enter Your Email">
                        <textarea name="msg" id="msg" placeholder="Your Message "></textarea>
                        <input type="submit" value="Send" id="send">
                     </form>
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="contact_info">
                     <div class="s-info">
                        <i class="fa fa-map-marker"></i>
                        <div class="meta-content">
                           <span>Patra jasa Office Tower Kav. 32, Jl. Gatot Subroto No.17, RT.6/RW.3, Kuningan Tim., </span>
                           <span>Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950, Indonesia</span>
                        </div>
                     </div>
                     <div class="s-info">
                        <i class="fa fa-mobile"></i>
                        <div class="meta-content">
                           <span>+62 87794418050</span>
                        </div>
                     </div>
                     <div class="s-info">
                        <i class="fa fa-paper-plane"></i>
                        <div class="meta-content">
                           <span><a href="index.html" class="__cf_email__" data-cfemail="3d6e484d4d524f497d5952505c5453135e5250">admin@ayoexport.com</a></span>
                         
                        </div>
                     </div>
                     <div class="c-social">
                        <ul>
                           <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                           <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                           <li><a href="#"><i class="fa fa-behance"></i></a></li>
                           <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <div class="floating-wpp"></div>
      <footer>
         <div class="footer-area">
            <div class="container">
               <p>
                  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Ayoexport.com
               </p>
            </div>
         </div>
      </footer>
      <script src="<?= base_url() ?>asset/zeedapp/assets/js/jquery-3.2.0.min.js"></script>
      <script src="<?= base_url() ?>asset/zeedapp/assets/js/jquery-ui.js"></script>
      <script src="<?= base_url() ?>asset/zeedapp/assets/js/bootstrap.min.js"></script>
      <script src="<?= base_url() ?>asset/zeedapp/assets/js/jquery.slicknav.min.js"></script>
      <script src="<?= base_url() ?>asset/zeedapp/assets/js/owl.carousel.min.js"></script>
      <script src="<?= base_url() ?>asset/zeedapp/assets/js/magnific-popup.min.js"></script>
      <script src="<?= base_url() ?>asset/zeedapp/assets/js/counterup.js"></script>
      <script src="<?= base_url() ?>asset/zeedapp/assets/js/jquery.waypoints.min.js"></script>
      <script src="<?= base_url() ?>asset/zeedapp/assets/js/jquery.mb.ytplayer.min.js"></script>
      <script src="<?= base_url() ?>asset/zeedapp/assets/js/theme.js"></script>
      <script async src="gtag/js_id_ua-23581568-13.js"></script>
      <script src="<?= base_url() ?>asset/js/floating-wpp.min.js"></script>
      <script>
         $(function () {
           $('.floating-wpp').floatingWhatsApp({
             phone: '+6287794418050',
             popupMessage: 'Please type your text below.',
             showPopup: true,
             message: 'Hello ayoexport, I want to ....',
             headerTitle: 'Contact Us'
           });
         });
      </script>
      <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());
         gtag('config', 'UA-23581568-13');
      </script>
   </body>
</html>


