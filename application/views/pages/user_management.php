

<div class="row">
   <div class="col-md-12">
      <h1 class="text-blue font-weight-bold m-t-45 m-b-20 lh-15">User Management</h1>
   </div>
</div>
<div class="row">
   <div class="col-md-12">
      <div class="white-box">
         <div class="row">
            <div class="table-responsive">
               <table class="table table-hover table-striped table-bordered w-100" id="data-table"></table>
            </div>
         </div>
      </div>
   </div>
   <div class="col-md-12">
      <div class="white-box">
         <h3 class="box-title">Menu Management</h3>
         <div class="table-responsive">
            <form action="<?= base_url() ?>index.php/page/user_level" method="post">
               <table class="table">
                  <tr>
                     <th>Menu</th>
                     <?php foreach ($levels as $l) {
                        echo "<th>$l[level]</th>";
                        } ?>
                  </tr>
                  <?php
                     $lvl = 0;
                     $userm_arr = array();
                     foreach ($usermenu as $u) {
                         $userm_arr[$u['level']] = $u['levels'];
                     }
                     foreach ($menus as $m) {
                         $lvl = substr_count($m['level'], '.');
                         echo "<tr><td>" . str_repeat('-', $lvl) . "$m[name]</td>";
                         foreach ($levels as $l) {
                             $ck = '';
                             if (in_array($m['id_menu'], explode(',', $userm_arr[$l['level']]))) {
                                 $ck = 'checked';
                             }
                             echo "<td><input type='checkbox' name='$l[level][]' value='$m[id_menu]' $ck/></td>";
                         }
                         echo "</tr>";
                     }
                     ?>
               </table>
               <input type='submit' value='simpan' class='btn btn-primary' />
            </form>
         </div>
      </div>
   </div>
</div>
<!-- /.modal -->
<div class="modal fade" id="modal-edit">
   <div class="modal-dialog modal-lg">
      <div class="modal-content bg-secondary">
         <div class="modal-header">
            <h4 class="modal-title">Detail User</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
         </div>
         <div class="modal-body">
            <table class="table">
               <thead>
                  <tr>
                     <th>Informasi</th>
                     <th>Nilai</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <th scope="row">Nama</th>
                     <td><span id="nama-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Level</th>
                     <td><span id="level-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Email</th>
                     <td><span id="email-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">HP</th>
                     <td><span id="hp-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Website</th>
                     <td><span id="website-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Alamat</th>
                     <td><span id="alamat-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Kota</th>
                     <td><span id="city-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Provinsi</th>
                     <td><span id="province-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Jenis usaha</th>
                     <td><span id="jen-usaha-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Jenis Produk</th>
                     <td><span id="jen-produk-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Daftar Produk</th>
                     <td><span id="daftar-produk-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Kapasitas Produk</th>
                     <td><span id="kapasitas-produk-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Satuan Produk</th>
                     <td><span id="satuan-produk-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Gudang</th>
                     <td><span id="gudang-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Pabrik</th>
                     <td><span id="pabrik-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Lama Produk</th>
                     <td><span id="lama-produk-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Kendala</th>
                     <td><span id="kendala-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Tahun berdiri</th>
                     <td><span id="tahun-berdiri-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Pernah Ekspor</th>
                     <td><span id="ekspor-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Negara Tujuan Ekspor Sebelumnya</th>
                     <td><span id="negara-tujuan-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Jumlah Karyawan</th>
                     <td><span id="jumlah-karyawan-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Sertifikasi</th>
                     <td><span id="sertifikasi-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">OEM</th>
                     <td><span id="oem-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Term Pembayaran</th>
                     <td><span id="term-pembayaran-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Term Harga</th>
                     <td><span id="term-harga-edit"> </span></td>
                  </tr>
                  <tr>
                     <th scope="row">Logistik</th>
                     <td><span id="logistik-edit"> </span></td>
                  </tr>
               </tbody>
            </table>
            <div class="modal-footer justify-content-between">
               <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
</div>
<!-- Here is Icon Loader !!!!!! -->
<div id="loader" class="loader-rm">
   <img src="https://www.drupal.org/files/issues/throbber_12.gif" class="img-gif-rm" alt="Loading..." />
</div>
<style>
   .img-gif-rm {
   position: fixed;
   top: 50%;
   left: 50%;
   width: 75px;
   height: 75px;
   }
   .show {
   display: block;
   }
</style>
