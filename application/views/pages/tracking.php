<div class="d-sm-flex justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
    <div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                <li class="breadcrumb-item">
                    <a href="index.html">Dashboard</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    Tracking
                </li>
            </ol>
        </nav>
        <h4 class="mg-b-0 tx-spacing--1">Dashboard Analytics User Tracking</h4>
    </div>
    <div class="mg-t-10 d-flex justify-content-end">
        <button class="btn btn-sm pd-x-15 btn-white btn-uppercase" id="reportrange"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar wd-10 mg-r-5">
                <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                <line x1="16" y1="2" x2="16" y2="6"></line>
                <line x1="8" y1="2" x2="8" y2="6"></line>
                <line x1="3" y1="10" x2="21" y2="10"></line>
            </svg>21 January 2020 to TODAY</button>
    </div>
</div>
<?= $this->session->flashdata('msg') ?>
<div class="row mt-4">
    <div class="col-md-12">
        <div class="white-box">
            <table id="customers2" class="mt-3 mb-3">
                <thead>
                    <tr>
                        <th>
                            <center>No
                        </th>
                        <th>
                            <center>ID Tracker
                        </th>
                        <th>
                            <center>Time IN
                        </th>
                        <th>
                            <center>Time OUT
                        </th>
                        <th>
                            <center>IP
                        </th>
                        <th>
                            <center>Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    if (!empty($tracking)) {
                        foreach ($tracking as $data) {
                            echo "<tr>";
                            echo "<td><center>" . $no++ . "</td>";
                            echo "<td><center>" . $data['id_tracker'] . "</td>";
                            echo "<td><center>" . $data['time_in'] . "</td>";
                            echo "<td><center>" . $data['time_out'] . "</td>";
                            echo "<td><center>" . $data['ip_addr'] . "</td>";
                    ?>
                            <td>
                                <center>
                                    <a href="<?= base_url();  ?>page/tracking_detail/<?php echo $data['id'] ?>" class="btn btn-success btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <a href="<?= base_url();  ?>page/tracking_edit/<?php echo $data['id'] ?>" class="btn btn-warning btn-sm"><i class="fa fa-pencil text-white" aria-hidden="true"></i></a>
                                    <a href="<?= base_url();  ?>page/tracking_delete/<?php echo $data['id'] ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </td>
                    <?php echo "</tr>";
                        }
                    } else {
                        echo "No data";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
$this->load->view('templates/footer');
