<div class="row" style="margin-top : 50px; ">
    <div class="col-md-12">
        <div class="white-box" style="width:100%;">
            <div class="row mb-3" style="margin-top:20px;">
                <h5> Dashboard / Tracking / Tracking Detail </h5>
            </div>
        </div>
    </div>
</div>

<?= $this->session->flashdata('msg') ?>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <table class="table">
                <thead>
                    <th width="30%">Name</th>
                    <th>Value</th>
                </thead>
                <tbody>
                    <?php foreach ($tracking as $data) { ?>
                        <tr>
                            <td>ID</td>
                            <td><strong>: <?php echo $data->id ?></strong></td>
                        </tr>
                        <tr>
                            <td>ID Tracking</td>
                            <td><strong>: <?php echo $data->id_tracker ?></strong></td>
                        </tr>
                        <tr>
                            <td>Time In</td>
                            <td><strong>: <?php echo $data->time_in ?></strong></td>
                        </tr>
                        <tr>
                            <td>Time Out</td>
                            <td><strong>: <?php echo $data->time_out ?></strong></td>
                        </tr>
                        <tr>
                            <td>IP Address</td>
                            <td><strong>: <?php echo $data->ip_addr ?></strong></td>
                        </tr>
                        <tr>
                            <td>Screen X</td>
                            <td><strong>: <?php echo $data->screen_x ?></strong></td>
                        </tr>
                        <tr>
                            <td>Screen y</td>
                            <td><strong>: <?php echo $data->screen_y ?></strong></td>
                        </tr>
                        <tr>
                            <td>User Agent</td>
                            <td><strong>: <?php echo $data->user_agent ?></strong></td>
                        </tr>
                        <tr>
                            <td>OS Family</td>
                            <td><strong>: <?php echo $data->os_family ?></strong></td>
                        </tr>
                        <tr>
                            <td>OS Version</td>
                            <td><strong>: <?php echo $data->os_version ?></strong></td>
                        </tr>
                        <tr>
                            <td>Device Brand</td>
                            <td><strong>: <?php echo $data->device_brand ?></strong></td>
                        </tr>
                        <tr>
                            <td>Device Model</td>
                            <td><strong>: <?php echo $data->device_model ?></strong></td>
                        </tr>
                        <tr>
                            <td>ID CFS</td>
                            <td><strong>: <?php echo $data->id_cfs ?></strong></td>
                        </tr>
                        <tr>
                            <td>ID PUB</td>
                            <td><strong>: <?php echo $data->id_pub ?></strong></td>
                        </tr>
                        <tr>
                            <td>Referer</td>
                            <td><strong>: <?php echo $data->referer ?></strong></td>
                        </tr>
                        <tr>
                            <td>Domain</td>
                            <td><strong>: <?php echo $data->domain ?></strong></td>
                        </tr>
                        <tr>
                            <td>Path</td>
                            <td><strong>: <?php echo $data->path ?></strong></td>
                        </tr>
                        <tr>
                            <td>Query Fragment</td>
                            <td><strong>: <?php echo $data->query_fragment ?></strong></td>
                        </tr>
                        <tr>
                            <td>DS</td>
                            <td><strong>: <?php echo $data->ds ?></strong></td>
                        </tr>
                        <tr>
                            <td>Geo Code</td>
                            <td><strong>: <?php echo $data->geo_code ?></strong></td>
                        </tr>
                        <tr>
                            <td>Geo Country</td>
                            <td><strong>: <?php echo $data->geo_country ?></strong></td>
                        </tr>
                        <tr>
                            <td>Geo Region</td>
                            <td><strong>: <?php echo $data->geo_region ?></strong></td>
                        </tr>
                        <tr>
                            <td>Geo City</td>
                            <td><strong>: <?php echo $data->geo_city ?></strong></td>
                        </tr>
                        <tr>
                            <td>Geo Lat</td>
                            <td><strong>: <?php echo $data->geo_lat ?></strong></td>
                        </tr>
                        <tr>
                            <td>Geo Long</td>
                            <td><strong>: <?php echo $data->geo_long ?></strong></td>
                        </tr>
                        <tr>
                            <td>Geo ISP</td>
                            <td><strong>: <?php echo $data->geo_isp ?></strong></td>
                        </tr>
                        <tr>
                            <td>Referer Domain</td>
                            <td><strong>: <?php echo $data->referer_domain ?></strong></td>
                        </tr>
                        <tr>
                            <td>Is Mobile</td>
                            <td><strong>: <?php echo $data->is_mobile ?></strong></td>
                        </tr>
                        <tr>
                            <td>Page Title</td>
                            <td><strong>: <?php echo $data->page_title ?></strong></td>
                        </tr>
                        <tr>
                            <td>Browser</td>
                            <td><strong>: <?php echo $data->browser ?></strong></td>
                        </tr>
                        <tr>
                            <td>Browser Version</td>
                            <td><strong>: <?php echo $data->browser_version ?></strong></td>
                        </tr>
                        <tr>
                            <td>Language</td>
                            <td><strong>: <?php echo $data->language ?></strong></td>
                        </tr>
                        <tr>
                            <td>Referer Medium</td>
                            <td><strong>: <?php echo $data->referer_medium ?></strong></td>
                        </tr>
                        <tr>
                            <td>Referer Source</td>
                            <td><strong>: <?php echo $data->referer_source ?></strong></td>
                        </tr>
                        <tr>
                            <td>Referer Term</td>
                            <td><strong>: <?php echo $data->referer_term ?></strong></td>
                        </tr>
                        <tr>
                            <td>MAC Address</td>
                            <td><strong>: <?php echo $data->mac_addr ?></strong></td>
                        </tr>
                        <tr>
                            <td>Last Page</td>
                            <td><strong>: <?php echo $data->lastpage ?></strong></td>
                        </tr>
                        <tr>
                            <td>URL</td>
                            <td><strong>: <?php echo $data->url ?></strong></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>&nbsp;&nbsp;<a href="<?php echo base_url(); ?>page/tracking" class="btn waves-effect waves-light btn-rounded btn-warning"><i class="mdi mdi-arrow-left"></i> Back</a></td>
                        </tr>
                    <?php }  ?>
                </tbody>
            </table>
        </div>
    </div>
</div>