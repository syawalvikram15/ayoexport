<div class="d-sm-flex justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
    <div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                <li class="breadcrumb-item">
                    <a href="index.html">Register</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    Add Web
                </li>
            </ol>
        </nav>
        <h4 class="mg-b-0 tx-spacing--1">Dashboard Analytics User Tracking</h4>
    </div>
    <div class="mg-t-10 d-flex justify-content-end">
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <form class="form-group" method="POST" action="<?= base_url(); ?>page/register_web_act">
                <div class="row">
                    <input type="text" class="form__input" id="id_tracker" name="id_tracker" value="USER-<?php echo sprintf("%04s", $id_tracker) ?>" readonly>
                </div>
                <div class="row">
                    <input type="text" class="form__input" id="id_website" name="id_website" value="<?= $kodeweb; ?>" readonly>
                </div>
                <div class="row">
                    <input type="text" name="website_name" id="website_name" class="form__input" placeholder="Nama Website" required>
                </div>
                <button type="submit" class="btn btn-primary mt-4 mb-3">Submit</button>
            </form>
        </div>
    </div>
</div>
<?php
$this->load->view('templates/footer');
