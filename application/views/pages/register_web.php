<div class="d-sm-flex justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
    <div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                <li class="breadcrumb-item">
                    <a href="index.html">Register</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    Register Web
                </li>
            </ol>
        </nav>
        <h4 class="mg-b-0 tx-spacing--1">Dashboard Analytics User Tracking</h4>
    </div>
    <div class="mg-t-10 d-flex justify-content-end">
    </div>
</div>

<a href="<?= base_url(); ?>page/register_web_add" class="btn btn-primary mt-4 mb-3">Registrasi Website</a>
<?= $this->session->flashdata('msg') ?>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <table id="customers2" class="mt-3 mb-3">
                <thead>
                    <tr>
                        <th>
                            <center>No
                        </th>
                        <th>
                            <center>ID Tracker
                        </th>
                        <th>
                            <center>Website
                        </th>
                        <th>
                            <center>Registrasi
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    if (!empty($user_website)) {
                        foreach ($user_website as $data) {
                            echo "<tr>";
                            echo "<td><center>" . $no++ . "</td>";
                            echo "<td><center>" . $data['id_tracker'] . "</td>";
                            echo "<td><center>" . $data['website_name'] . "</td>";
                            echo "<td><center>" . $data['tgl_registrasi'] . "</td>";
                            echo "</tr>";
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
$this->load->view('templates/footer');
