

<div class="row" style="max-height:70%">
<!-- Nav tabs -->
<div class="tab" role="tabpanel" style="width:100%;margin-left: 15px;">
   <!-- Nav tabs -->
   <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#4" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-th-large"></i>Main Menu</a></li>
      <li role="presentation"><a href="#6" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-bell-o"></i>Notifications</a></li>
   </ul>
   <div class="tab-content">
      <div role="tabpanel" class="tab-pane in active" id="4">
         <div class="row">
            <div class="col-md-4 border-content-main b-s-0">
               <section class="msger">
                  <header class="msger-header">
                     <div class="msger-header-title">
                        Export Asistant 
                     </div>
                  </header>
                  <main class="msger-chat">
                     <div class="msg left-msg">
                        <div class="msg-img" style="background-image: url(https://image.flaticon.com/icons/svg/327/327779.svg)"></div>
                        <div class="msg-bubble">
                           <div class="msg-info">
                              <div class="msg-info-name">Export Asistant</div>
                              <div class="msg-info-time">12:45</div>
                           </div>
                           <div class="msg-text">
                              Hallo Exportir, ada yang dapat saya bantu? 😄
                           </div>
                        </div>
                     </div>
                  </main>
                  <form class="msger-inputarea">
                     <input type="text" class="msger-input" id="textInput" placeholder="Enter your message...">
                     <button type="submit" class="msger-send-btn">Send</button>
                  </form>
               </section>
            </div>
            <div class="col-md-8 border-content-main">
               <header class="msger-header">
                  <div class="msger-header-title">
                     Request
                  </div>
               </header>
               <div class="ovr-auto">
                  <div class="boxContent">
                     <div class="col-md-4 m-0">
                        <a class="btn btn-primary b-t-tab" data-toggle="collapse" href="#1" role="button" aria-expanded="false" aria-controls="collapseExample">
                        On Progress
                        </a>
                     </div>
                     <div class="col-md-4 m-0">
                        <a class="btn btn-primary b-t-tab" data-toggle="collapse" href="#2" role="button" aria-expanded="false" aria-controls="collapseExample">
                        Done
                        </a>
                     </div>
                     <div class="col-md-4 m-0">
                        <a class="btn btn-primary b-t-tab" data-toggle="collapse" href="#3" role="button" aria-expanded="false" aria-controls="collapseExample">
                        Rejected
                        </a>
                     </div>
                     <div class="collapse in"  id="1">
                        <br>
                        <button style="font-size: 1.3rem;margin: 20px;" data-title="Add" data-toggle="modal" data-target="#tambah-transaksi" class="btn btn-rounded btn-success"><i class="fas fa-plus pr-2" aria-hidden="true"></i> Add Request</button>
                        <br>
                        <div class="row border-custom m-10">
                           <h4>Request On Progress</h4>
                           <div class="table-responsive">
                              <table class="table table-hover table-striped table-bordered w-100" id="data-table"></table>
                           </div>
                        </div>
                     </div>
                     <div class="collapse" id="2">
                        <div class="row border-custom m-10">
                           <h4>Completed</h4>
                           <div class="table-responsive">
                              <table class="table table-hover table-striped table-bordered w-100" id="data-table2"></table>
                           </div>
                        </div>
                     </div>
                     <div class="collapse" id="3">
                        <div class="row border-custom m-10">
                           <h4>Rejected</h4>
                           <div class="table-responsive">
                              <table class="table table-hover table-striped table-bordered w-100" id="data-table3"></table>
                           </div>
                        </div>
                     </div>
                     <div class="modal fade" id="tambah-transaksi" tabindex="-1" role="dialog" aria-labelledby="tambah" aria-hidden="true">
                        <div class="modal-dialog">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h4 class="modal-title custom_align" id="Heading">Add Request</h4>
                                 <button type="button" id="close-modal-tambah" class="close" data-dismiss="modal" aria-hidden="true">
                                 <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <form id="formTambah">
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Nama Barang</label>
                                       <input class="form-control" id="nama_produk" type="text" placeholder="Contoh: Jagung">
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Jumlah Produk (satuan dipilih sesuai jumlah produk: kg/ton/pcs/box/liter)</label>
                                       <input class="form-control " id="jumlah_produk" type="text" placeholder="Contoh: 100 liter">
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Volume Produk (Panjang x Lebar x Tinggi)</label>
                                       <div class="row">
                                          <div class="col-md-3">
                                             <input class="form-control " id="volume_produk1" type="text" placeholder="Contoh:10">
                                          </div>
                                          <div class="col-md-1">
                                             x
                                          </div>
                                          <div class="col-md-3">
                                             <input class="form-control " id="volume_produk2" type="text" placeholder="Contoh:10">
                                          </div>
                                          <div class="col-md-1">
                                             x
                                          </div>
                                          <div class="col-md-3">
                                             <input class="form-control " id="volume_produk3" type="text" placeholder="Contoh:10">
                                          </div>
                                          <div class="col-md-1">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Satuan Produk (m/cm/mm)</label>
                                       <input class="form-control " id="satuan_ukuran" type="text" placeholder="Contoh : m">
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Asal Produk</label>
                                       <input class="form-control " id="asal_produk" type="text" placeholder="Contoh : Blitar, Jawa Timur, Indonesia">
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Tujuan Pengiriman</label>
                                       <input class="form-control " id="tujuan_pengiriman" type="text" placeholder="Contoh : Tokyo, Jepang">
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Estimasi Budget (Rupiah)</label>
                                       <input class="form-control " id="estimasi_budget" type="text" placeholder="Contoh : 100.000.000">
                                    </div>
                              </div>
                              <div class="modal-footer ">
                              <button type="button" class="btn btn-warning btn-lg" style="width: 100%;" onclick="add_transaction()">
                              <span class="glyphicon glyphicon-ok-sign"></span> Tambah
                              </button>
                              </div>
                              </form>
                           </div>
                           <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                     </div>
                     <div class="modal fade" id="edit-transaksi" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                        <div class="modal-dialog">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h4 class="modal-title custom_align" id="Heading">Edit Your Detail</h4>
                                 <button type="button" id="close-modal-edit" class="close" data-dismiss="modal" aria-hidden="true">
                                 <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <form id="formTambah">
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Nama Barang</label>
                                       <input class="form-control" id="e_nama_produk" type="text" placeholder="Contoh: Jagung">
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Jumlah Produk (satuan dipilih sesuai jumlah produk: kg/ton/pcs/box/liter)</label>
                                       <input class="form-control " id="e_jumlah_produk" type="text" placeholder="Contoh: 100 liter">
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Volume Produk (Panjang x Lebar x Tinggi)</label>
                                       <div class="row">
                                          <div class="col-md-3">
                                             <input class="form-control " id="e_volume_produk1" type="text" placeholder="Contoh : 10">
                                          </div>
                                          <div class="col-md-1">
                                             x
                                          </div>
                                          <div class="col-md-3">
                                             <input class="form-control " id="e_volume_produk2" type="text" placeholder="Contoh : 10">
                                          </div>
                                          <div class="col-md-1">
                                          </div>
                                          <div class="col-md-1">
                                             x
                                          </div>
                                          <div class="col-md-3">
                                             <input class="form-control " id="e_volume_produk3" type="text" placeholder="Contoh : 10">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Satuan Produk (m/cm/mm)</label>
                                       <input class="form-control " id="e_satuan_ukuran" type="text" placeholder="Contoh : m">
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Asal Produk</label>
                                       <input class="form-control " id="e_asal_produk" type="text" placeholder="Contoh : Blitar">
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Tujuan Pengiriman</label>
                                       <input class="form-control " id="e_tujuan_pengiriman" type="text" placeholder="Contoh : Jepang">
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Estimasi Budget (Rupiah)</label>
                                       <input class="form-control " id="e_estimasi_budget" type="text" placeholder="Contoh : 100.000.000">
                                    </div>
                                 </form>
                              </div>
                              <div class="modal-footer ">
                                 <button type="button" class="btn btn-warning btn-lg" style="width: 100%;" onclick="update_transaction()">
                                 <span class="glyphicon glyphicon-ok-sign"></span> Update
                                 </button>
                              </div>
                           </div>
                           <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                     </div>
                     <div class="modal fade" id="modal-delete-transaksi" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                        <div class="modal-dialog">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                                 <button type="button" id="delete-transaksi" class="close" data-dismiss="modal" aria-hidden="true" >
                                 <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="alert alert-danger">
                                    <span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?
                                 </div>
                              </div>
                              <div class="modal-footer ">
                                 <button type="button" class="btn btn-success" onclick="confirm_delete_transaction()" >
                                 <span class="glyphicon glyphicon-ok-sign"></span> Yes
                                 </button>
                                 <button type="button" class="btn btn-default" data-dismiss="modal">
                                 <span class="glyphicon glyphicon-remove"></span> No
                                 </button>
                              </div>
                           </div>
                           <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div role="tabpanel" class="tab-pane" id="6">
         <div class="ovr-auto-notif">
            <div id="notification-content"> </div>
         </div>
      </div>
   </div>
   <!-- col -->
</div>
<div class="modal fade" id="show_detail" tabindex="-1" role="dialog" aria-labelledby="show_detail" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title custom_align" id="Heading">Detail Transaksi</h4>
            <button type="button" id="close-modal-detail" class="close" data-dismiss="modal" aria-hidden="true">
            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
            </button>
         </div>
         <div class="modal-body">
            <div class="col-md-6">
               <div class="form-group">
                  <label for="exampleInputEmail1">Nama Barang</label>
                  <h5 class="font-detail">
                     <span id="detail_nama_produk">-</span> 
                  </h5>
                  <hr>
               </div>
               <div class="form-group">
                  <label for="exampleInputEmail1">Jumlah Produk</label>
                  <h5 class="font-detail">
                     <span id="detail_jumlah_produk">-</span> 
                  </h5>
                  <hr>
               </div>
               <div class="form-group">
                  <label for="exampleInputEmail1">Volume Produk</label>
                  <h5 class="font-detail">
                     <span id="detail_volume_produk">-</span> 
                  </h5>
                  <hr>
               </div>
               <div class="form-group">
                  <label for="exampleInputEmail1">Satuan Produk</label>
                  <h5 class="font-detail">
                     <span id="detail_satuan_ukuran">-</span> 
                  </h5>
                  <hr>
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label for="exampleInputEmail1">Asal Produk</label>
                  <h5 class="font-detail">
                     <span id="detail_asal_produk">-</span> 
                  </h5>
                  <hr>
               </div>
               <div class="form-group">
                  <label for="exampleInputEmail1">Tujuan Pengiriman</label>
                  <h5 class="font-detail">
                     <span id="detail_tujuan_pengiriman">-</span> 
                  </h5>
                  <hr>
               </div>
               <div class="form-group">
                  <label for="exampleInputEmail1">Estimasi Budget</label>
                  <h5 class="font-detail">
                     <span id="detail_estimasi_budget">-</span> 
                  </h5>
                  <hr>
               </div>
               <div class="form-group">
                  <label for="exampleInputEmail1">Tanggal Transaksi </label>
                  <h5 class="font-detail">
                     <span id="detail_tanggal_pengiriman">-</span> 
                  </h5>
                  <hr>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="show_detail_offer" tabindex="-1" role="dialog" aria-labelledby="show_detail" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title custom_align" id="Heading">Detail Penawaran</h4>
            <button type="button" id="close-modal-detail-offer" class="close" data-dismiss="modal" aria-hidden="true">
            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
            </button>
         </div>
         <div class="modal-body">
            <div class="col-md-12">
              <h3> Hi <?= $this->session->nama; ?></h3> 
               <p id="text-detail-offer"> </p>
               <p> Terima kasih </p>
            </div>
         </div>
         <div class="modal-footer" id="footer-notif-offer">
            <button type="button" class="btn btn-success btn-lg" style="width: 100%;" onclick="confirm_offer('accept')">
            <span class="glyphicon glyphicon-ok-sign"></span> Terima
            </button>
            <button type="button" class="btn btn-warning btn-lg" style="width: 100%;" onclick="confirm_offer('reject')">
            <span class="glyphicon glyphicon-remove"></span> Tolak
            </button>
         </div>
      </div>
   </div>
</div>
<!-- Here is Icon Loader !!!!!! -->
<div id="loader" class="loader-rm">
   <img src="https://www.drupal.org/files/issues/throbber_12.gif" class="img-gif-rm" alt="Loading..." />
</div>
<style>
   .tab {
   margin-top: 30px;
   margin-right: 20px;
   }
   .tab .nav-tabs{
   border:none;
   border-bottom: 1px solid #e4e4e4;
   }
   .nav-tabs li a{
   padding: 15px 40px;
   border:1px solid #ededed;
   border-top: 2px solid #ededed;
   border-right: 0px none;
   background: #337ab7;
   color:#fff;
   border-radius: 0px;
   margin-right: 0px;
   font-weight: bold;
   transition: all 0.3s ease-in 0s;
   }
   .nav-tabs li a:hover{
   border-bottom-color: #ededed;
   border-right: 0px none;
   background: #00b0ad;
   color: #fff;
   }
   .nav-tabs li a i{
   display: inline-block;
   text-align: center;
   margin-right:10px;
   }
   .nav-tabs li:last-child{
   border-right:1px solid #ededed;
   }
   .nav-tabs li.active a,
   .nav-tabs li.active a:focus,
   .nav-tabs li.active a:hover{
   border-top: 3px solid #00b0ad;
   border-right: 1px solid #d3d3d3;
   margin-top: -15px;
   color: #444;
   padding: 22px 40px;
   }
   .tab .tab-content{
   padding: 20px;
   line-height: 22px;
   /* box-shadow:0px 1px 0px #808080; */
   }
   .tab .tab-content h3{
   margin-top: 0;
   }
   @media only screen and (max-width: 767px){
   .nav-tabs li{
   width:100%;
   margin-bottom: 10px;
   }
   .nav-tabs li a{
   padding: 15px;
   }
   .nav-tabs li.active a,
   .nav-tabs li.active a:focus,
   .nav-tabs li.active a:hover{
   padding: 15px;
   margin-top: 0;
   }
   }
</style>

