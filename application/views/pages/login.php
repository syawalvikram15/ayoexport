<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>/asset/zeedapp/assets/img/icon/icon.png">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="Yinka Enoch Adedokun">
	<link href="<?= base_url() ?>asset/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- 
  <link href="<=base_url()?>asset/css/animate.css" rel="stylesheet">
 
  <link href="<=base_url()?>asset/css/style.css" rel="stylesheet">
  <link href="<=base_url()?>asset/css/colors/default.css" id="theme" rel="stylesheet"> -->
	<style>
		#background {
			width: 100%;
			height: 100%;
			position: fixed;
			left: 0px;
			top: 0px;
			z-index: -1;
			/* Ensure div tag stays behind content; -999 might work, too. */
		}

		.stretch {
			width: 100%;
			height: 100%;
		}

		.main-content {
			width: 50%;
			border-radius: 20px;
			box-shadow: 0 5px 5px rgba(0, 0, 0, .4);
			margin: 5em auto;
			display: flex;
		}

		.company__info {
			background-color: #3498DB;
			border-top-left-radius: 20px;
			border-bottom-left-radius: 20px;
			display: flex;
			flex-direction: column;
			justify-content: center;
			color: #fff;
		}

		.fa-android {
			font-size: 3em;
		}

		@media screen and (max-width: 640px) {
			.main-content {
				width: 90%;
			}

			.company__info {
				display: none;
			}

			.login_form {
				border-top-left-radius: 20px;
				border-bottom-left-radius: 20px;
			}
		}

		@media screen and (min-width: 642px) and (max-width:800px) {
			.main-content {
				width: 70%;
			}
		}

		.row>h2 {
			color: #008080;
		}

		.login_form {
			background-color: #fff;
			border-top-right-radius: 20px;
			border-bottom-right-radius: 20px;
			border-top: 1px solid #ccc;
			border-right: 1px solid #ccc;
		}

		form {
			padding: 0 2em;
		}

		.form__input {
			width: 100%;
			border: 0px solid transparent;
			border-radius: 0;
			border-bottom: 1px solid #aaa;
			padding: 1em .5em .5em;
			padding-left: 2em;
			outline: none;
			margin: 1.5em auto;
			transition: all .5s ease;
		}

		.form__input:focus {
			border-bottom-color: #008080;
			box-shadow: 0 0 5px rgba(0, 80, 80, .4);
			border-radius: 4px;
		}

		.btn {
			transition: all .5s ease;
			width: 70%;
			border-radius: 30px;
			color: #008080;
			font-weight: 600;
			background-color: #fff;
			border: 1px solid #008080;
			margin-top: 1.5em;
			margin-bottom: 1em;
		}

		.btn:hover,
		.btn:focus {
			background-color: #008080;
			color: #fff;
		}
		.companyinfo {
			background-color: #fff;
			border-top-left-radius: 20px;
			border-bottom-left-radius: 20px;
			display: flex;
			flex-direction: column;
			justify-content: center;
			color: #fff;
			border-top: 1px solid #ccc;
			border-right: 1px solid #ccc;
		}
	</style>
	<title>Login Page</title>
</head>

<body>
	<div id="background">
		<img src="<?= base_url() ?>asset/login_img.png" class="stretch" alt="" />
	</div>
	<!-- Main Content -->
	<div class="container-fluid">
		<div class="row main-content bg-success text-center">
			<div class="col-md-4 text-center companyinfo">
				<span class="company__logo">
					<h2> 
					<a href="<?= base_url() ?>" > <img src="<?= base_url() ?>asset/zeedapp/assets/img/icon/logo.png" style="width:180px;"> </h2>
				</a>
				</span>
				<!-- <h4 class="company_title">AYOEXPORT.COM</h4> -->
			</div>
			<div class="col-md-8 col-xs-12 col-sm-12 login_form ">
				<div class="container-fluid">
					<div class="row">
						<h2>Log In</h2>
					</div>
					<p><?php echo $this->session->flashdata('msg'); ?></p>
					<div class="row">
						<form class="form-group" method='POST' id="loginform" action="<?= base_url() ?>index.php/page/login">
							<div class="row">
								<input type="text" name="username" id="username" class="form__input" placeholder="Username">
							</div>
							<div class="row">
								<!-- <span class="fa fa-lock"></span> -->
								<input type="password" name="password" id="password" class="form__input" placeholder="Password">
							</div>
							<div class="row">
								<input type="checkbox" name="remember_me" id="remember_me" class="">
								<label for="remember_me">Remember Me!</label>
							</div>
							<div class="row">
								<input type="submit" value="Submit" class="btn">
							</div>
						</form>
					</div>
					<div class="row">
					<!-- <p>Forget Password? <a href="<= base_url(); ?>forget_password">Forget Password</a></p> -->
						<p>Don't have an account? <a href="<?= base_url(); ?>register">Register Here</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Footer -->
	<div class="container-fluid text-center footer">
		Copyright || ayoexport.com</a></p>
	</div>
</body>

<!-- jQuery -->

<script src="<?= base_url() ?>asset/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?= base_url() ?>asset/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?= base_url() ?>asset/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?= base_url() ?>asset/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?= base_url() ?>asset/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?= base_url() ?>asset/js/custom.min.js"></script>
<!--Style Switcher -->
<script src="<?= base_url() ?>asset/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<script src="https://kit.fontawesome.com/a076d05399.js"></script>


<input id="ext-version" type="hidden" value="1.3.5"></body>

</html>