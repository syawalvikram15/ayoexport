

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <title>AyoExport Asisten</title>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link href="<?= base_url() ?>asset/css/chatbot.css" rel="stylesheet">
      <link rel="stylesheet" href="<?= base_url() ?>asset/bootstrap/dist/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
      <?php header('Access-Control-Allow-Origin: *'); ?>
      <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>asset/zeedapp/assets/img/icon/icon.png">
   </head>
   <body>
      <!-- partial:index.partial.html -->
      <section class="msger " style="margin-left: auto;margin-right: auto;">
         <header class="msger-header">
            <div class="msger-header-title">
               <!-- <i class="fas fa-angle-double-right"></i> AyoExport Chatbot <i class="fas fa-angle-double-left"></i> -->
            </div>
         </header>
         <main class="msger-chat">
            <div class="msg left-msg">
                <!-- <div class="msg-img" style="background-image: url(https://image.freepik.com/free-vector/chatbot-icon-concept-support-robot-technology-digital-chat-bot-application_48369-14496.jpg)"></div> -->
                <div class="msg-bubble" style="margin-left: 60px;">
                      <div class="msg-info">
                          <div class="msg-info-name">AyoExport Asisten</div>
                          <div class="msg-info-time">12:45</div>
                      </div>
                      <div class="msg-text">
                          Hallo, saya Ayoexport asisten virtual Anda. 
                      </div>
                </div>
            </div>
            <div class="msg left-msg">
                <div class="msg-img" style="background-image: url(https://image.freepik.com/free-vector/chatbot-icon-concept-support-robot-technology-digital-chat-bot-application_48369-14496.jpg)"></div>
                <div class="msg-bubble">
                      <div class="msg-info">
                          <div class="msg-info-name">AyoExport Asisten</div>
                          <div class="msg-info-time">12:45</div>
                      </div>
                      <div class="msg-text">
                         Layanan kami masih dalam tahap penyempurnaan namun Anda sudah dapat melakukan pendaftaran. Apakah Anda membutuhkan informasi lebih lanjut ? 
                      </div>
                      <br>
                      <button type="button" class="btn btn-primary" onclick="botResponse_but('about')">Tentang Ayoexport </button>
                      <button type="button" class="btn btn-primary" onclick="botResponse_but('how')">Cara Bekerjasama </button>
                      <button type="button" class="btn btn-primary" onclick="botResponse_but('service')">Layanan Ayoexport </button>
                      <button type="button" class="btn btn-primary" onclick="botResponse_but('get')">Cara mendapatkan buyer </button>
                      <button type="button" class="btn btn-primary" onclick="botResponse_but('contact')">Bicara Langsung dengan team kami </button>
                </div>
            </div>
         </main>
         <form class="msger-inputarea">
            <input type="text" class="msger-input" id="textInput" placeholder="Enter your message...">
            <button type="submit" class="msger-send-btn">Send</button>
         </form>
      </section>
      <!-- partial -->
      <script src='https://use.fontawesome.com/releases/v5.0.13/js/all.js'></script>
      <script>
         const msgerForm = get(".msger-inputarea");
         const msgerInput = get(".msger-input");
         const msgerChat = get(".msger-chat");
         
         
         // Icons made by Freepik from www.flaticon.com
         const BOT_IMG = "https://image.freepik.com/free-vector/chatbot-icon-concept-support-robot-technology-digital-chat-bot-application_48369-14496.jpg";
         const PERSON_IMG = "https://image.flaticon.com/icons/svg/145/145867.svg";
         const BOT_NAME = "AyoExport Asisten";
         const PERSON_NAME = "You";
         
         msgerForm.addEventListener("submit", event => {
           event.preventDefault();
         
           const msgText = msgerInput.value;
           if (!msgText) return;
         
          //  appendMessage(PERSON_NAME, PERSON_IMG, "right", msgText);
           msgerInput.value = "";
           botResponse(msgText);
         });
         
         function appendMessage(name, img, side, text) {
           //   Simple solution for small apps
           const msgHTML = `
                     <div class="msg ${side}-msg">
                       <div class="msg-img" style="background-image: url(${img})"></div>
         
                       <div class="msg-bubble">
                         <div class="msg-info">
                           <div class="msg-info-name">${name}</div>
                           <div class="msg-info-time">${formatDate(new Date())}</div>
                         </div>
         
                         <div class="msg-text">${text}</div>
                       </div>
                     </div>
                     `;
           msgerChat.insertAdjacentHTML("beforeend", msgHTML);
           msgerChat.scrollTop += 500;
         }
         
         function botResponse(rawText) {
         
           appendMessage(PERSON_NAME, PERSON_IMG, "right", rawText);
         
           link_button = "location.href='https://api.whatsapp.com/send?phone=6287794418050'"
           appendMessage(BOT_NAME, BOT_IMG, "left", 'Untuk pertanyaan lebih lanjut silahkan hubungi kami. <br><button type="button" class="btn btn-primary" onclick="'+ link_button +'">Hubungi kami</button>');
           
         }
         
         
         function botResponse_but(rawText) {
           // // Bot Response
           // $.get("http://34.126.80.48:7000/get", { msg: rawText }).done(function (data) {
           //   console.log(rawText);
           //   console.log(data);
           //   const msgText = data;
           //   appendMessage(BOT_NAME, BOT_IMG, "left", msgText);
         
           // });
           if (rawText == 'about'){
               textChat = 'Tentang Ayoexport'
               link_button = "location.href='https://www.ayoexport.com/ayoexport/#feature'"
               msgText  = 'Ayoexport.com merupakan layanan virtual assistant berbasis AI yang membantu pelaku usaha untuk melakukan ekspor dengan lebih mudah, efisien dan terintegrasi melalui aplikasi website. Klik tombol berikut untuk mengetahui lebih lanjut tentang kami. <br><button type="button" class="btn btn-primary" onclick="'+ link_button +'">Tentang kami</button>'
           } else if (rawText == 'how'){
               textChat = 'Cara Bekerjasama'
               link_button = "location.href='https://product.ayoexport.com/for-supplier/'"
               msgText  = 'Silahkan melakukan pendaftaran dengan klik tombol berikut.<br> <button type="button" class="btn btn-primary" onclick="'+ link_button +'">Daftar sekarang</button>'
           } else if (rawText == 'service'){
               textChat = 'Layanan Ayoexport'
               link_button = "location.href='https://www.ayoexport.com/ayoexport/#feature'"
               msgText  = 'Untuk mengetahui tentang layanan ayoexport.com silahkan klik tombol berikut. <br><button type="button" class="btn btn-primary" onclick="'+ link_button +'"> Layanan kami</button>'
           } else if (rawText == 'get'){
               textChat = 'Cara mendapatkan buyer'
               link_button = "location.href='https://product.ayoexport.com/for-supplier/'"
               msgText  = 'Untuk mendapatkan buyer Anda dapat mendaftar terlebih dahulu. Kami akan membantu Anda untuk mendapatkan buyer. Untuk pendaftaran silahkan klik tombol berikut.<br> <button type="button" class="btn btn-primary" onclick="'+ link_button +'"> Daftar sekarang </button>'
           } else if (rawText == 'contact'){
               textChat = 'Bicara Langsung dengan team kami'
               link_button = "location.href='https://api.whatsapp.com/send?phone=6287794418050'"
               msgText  = 'Klik tombol dibawah ini untuk bicara langsung dengan team kami. <br> <button type="button" class="btn btn-primary" onclick="'+ link_button +'"> Hubungi kami </button>'
           }
           // if (textChat == ''){
            appendMessage(PERSON_NAME, PERSON_IMG, "right", textChat);
           // }
           // msgerInput.value = "";
           if (rawText != ''){
           appendMessage(BOT_NAME, BOT_IMG, "left", msgText);
           }
         
           rawText = ''
           textChat = ''
           msgText = ''
         }
         
         // Utils
         function get(selector, root = document) {
           return root.querySelector(selector);
         }
         
         function formatDate(date) {
           const h = "0" + date.getHours();
           const m = "0" + date.getMinutes();
         
           return `${h.slice(-2)}:${m.slice(-2)}`;
         }
         
      </script>
      <style>
         .btn-primary {
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
            margin-top: 20px;
         }
         .btn {
            display: inline-block;
            padding: 26px 22px;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
         }
         body {
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            line-height: 1.42857143;
            color: #333;
            overflow: visible;
            background-image: url("https://assets.website-files.com/5e1c4fb5db4d5243c0021d34/5e1c4fb5db4d526c44021d44_bg-shapes.svg");
            background-size: cover;
            background-repeat: repeat-x;
         }

         :root {
  --body-bg: linear-gradient(135deg, #f5f7fa 0%, #c3cfe2 100%);
  --msger-bg: #fff;
  --border: 2px solid #ddd;
  --left-msg-bg: #ececec;
  --right-msg-bg: #579ffb;
} 