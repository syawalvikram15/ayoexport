

<div class="container" style="width:100% !important">
   <div class="main-body">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="main-breadcrumb">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">User Profile</li>
         </ol>
      </nav>
      <div class="floating-wpp"></div>
      <!-- /Breadcrumb -->
      <div class="row gutters-sm">
         <div class="col-md-4 mb-3">
            <div class="card-profile">
               <div class="card-profile-body">
                  <div class="d-flex flex-column align-items-center text-center">
                     <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" class="rounded-circle" width="150">
                     <div class="mt-3">
                        <h4> <span id="nama2"> </span> </h4>
                        <p class="text-secondary mb-1"> <span id="level"> </span> </p>
                        <p class="text-muted font-size-sm"> <span id="alamat1"> </span> </p>
                        <button class="btn btn-primary" style="font-size: 1.3rem;" data-title="Edit" data-toggle="modal" data-target="#edit" id="button-edit-profile2">Edit Profile</button>
                        <!-- <button class="btn btn-outline-primary">Message</button> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-8">
            <div class="card-profile mb-3">
               <div class="card-profile-body">
                  <div class="alert" id="alert_profile" style="display:none;">
                     <div class="container-notif">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">
                        <i class="fas fa-times"></i>
                        </button>
                        <span id="notif_biodata"> - </span> </br>
                        <button class="btn btn-primary" style="font-size: 1.3rem;display:none;" data-title="Edit" data-toggle="modal" data-target="#edit" id="button-edit-profile">Lengkapi Profile</button>
                     </div>
                  </div>
                  <h3 class="mb-0">Biodata User</h3>
                  <br>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Full Name</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="nama"> - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Email</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="email"> - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">HP</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="hp"> - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Website</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="website"> - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Alamat</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="alamat2"> -  </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Tahun Berdiri</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="tahun_berdiri"> - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Jenis Usaha</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="jenis_usaha"> - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Jenis Produk</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="jenis_produk"> - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Daftar Produk</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="daftar_produk"> -</span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Kapasitas Produk</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="kapasitas_produk"> -</span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Memiliki Gudang?</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="gudang"> - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Memiliki Pabrik?</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="pabrik"> - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Lama Produk</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="lama_produk">  - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Kendala Eksport</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="kendala"> - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Negara Tujuan</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="negara_tujuan"> - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Jumlah Karyawan</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="jum_karyawan"> - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Sertifikasi</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="sertifikasi"> - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">OEM</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="oem"> - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Term pembayaran</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="term_pembayaran"> - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Term Harga</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="term_harga"> - </span>
                     </div>
                  </div>
                  <hr>
                  <div class="row">
                     <div class="col-sm-3">
                        <h6 class="mb-0">Logistik</h6>
                     </div>
                     <div class="col-sm-9 text-secondary">
                        <span id="logistik"> - </span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="edit">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title custom_align" id="Heading">Edit Your Profile</h4>
            <button type="button" id="close-modal-edit" class="close" data-dismiss="modal" aria-hidden="true">
            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
            </button>
         </div>
         <div class="modal-body">
         <form role="form" id="quickForm"> 
            <div class="form-group">
               <label for="exampleInputEmail1">Nama Perusahaan</label>
               <input class="form-control " type="text" id="input_nama" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Email Perusahaan</label>
               <input class="form-control " type="text"  id="input_email" readonly="readonly">
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">HP</label>
               <input class="form-control " type="text" id="input_hp"required >
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Website Perusahaan</label>
               <input class="form-control " type="text" id="input_website" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Alamat Lengkap Perusahaan</label>
               <input class="form-control " type="text" placeholder="Jl.Soekarno Hata" id="input_alamat" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Kota</label>
               <input class="form-control " type="text" placeholder="Jakarta Selatan" id="input_city" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Provinsi</label>
               <input class="form-control " type="text" placeholder="DKI Jakarta" id="input_province" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Jenis Usaha</label>
               <select style="height: auto;" class="form-control" id="input_jenis_usaha" required>
                  <option value="produsen">Produsen</option>
                  <option value="trading" selected="selected">Trading</option>
                  <option value="other">Lainnya</option>
               </select>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Jenis Produk</label>
               <input class="form-control " type="text" id="input_jenis_produk" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Daftar Produk</label>
               <input class="form-control " type="text" id="input_daftar_produk" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Kapasitas atau jumlah produksi perbulan</label>
               <input class="form-control " type="text" id="input_kapasitas_produk" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Satuan kapasitas. Misal : ton/kg/pcs/dll</label>
               <input class="form-control " type="text" id="input_satuan_produk" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Apakah memiliki tempat produksi atau pabrik ?</label>
               <select style="height: auto;" class="form-control" id="input_pabrik" required>
                  <option value="1">Ya</option>
                  <option value="0" selected="selected">Tidak</option>
               </select>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Apakah memiliki tempat penyimpanan atau gudang ?</label>
               <select style="height: auto;" class="form-control" id="input_gudang" required>
                  <option value="1">Ya</option>
                  <option value="0" selected="selected">Tidak</option>
               </select>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Rata-rata berapa lama proses produksi berlangsung (Minggu)?</label>
               <input class="form-control " type="text" id="input_lama_produk" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Kendala-kendala yang sering dihadapi?</label>
               <input class="form-control " type="text" id="input_kendala" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Tahun Berdiri</label>
               <input class="form-control " type="text" id="input_tahun_berdiri" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail6">Apakah sudah memulai ekspor?</label>
               <select style="height: auto;" class="form-control" id="input_ekspor" required>
               <option value="1">Sudah</option>
               <option value="0" selected="selected">Belum</option>
               </select>
            </div>
            <br>
            <div class="form-group">
               <label for="exampleInputEmail1">Negara tujuan ekspor sebelumnya (kosongkan jika belum)?</label>
               <input class="form-control " type="text" id="input_negara_tujuan">
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Jumlah Karyawan</label>
               <input class="form-control " type="text" id="input_jumlah_karyawan" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Sertifikasi yang dimiliki ?</label>
               <input class="form-control " type="text" id="input_sertifikasi" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Terms pengiriman yang diterima ?</label>
               <input class="form-control " type="text" id="input_term_harga" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Terms pembayaran yang diterima?</label>
               <input class="form-control " type="text" id="input_term_pembayaran" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Apakah memiliki layanan OEM ?</label>
               <select style="height: auto;" class="form-control" id="input_oem" required>
               <option value="1">Ya</option>
               <option value="0" selected="selected">Tidak</option>
               </select>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Apakah sudah memiliki layanan logistik sendiri ?</label>
               <select style="height: auto;" class="form-control" id="input_logistik" required>
               <option value="1">Ya</option>
               <option value="0" selected="selected">Tidak</option>
               </select>
            </div>
         </div>
         <div class="modal-footer ">
            <button type="button"  id="button-update" class="btn btn-warning btn-lg" style="width: 100%;" onclick="update_profile()">
            <span class="glyphicon glyphicon-ok-sign"></span> Update
            </button>
         </div>
         </form>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>


<!-- Here is Icon Loader !!!!!! -->
<div id="loader" class="loader-rm">
    <img src="https://www.drupal.org/files/issues/throbber_12.gif" class="img-gif-rm" alt="Loading..." />
</div>


<style>
   }
   .card-profile {
   box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
   }
   .card-profile {
   position: relative;
   display: flex;
   flex-direction: column;
   min-width: 0;
   word-wrap: break-word;
   background-color: #DCDCDC;
   background-clip: border-box;
   border: 0 solid rgba(0,0,0,.125);
   border-radius: .25rem;
   }
   .card-profile-body {
   flex: 1 1 auto;
   min-height: 1px;
   padding: 1rem;
   background-color: #DCDCDC;
   }
   .gutters-sm {
   margin-right: -8px;
   margin-left: -8px;
   }
   .gutters-sm>.col, .gutters-sm>[class*=col-] {
   padding-right: 8px;
   padding-left: 8px;
   }
   .mb-3, .my-3 {
   margin-bottom: 1rem!important;
   }
   .bg-gray-300 {
   background-color: #e2e8f0;
   }
   .h-100 {
   height: 100%!important;
   }
   .shadow-none {
   box-shadow: none!important;
   }
   .img-gif-rm {
	position: fixed;
	top: 50%;
	left: 50%;
	width: 75px;
	height: 75px;
}
.show {
    display: block;
}
</style>

