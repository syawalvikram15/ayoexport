<?php
class Logistik_model extends CI_Model
{

    public function __construct()
    {
        // $this->load->database();
        parent::__construct();
        $this->db = $this
            ->load
            ->database('default', true);

    }
    public function add_transaction_logistik($data)
    {
        // $query = $this->db->insert('logistik',$data);
        $hasil = $this
            ->db
            ->query("INSERT into logistik (email,	nama_produk,	jumlah_produk,	volume_produk,	satuan_ukuran,	asal_produk,	tujuan_pengiriman,	estimasi_budget,	tanggal_pengiriman,status) 
            values (?,?,?,?,?,?,?,?,?,?);", array(
            $data['email'],
            $data['nama_produk'],
            $data['jumlah_produk'],
            $data['volume_produk'],
            $data['satuan_ukuran'],
            $data['asal_produk'],
            $data['tujuan_pengiriman'],
            $data['estimasi_budget'],
            $data['tanggal_pengiriman'],
            $data['status']
        ));
        if (!$hasil)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $hasil;
        }
    }
    public function get_transaction_logistik()
    {

        $sql = "SELECT t1.id_logistik, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk, t1.volume_produk,t1.satuan_ukuran,t1.asal_produk,t1.tujuan_pengiriman,t1.estimasi_budget, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM logistik t1 , user t2 WHERE t1.email = t2.email AND (t1.status = 'on-proggress' OR t1.status = 'offer');";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_transaction_logistik_done()
    {

        $sql = "SELECT t1.id_logistik, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk, t1.volume_produk,t1.satuan_ukuran,t1.asal_produk,t1.tujuan_pengiriman,t1.estimasi_budget, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM logistik t1 , user t2 WHERE t1.email = t2.email AND t1.status = 'done';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_transaction_logistik_reject()
    {

        $sql = "SELECT t1.id_logistik, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk, t1.volume_produk,t1.satuan_ukuran,t1.asal_produk,t1.tujuan_pengiriman,t1.estimasi_budget, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM logistik t1 , user t2 WHERE t1.email = t2.email AND t1.status = 'reject';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_detail_logistik($id)
    {

        $sql = "SELECT t1.id_logistik, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk, t1.volume_produk,t1.satuan_ukuran,t1.asal_produk,t1.tujuan_pengiriman,t1.estimasi_budget, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM logistik t1 , user t2 WHERE t1.email = t2.email AND t1.id_logistik = $id ;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }

    public function get_action_logistik($id, $status)
    {

        $sql = "UPDATE logistik set status = '$status' where id_logistik = $id;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return true;
        }

    }
    // public function get_action_logistik($id, $status)
    // {

    //     $sql = "UPDATE logistik_respon set status = '$status', date_accepted = now() where id_respon = $id;";
    //     $query = $this
    //         ->db
    //         ->query($sql);
    //     if (!$query)
    //     {
    //         return $error = $this
    //             ->db
    //             ->error();
    //     }
    //     else
    //     {
    //         return true;
    //     }

    // }
    public function send_respond_logistik($user_id, $respon, $status, $date, $id_logistik)
    {
        $sql = "INSERT INTO logistik_respon (id_user, respon,status,date, id_logistik) values ( '$user_id', '$respon' , '$status' , '$date','$id_logistik' );";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return true;
        }

    }

    // USER LOGISTIK
    public function get_own_transaction_logistik($email)
    {

        $sql = "SELECT id_logistik, email, nama_produk, jumlah_produk, volume_produk,satuan_ukuran,asal_produk,tujuan_pengiriman,estimasi_budget, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM logistik  WHERE email = '$email'  AND (status = 'on-proggress' OR status = 'offer');";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_own_transaction_logistik_done($email)
    {

        $sql = "SELECT id_logistik, email, nama_produk, jumlah_produk, volume_produk,satuan_ukuran,asal_produk,tujuan_pengiriman,estimasi_budget, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM logistik  WHERE email = '$email'  AND status = 'done';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_own_transaction_logistik_reject($email)
    {

        $sql = "SELECT id_logistik, email, nama_produk, jumlah_produk, volume_produk,satuan_ukuran,asal_produk,tujuan_pengiriman,estimasi_budget, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM logistik  WHERE email = '$email'  AND status = 'reject';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function update_own_transaction_logistik($id, $nama_produk, $jumlah_produk, $volume_produk, $satuan_ukuran, $asal_produk, $tujuan_pengiriman, $estimasi_budget)
    {
        $hasil = $this
            ->db
            ->query("UPDATE logistik  set	nama_produk = '$nama_produk',	jumlah_produk = '$jumlah_produk',	volume_produk = '$volume_produk',	satuan_ukuran = '$satuan_ukuran',	asal_produk = '$asal_produk',	tujuan_pengiriman = '$tujuan_pengiriman',	estimasi_budget = '$estimasi_budget' 
            WHERE id_logistik = '$id';");

        if (!$hasil)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $hasil;
        }
    }

    public function get_respon_logistik($email)
    {
        $sql = "SELECT * from logistik_respon  WHERE id_user = '$email' ORDER BY 1 DESC;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    public function get_respon_logistik_admin()
    {
        $sql = "SELECT * from logistik_respon ORDER BY 1 DESC;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    

    public function delete_own_transaction_logistik($id_logistik)
    {
        $this
            ->db
            ->where('id_logistik', $id_logistik);
        $query = $this
            ->db
            ->delete('logistik');
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query;
        }
    }

    public function get_own_detail_logistik($id)
    {

        $sql = "SELECT id_logistik, email, nama_produk,  jumlah_produk, volume_produk,satuan_ukuran,asal_produk,tujuan_pengiriman,estimasi_budget, date(tanggal_pengiriman) as tanggal_pengiriman, nama_gambar, status  FROM logistik WHERE id_logistik = $id ;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    public function confirm_offer_logistik($id, $respon)
    {

        $sql = "UPDATE logistik_respon SET accepted = '$respon' , date_accepted = now() where id_respon = $id;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query;
        }
    }

}

