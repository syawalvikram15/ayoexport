<?php
class Regulasi_model extends CI_Model
{

    public function __construct()
    {
        // $this->load->database();
        parent::__construct();
        $this->db = $this
            ->load
            ->database('default', true);

    }
    public function add_transaction_regulasi($data)
    {
        // $query = $this->db->insert('regulasi',$data);
        $hasil = $this
            ->db
            ->query("INSERT into regulasi (email,	nama_produk,	jumlah_produk,	asal_produk,	tujuan_pengiriman,	tanggal_pengiriman,status) 
            values (?,?,?,?,?,?,?);", array(
            $data['email'],
            $data['nama_produk'],
            $data['jumlah_produk'],
            $data['asal_produk'],
            $data['tujuan_pengiriman'],
            $data['tanggal_pengiriman'],
            $data['status']
        ));
        if (!$hasil)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $hasil;
        }
    }
    public function get_transaction_regulasi()
    {

        $sql = "SELECT t1.id_regulasi, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk,t1.asal_produk,t1.tujuan_pengiriman, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM regulasi t1 , user t2 WHERE t1.email = t2.email AND (t1.status = 'on-proggress' OR t1.status = 'offer');";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_transaction_regulasi_done()
    {

        $sql = "SELECT t1.id_regulasi, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk,t1.asal_produk,t1.tujuan_pengiriman, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM regulasi t1 , user t2 WHERE t1.email = t2.email AND t1.status = 'done';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_transaction_regulasi_reject()
    {

        $sql = "SELECT t1.id_regulasi, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk,t1.asal_produk,t1.tujuan_pengiriman, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM regulasi t1 , user t2 WHERE t1.email = t2.email AND t1.status = 'reject';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_detail_regulasi($id)
    {

        $sql = "SELECT t1.id_regulasi, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk,t1.asal_produk,t1.tujuan_pengiriman, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM regulasi t1 , user t2 WHERE t1.email = t2.email AND t1.id_regulasi = $id ;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }

    public function get_action_regulasi($id, $status)
    {

        $sql = "UPDATE regulasi set status = '$status' where id_regulasi = $id;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return true;
        }

    }
    // public function get_action_regulasi($id, $status)
    // {

    //     $sql = "UPDATE regulasi_respon set status = '$status', date_accepted = now() where id_respon = $id;";
    //     $query = $this
    //         ->db
    //         ->query($sql);
    //     if (!$query)
    //     {
    //         return $error = $this
    //             ->db
    //             ->error();
    //     }
    //     else
    //     {
    //         return true;
    //     }

    // }
    public function send_respond_regulasi($user_id, $respon, $status, $date, $id_regulasi)
    {
        $sql = "INSERT INTO regulasi_respon (id_user, respon,status,date, id_regulasi) values ( '$user_id', '$respon' , '$status' , '$date','$id_regulasi' );";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return true;
        }

    }

    // USER REGULASI
    public function get_own_transaction_regulasi($email)
    {

        $sql = "SELECT id_regulasi, email, nama_produk, jumlah_produk,asal_produk,tujuan_pengiriman, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM regulasi  WHERE email = '$email'  AND (status = 'on-proggress' OR status = 'offer');";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_own_transaction_regulasi_done($email)
    {

        $sql = "SELECT id_regulasi, email, nama_produk, jumlah_produk,asal_produk,tujuan_pengiriman, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM regulasi  WHERE email = '$email'  AND status = 'done';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_own_transaction_regulasi_reject($email)
    {

        $sql = "SELECT id_regulasi, email, nama_produk, jumlah_produk,asal_produk,tujuan_pengiriman, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM regulasi  WHERE email = '$email'  AND status = 'reject';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function update_own_transaction_regulasi($id, $nama_produk, $jumlah_produk, $asal_produk, $tujuan_pengiriman)
    {
        $hasil = $this
            ->db
            ->query("UPDATE regulasi  set	nama_produk = '$nama_produk',	jumlah_produk = '$jumlah_produk',	asal_produk = '$asal_produk',	tujuan_pengiriman = '$tujuan_pengiriman' 
            WHERE id_regulasi = '$id';");

        if (!$hasil)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $hasil;
        }
    }

    public function get_respon_regulasi($email)
    {
        $sql = "SELECT * from regulasi_respon  WHERE id_user = '$email' ORDER BY 1 DESC;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    public function get_respon_regulasi_admin()
    {
        $sql = "SELECT * from regulasi_respon ORDER BY 1 DESC;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    

    public function delete_own_transaction_regulasi($id_regulasi)
    {
        $this
            ->db
            ->where('id_regulasi', $id_regulasi);
        $query = $this
            ->db
            ->delete('regulasi');
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query;
        }
    }

    public function get_own_detail_regulasi($id)
    {

        $sql = "SELECT id_regulasi, email, nama_produk,  jumlah_produk,asal_produk,tujuan_pengiriman, date(tanggal_pengiriman) as tanggal_pengiriman, nama_gambar, status  FROM regulasi WHERE id_regulasi = $id ;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    public function confirm_offer_regulasi($id, $respon)
    {

        $sql = "UPDATE regulasi_respon SET accepted = '$respon' , date_accepted = now() where id_respon = $id;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query;
        }
    }

}

