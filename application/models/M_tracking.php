<?php
class M_tracking extends CI_Model
{

    function detail($where, $table)
    {
        return $this->db->get_where($table, $where);
    }
    function get_data($table)
    {
        return $this->db->get($table);
    }
    function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
    }
    function get_tracking()
    {
        $query = $this->db->get('tracking');
        return $query->result_array();
    }
    function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }
    function delete_data($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
