<?php
class Pembiayaan_model extends CI_Model
{

    public function __construct()
    {
        // $this->load->database();
        parent::__construct();
        $this->db = $this
            ->load
            ->database('default', true);

    }
    public function add_transaction_pembiayaan($data)
    {
        // $query = $this->db->insert('pembiayaan',$data);
        $hasil = $this
            ->db
            ->query("INSERT into pembiayaan (email,	nama_produk,	jumlah_produk,	asal_produk,	tujuan_pengiriman,	estimasi_modal,	tanggal_pengiriman,status) 
            values (?,?,?,?,?,?,?,?);", array(
            $data['email'],
            $data['nama_produk'],
            $data['jumlah_produk'],
            $data['asal_produk'],
            $data['tujuan_pengiriman'],
            $data['estimasi_modal'],
            $data['tanggal_pengiriman'],
            $data['status']
        ));
        if (!$hasil)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $hasil;
        }
    }
    public function get_transaction_pembiayaan()
    {

        $sql = "SELECT t1.id_pembiayaan, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk,t1.asal_produk,t1.tujuan_pengiriman,t1.estimasi_modal, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM pembiayaan t1 , user t2 WHERE t1.email = t2.email AND (t1.status = 'on-proggress' OR t1.status = 'offer');";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_transaction_pembiayaan_done()
    {

        $sql = "SELECT t1.id_pembiayaan, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk,t1.asal_produk,t1.tujuan_pengiriman,t1.estimasi_modal, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM pembiayaan t1 , user t2 WHERE t1.email = t2.email AND t1.status = 'done';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_transaction_pembiayaan_reject()
    {

        $sql = "SELECT t1.id_pembiayaan, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk,t1.asal_produk,t1.tujuan_pengiriman,t1.estimasi_modal, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM pembiayaan t1 , user t2 WHERE t1.email = t2.email AND t1.status = 'reject';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_detail_pembiayaan($id)
    {

        $sql = "SELECT t1.id_pembiayaan, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk,t1.asal_produk,t1.tujuan_pengiriman,t1.estimasi_modal, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM pembiayaan t1 , user t2 WHERE t1.email = t2.email AND t1.id_pembiayaan = $id ;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }

    public function get_action_pembiayaan($id, $status)
    {

        $sql = "UPDATE pembiayaan set status = '$status' where id_pembiayaan = $id;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return true;
        }

    }
    // public function get_action_pembiayaan($id, $status)
    // {

    //     $sql = "UPDATE pembiayaan_respon set status = '$status', date_accepted = now() where id_respon = $id;";
    //     $query = $this
    //         ->db
    //         ->query($sql);
    //     if (!$query)
    //     {
    //         return $error = $this
    //             ->db
    //             ->error();
    //     }
    //     else
    //     {
    //         return true;
    //     }

    // }
    public function send_respond_pembiayaan($user_id, $respon, $status, $date, $id_pembiayaan)
    {
        $sql = "INSERT INTO pembiayaan_respon (id_user, respon,status,date, id_pembiayaan) values ( '$user_id', '$respon' , '$status' , '$date','$id_pembiayaan' );";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return true;
        }

    }

    // USER PEMBIAYAAN
    public function get_own_transaction_pembiayaan($email)
    {

        $sql = "SELECT id_pembiayaan, email, nama_produk, jumlah_produk,asal_produk,tujuan_pengiriman,estimasi_modal, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM pembiayaan  WHERE email = '$email'  AND (status = 'on-proggress' OR status = 'offer');";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_own_transaction_pembiayaan_done($email)
    {

        $sql = "SELECT id_pembiayaan, email, nama_produk, jumlah_produk,asal_produk,tujuan_pengiriman,estimasi_modal, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM pembiayaan  WHERE email = '$email'  AND status = 'done';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_own_transaction_pembiayaan_reject($email)
    {

        $sql = "SELECT id_pembiayaan, email, nama_produk, jumlah_produk,asal_produk,tujuan_pengiriman,estimasi_modal, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM pembiayaan  WHERE email = '$email'  AND status = 'reject';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function update_own_transaction_pembiayaan($id, $nama_produk, $jumlah_produk, $asal_produk, $tujuan_pengiriman, $estimasi_modal)
    {
        $hasil = $this
            ->db
            ->query("UPDATE pembiayaan  set	nama_produk = '$nama_produk',	jumlah_produk = '$jumlah_produk',	asal_produk = '$asal_produk',	tujuan_pengiriman = '$tujuan_pengiriman',	estimasi_modal = '$estimasi_modal' 
            WHERE id_pembiayaan = '$id';");

        if (!$hasil)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $hasil;
        }
    }

    public function get_respon_pembiayaan($email)
    {
        $sql = "SELECT * from pembiayaan_respon  WHERE id_user = '$email' ORDER BY 1 DESC;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    public function get_respon_pembiayaan_admin()
    {
        $sql = "SELECT * from pembiayaan_respon ORDER BY 1 DESC;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    

    public function delete_own_transaction_pembiayaan($id_pembiayaan)
    {
        $this
            ->db
            ->where('id_pembiayaan', $id_pembiayaan);
        $query = $this
            ->db
            ->delete('pembiayaan');
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query;
        }
    }

    public function get_own_detail_pembiayaan($id)
    {

        $sql = "SELECT id_pembiayaan, email, nama_produk,  jumlah_produk,asal_produk,tujuan_pengiriman,estimasi_modal, date(tanggal_pengiriman) as tanggal_pengiriman, nama_gambar, status  FROM pembiayaan WHERE id_pembiayaan = $id ;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    public function confirm_offer_pembiayaan($id, $respon)
    {

        $sql = "UPDATE pembiayaan_respon SET accepted = '$respon' , date_accepted = now() where id_respon = $id;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query;
        }
    }

}

