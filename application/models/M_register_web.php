<?php
class M_register_web extends CI_Model
{
    public $id_tracker;
    public $id_website;

    public function __construct()
    {
        $this->load->database();
    }

    public function cekidtracker()
    {
        $query = $this->db->query("SELECT MAX(id_tracker) as idtracker from user_website");
        $hasil = $query->row();
        return $hasil->idtracker;
    }

    public function cekidwebsite()
    {
        $query = $this->db->query("SELECT MAX(id_website) as idwebsite from user_website");
        $hasil = $query->row();
        return $hasil->idwebsite;
    }

    function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
    }

    function get_register_web()
    {
        $query = $this->db->get('user_website');
        return $query->result_array();
    }

    function get_code()
    {
        $this->db->SELECT('RIGHT(user_website.id_website,4) as kode', false);
        $this->db->order_by('id_website', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('user_website'); //cek user
        if ($query->num_rows() <> 0) { //jika sudah ada 
            $data = $query->row();
            $kode = intval($data->kode) + 1;
        } else {
            //jika belum ada
            $kode = 1;
        }
        $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT); //angka 4 merupakan 4 angka 0
        $kodejadi = "WEB-3442-" . $kodemax;
        return $kodejadi;
    }
}
