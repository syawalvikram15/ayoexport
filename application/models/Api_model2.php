<?php
class Api_model2 extends CI_Model {
    private $nodes = array();
    private $start,$end,$user_id,$no_hp;
    public function getNodes(){
        $this->end = (int) isset($_SESSION['tgl_end'])?date('Ymd',strtotime($_SESSION['tgl_end'])):date('Ymd',strtotime('today'));
        $this->start = (int) isset($_SESSION['tgl_start'])?date('Ymd',strtotime($_SESSION['tgl_start'])):date('Ymd',strtotime('-7 days'));
        $this->no_hp = isset($_SESSION['no_hp'])?$_SESSION['no_hp']:'+628122358224';
		$this->user_id =  $this->getUserIdfromHp()->id;//isset($_SESSION['user_id'])?$_SESSION['user_id']:'0000ece0-7f85-11ea-ad45-c1bbb1aab245';
        $this->nodes =  $this->getNodesId(
           $this->user_id,
            $this->start,
			$this->end
        );
        //$query = $this->db->query("SELECT 	 user_id,nama,no_hp,status,mac_address,tgl_terdaftar,mac_address_contacted,user_id_contacted,jml_kontak,first_contacted,last_contacted,lama_kontak,nama_contacted,no_hp_contacted,status_contacted,tgl_terdaftar_contacted FROM mart_user_contact_kelurahan WHERE user_id IN ?  AND ds between ? and ?;",
        $query = $this->db->query("SELECT user_id,ifnull(nama,'')nama,no_hp,status,ifnull(mac_address,'')mac_address,tgl_terdaftar,
ifnull(mac_address_contacted,'')mac_address_contacted,user_id_contacted,ifnull(nama_contacted,'')nama_contacted,
no_hp_contacted,status_contacted,tgl_terdaftar_contacted,
SUM(jml_kontak) jml_kontak,MIN(first_contacted)first_contacted,MAX(last_contacted)last_contacted,SUM(lama_kontak)lama_kontak
 FROM mart_user_contact_kelurahan 
WHERE user_id IN ? AND ds BETWEEN ? AND ?
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12;",
        array($this->nodes,(int)$this->start,(int)$this->end));
        return $query->result_array();
    }
    public function getLinks(){
        $query = $this->db->query("SELECT user_id source , if(user_id_contacted!='',user_id_contacted,mac_address_contacted) target,1 value FROM mart_user_contact_kelurahan WHERE ds between ? and ? AND user_id in ?;",array($this->start,$this->end,$this->nodes));
        return $query->result_array();
    }
    
	private function getUserIdfromHp(){
		//$q = "SELECT DISTINCT user_id id FROM mart_user_contact_kelurahan WHERE ds BETWEEN ? AND ? and no_hp=?;";
		$q = "SELECT user_id id FROM `dma_pl_users` WHERE mobile_number=?;";
		$query = $this->db->query($q,array($this->no_hp));
		return $query->row();
	}

    private function getNodesId($id,$start,$end){
        $data_all= array();
        //$data = $this->getRealted([$id],$start,$end);
		$data_all[] = $id;
        //$data[] = $id;
		//while(sizeof($data)>0){
		//	$data = array_diff($data,$data_all);
		//	$data_all = array_merge($data_all,$data);
		//	$data = $this->getRealted($data,$start,$end);
        //}
        return $data_all;
    }

    public function getRealted($id,$start,$end){
        $data_temp = array();
        $q = "SELECT user_id_contacted id
        FROM mart_user_contact_kelurahan WHERE user_id IN ('".implode("','",$id)."') AND ds between $start and $end;";
        //echo $q;
        $query = $this->db->query($q);
        $data = $query->result_array();
        foreach($data as $d){
            if($d['id']!=null)
                $data_temp[]=$d['id'];
        }
        return $data_temp;
    }
// ds,user_id,nama,no_hp,status,mac_address,tgl_terdaftar,mac_address_contacted,user_id_contacted,jml_kontak,first_contacted,last_contacted,lama_kontak,nama_contacted,no_hp_contacted,status_contacted,tgl_terdaftar_contacted,latitude,longitude,kelurahan,esri_oid,shape,kode_desa,desa_kelurahan,kecamatan,kabupaten,provinsi,zone_class,shp_point
    public function getNode(){
        //$query = $this->db->query("SELECT DISTINCT user_id id,1 \"group\",nama,no_hp,status,mac_address,tgl_terdaftar FROM mart_user_contact_kelurahan WHERE ds BETWEEN ? AND ? and user_id=?;",array($this->start,$this->end,$this->user_id));
        $query = $this->db->query("SELECT DISTINCT user_id id,1 \"group\",full_name nama,mobile_number no_hp,status
        FROM dma_pl_users where user_id=?;",array($this->user_id));
        return $query->result_array();
    }
}
?>