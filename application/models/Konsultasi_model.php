<?php
class Konsultasi_model extends CI_Model
{

    public function __construct()
    {
        // $this->load->database();
        parent::__construct();
        $this->db = $this
            ->load
            ->database('default', true);

    }
    public function add_transaction_konsultasi($data)
    {
        // $query = $this->db->insert('konsultasi',$data);
        $hasil = $this
            ->db
            ->query("INSERT into konsultasi (email, nama_produk, asal_produk, target_pasar, tanggal_pengiriman, status) 
            values (?,?,?,?,?,?);", array(
            $data['email'],
            $data['nama_produk'],
            $data['asal_produk'],
            $data['target_pasar'],
            $data['tanggal_pengiriman'],
            $data['status']
        ));
        if (!$hasil)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $hasil;
        }
    }
    public function get_transaction_konsultasi()
    {

        $sql = "SELECT t1.id_konsultasi, t1.email, t1.nama_produk,t2.nama, t1.asal_produk,t1.target_pasar,date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM konsultasi t1 , user t2 WHERE t1.email = t2.email AND (t1.status = 'on-progress' OR t1.status = 'offer');";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_transaction_konsultasi_done()
    {

        $sql = "SELECT t1.id_konsultasi, t1.email, t1.nama_produk,t2.nama,t1.asal_produk,t1.target_pasar, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM konsultasi t1 , user t2 WHERE t1.email = t2.email AND t1.status = 'done';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_transaction_konsultasi_reject()
    {

        $sql = "SELECT t1.id_konsultasi, t1.email, t1.nama_produk,t2.nama,t1.asal_produk,t1.target_pasar, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM konsultasi t1 , user t2 WHERE t1.email = t2.email AND t1.status = 'reject';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_detail_konsultasi($id)
    {

        $sql = "SELECT t1.id_konsultasi, t1.email, t1.nama_produk,t2.nama,t1.asal_produk,t1.target_pasar, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM konsultasi t1 , user t2 WHERE t1.email = t2.email AND t1.id_konsultasi = $id ;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }

    public function get_action_konsultasi($id, $status)
    {

        $sql = "UPDATE konsultasi set status = '$status' where id_konsultasi = $id;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return true;
        }

    }
    // public function get_action_konsultasi($id, $status)
    // {

    //     $sql = "UPDATE konsultasi_respon set status = '$status', date_accepted = now() where id_respon = $id;";
    //     $query = $this
    //         ->db
    //         ->query($sql);
    //     if (!$query)
    //     {
    //         return $error = $this
    //             ->db
    //             ->error();
    //     }
    //     else
    //     {
    //         return true;
    //     }

    // }
    public function send_respond_konsultasi($user_id, $respon, $status, $date, $id_konsultasi)
    {
        $sql = "INSERT INTO konsultasi_respon (id_user, respon,status,date, id_konsultasi) values ( '$user_id', '$respon' , '$status' , '$date','$id_konsultasi' );";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return true;
        }

    }

    // USER konsultasi
    public function get_own_transaction_konsultasi($email)
    {

        $sql = "SELECT id_konsultasi, email, nama_produk,asal_produk,target_pasar, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM konsultasi  WHERE email = '$email'  AND (status = 'on-progress' OR status = 'offer');";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_own_transaction_konsultasi_done($email)
    {

        $sql = "SELECT id_konsultasi, email, nama_produk,asal_produk,target_pasar, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM konsultasi  WHERE email = '$email'  AND status = 'done';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_own_transaction_konsultasi_reject($email)
    {

        $sql = "SELECT id_konsultasi, email, nama_produk,asal_produk,target_pasar, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM konsultasi  WHERE email = '$email'  AND status = 'reject';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function update_own_transaction_konsultasi($id, $nama_produk, $asal_produk, $target_pasar)
    {
        $hasil = $this
            ->db
            ->query("UPDATE konsultasi  set	nama_produk = '$nama_produk',	asal_produk = '$asal_produk',	target_pasar = '$target_pasar' 
            WHERE id_konsultasi = '$id';");

        if (!$hasil)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $hasil;
        }
    }

    public function get_respon_konsultasi($email)
    {
        $sql = "SELECT * from konsultasi_respon  WHERE id_user = '$email' ORDER BY 1 DESC;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    public function get_respon_konsultasi_admin()
    {
        $sql = "SELECT * from konsultasi_respon ORDER BY 1 DESC;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    

    public function delete_own_transaction_konsultasi($id_konsultasi)
    {
        $this
            ->db
            ->where('id_konsultasi', $id_konsultasi);
        $query = $this
            ->db
            ->delete('konsultasi');
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query;
        }
    }

    public function get_own_detail_konsultasi($id)
    {

        $sql = "SELECT id_konsultasi, email, nama_produk,asal_produk,target_pasar, date(tanggal_pengiriman) as tanggal_pengiriman, nama_gambar, status  FROM konsultasi WHERE id_konsultasi = $id ;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    public function confirm_offer_konsultasi($id, $respon)
    {

        $sql = "UPDATE konsultasi_respon SET accepted = '$respon' , date_accepted = now() where id_respon = $id;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query;
        }
    }

}

