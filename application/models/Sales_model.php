<?php
class Sales_model extends CI_Model
{

    public function __construct()
    {
        // $this->load->database();
        parent::__construct();
        $this->db = $this
            ->load
            ->database('default', true);

    }
    public function add_transaction_sales($data)
    {
        // $query = $this->db->insert('sales',$data);
        $hasil = $this
            ->db
            ->query("INSERT into sales (email,	nama_produk,	jumlah_produk,	spesifikasi_produk, asal_produk,	target_pasar,	harga_produk,	tanggal_pengiriman,nama_gambar,status) 
            values (?,?,?,?,?,?,?,?,?,?);", array(
            $data['email'],
            $data['nama_produk'],
            $data['jumlah_produk'],
            $data['spesifikasi_produk'],
            $data['asal_produk'],
            $data['target_pasar'],
            $data['harga_produk'],
            $data['tanggal_pengiriman'],
            $data['nama_gambar'],
            $data['status']
        ));
        if (!$hasil)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $hasil;
        }
    }
    public function get_transaction_sales()
    {

        $sql = "SELECT t1.id_sales, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk,t1.spesifikasi_produk, t1.asal_produk,t1.target_pasar,t1.harga_produk, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM sales t1 , user t2 WHERE t1.email = t2.email AND (t1.status = 'on-progress' OR t1.status = 'offer');";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_transaction_sales_done()
    {

        $sql = "SELECT t1.id_sales, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk, t1.spesifikasi_produk, t1.asal_produk,t1.target_pasar,t1.harga_produk, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM sales t1 , user t2 WHERE t1.email = t2.email AND t1.status = 'done';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_transaction_sales_reject()
    {

        $sql = "SELECT t1.id_sales, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk, t1.spesifikasi_produk, t1.asal_produk,t1.target_pasar,t1.harga_produk, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM sales t1 , user t2 WHERE t1.email = t2.email AND t1.status = 'reject';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_detail_sales($id)
    {

        $sql = "SELECT t1.id_sales, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk, t1.spesifikasi_produk, t1.asal_produk,t1.target_pasar,t1.harga_produk, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM sales t1 , user t2 WHERE t1.email = t2.email AND t1.id_sales = $id ;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }

    public function get_action_sales($id, $status)
    {

        $sql = "UPDATE sales set status = '$status' where id_sales = $id;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return true;
        }

    }
    // public function get_action_sales($id, $status)
    // {

    //     $sql = "UPDATE sales_respon set status = '$status', date_accepted = now() where id_respon = $id;";
    //     $query = $this
    //         ->db
    //         ->query($sql);
    //     if (!$query)
    //     {
    //         return $error = $this
    //             ->db
    //             ->error();
    //     }
    //     else
    //     {
    //         return true;
    //     }

    // }
    public function send_respond_sales($user_id, $respon, $status, $date, $id_sales)
    {
        $sql = "INSERT INTO sales_respon (id_user, respon,status,date, id_sales) values ( '$user_id', '$respon' , '$status' , '$date','$id_sales' );";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return true;
        }

    }

    // USER sales
    public function get_own_transaction_sales($email)
    {

        $sql = "SELECT id_sales, email, nama_produk, jumlah_produk,spesifikasi_produk,asal_produk,target_pasar,harga_produk, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM sales  WHERE email = '$email'  AND (status = 'on-progress' OR status = 'offer');";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_own_transaction_sales_done($email)
    {

        $sql = "SELECT id_sales, email, nama_produk, jumlah_produk,spesifikasi_produk, asal_produk,target_pasar,harga_produk, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM sales  WHERE email = '$email'  AND status = 'done';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_own_transaction_sales_reject($email)
    {

        $sql = "SELECT id_sales, email, nama_produk, jumlah_produk,spesifikasi_produk,asal_produk,target_pasar,harga_produk, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM sales  WHERE email = '$email'  AND status = 'reject';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function update_own_transaction_sales($id, $nama_produk, $jumlah_produk, $spesifikasi_produk, $asal_produk, $target_pasar, $harga_produk)
    {
        $hasil = $this
            ->db
            ->query("UPDATE sales  set	nama_produk = '$nama_produk',	jumlah_produk = '$jumlah_produk',	spesifikasi_produk='$spesifikasi_produk', asal_produk = '$asal_produk',	target_pasar = '$target_pasar',	harga_produk = '$harga_produk' 
            WHERE id_sales = '$id';");

        if (!$hasil)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $hasil;
        }
    }

    public function get_respon_sales($email)
    {
        $sql = "SELECT * from sales_respon  WHERE id_user = '$email' ORDER BY 1 DESC;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    public function get_respon_sales_admin()
    {
        $sql = "SELECT * from sales_respon ORDER BY 1 DESC;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    

    public function delete_own_transaction_sales($id_sales)
    {
        $this
            ->db
            ->where('id_sales', $id_sales);
        $query = $this
            ->db
            ->delete('sales');
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query;
        }
    }

    public function get_own_detail_sales($id)
    {

        $sql = "SELECT id_sales, email, nama_produk,  jumlah_produk, spesifikasi_produk, asal_produk,target_pasar,harga_produk, date(tanggal_pengiriman) as tanggal_pengiriman, nama_gambar, status  FROM sales WHERE id_sales = $id ;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    public function confirm_offer_sales($id, $respon)
    {

        $sql = "UPDATE sales_respon SET accepted = '$respon' , date_accepted = now() where id_respon = $id;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query;
        }
    }

}

