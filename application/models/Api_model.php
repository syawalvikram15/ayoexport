<?php
class Api_model extends CI_Model {
    private $nodes = array();
    private $ds;
    public function getNodes(){
        $this->ds = date('Ymd',strtotime($_SESSION['tanggal']));
        $this->nodes =  $this->getNodesId(
            $_SESSION['user_id'],
            $this->ds
        );
        $query = $this->db->query("SELECT * FROM mart_user_contact WHERE user_id IN ?  AND ds=?;",
        array($this->nodes,$this->ds));
        return $query->result_array();
    }
    public function getLinks(){
        $query = $this->db->query("SELECT user_id source ,  IFNULL(`user_id_contacted`,mac_address_contacted) target,1 value FROM `mart_user_contact` WHERE ds=? AND user_id in ?;",array($this->ds,$this->nodes));
        return $query->result_array();
    }
    

    private function getNodesId($id,$ds){
        $data_all= array();
        $data = $this->getRealted([$id],$ds);
        $data[] = $id;
		while(sizeof($data)>0){
			$data = array_diff($data,$data_all);
			$data_all = array_merge($data_all,$data);
			$data = $this->api_model->getRealted($data,$ds);
        }
        return $data_all;
    }

    public function getRealted($id,$ds){
        $data_temp = array();
        $q = "SELECT user_id_contacted id
        FROM `mart_user_contact` WHERE user_id IN ('".implode("','",$id)."') AND ds=$ds;";
        //echo $q;
        $query = $this->db->query($q);
        $data = $query->result_array();
        foreach($data as $d){
            if($d['id']!=null)
                $data_temp[]=$d['id'];
        }
        return $data_temp;
    }

    public function getNode(){
        $query = $this->db->query("SELECT DISTINCT user_id id,1 `group`,full_name nama,mobile_number no_hp,status,mac_address,created_at_local tgl_terdaftar FROM dma_pl_users WHERE user_id=?;",array($_SESSION['user_id']));
        return $query->result_array();
    }
    
    private $tgl_akhir='';
    private $tgl_awal='';
    public function __construct()
    {
        $this->load->database();
        if(isset($_SESSION['filter'])){
            $this->tgl_awal = $_SESSION['filter']['tgl_awal'];
            $this->tgl_akhir = $_SESSION['filter']['tgl_akhir'];
        }
    }

    // SELECT sum(jumlah) as 'total' from mart_user_count?
    public function total_user($tgl_awal,$tgl_akhir){
        $query = $this->db->query("SELECT SUM(total)AS total FROM (
            SELECT DATE(created_at_local) AS tanggal, COUNT(*) AS total FROM dma_pl_users
            WHERE DATE(created_at_local) BETWEEN '$tgl_awal' AND '$tgl_akhir'
            GROUP BY tanggal)AS a;");
        return $query->result_array();
    }
    
    
    public function unique_scanned_dev(){
        $cur_date = date("Ymd");
        $query = $this->db->query("SELECT date(ds) AS ds, jml_dev_terscan
        FROM `mart_macaddress_contacted_count` 
        WHERE ds  BETWEEN  20200327 AND $cur_date
        ORDER BY ds;");
        return $query->result_array();
    }
    public function cont_datascan(){
        $cur_date = date("Ymd");
        $query = $this->db->query("SELECT date(ds) ds, jumlah FROM `mart_total_kontak_count`
        WHERE ds  BETWEEN  20200327 AND $cur_date
        ORDER BY ds asc;");
        return $query->result_array();
    }
    public function activeuser_scan(){
        $cur_date = date("Ymd");
        $query = $this->db->query("SELECT date(ds) ds, jumlah_user FROM `mart_active_user_count`
        WHERE ds  BETWEEN  20200327 AND $cur_date
        ORDER BY ds asc;");
        return $query->result_array();
    }
    public function user_status($tgl_awal,$tgl_akhir){
        $query = $this->db->query("SELECT 
        sum(case when status = 'ODP' then jumlah else 0 end) as 'ODP',
        sum(case when status = 'PDP' then jumlah else 0 end) as 'PDP',
        sum(case when status = 'NOT POSITIVE' then jumlah else 0 end) as 'NOT_POSITIVE',
        sum(case when status = 'POSITIVE' then jumlah else 0 end) as 'POSITIVE'
        FROM (
        
        SELECT DATE(created_at_local) AS tanggal,status, COUNT(*) AS jumlah FROM dma_pl_users
        WHERE DATE(created_at_local) BETWEEN '$tgl_awal' AND '$tgl_akhir'
        GROUP BY tanggal,status) AS a;");
        return $query->result_array();
    }
  
    public function user_maps($tgl_awal,$tgl_akhir){
        $query = $this->db->query("SELECT b.`id_map`,a.* FROM
        (SELECT `provinsi`, SUM(`jml_user_pl`) AS jml_user_pl FROM `mart_prov_kab_usr_regdt`
        WHERE tgl_daftar BETWEEN '$tgl_awal' AND '$tgl_akhir'
        GROUP BY `provinsi`)AS a
        LEFT JOIN 
        (SELECT * FROM `mart_province_user_stg_id_map`) AS b
        ON a.`provinsi`=b.`provinsi`
         ORDER BY jml_user_pl DESC;");
         return $query->result_array();
    }
    public function user_PerODP(){
        $query = $this->db->query("SELECT * from mart_province_user  where id_map <> 'id-3700'  
        ORDER BY pl_per_odp DESC;");
         return $query->result_array();
    }
    public function user_PerPend(){
        $query = $this->db->query("SELECT * from mart_province_user  where id_map <> 'id-3700'  
        ORDER BY pl_per_pend DESC;");
         return $query->result_array();
    }
    public function Update_Kabupaten($res_kabupaten,$tgl_awal,$tgl_akhir){
        $query = $this->db->query("SELECT b.`id_map`,a.* FROM
        (SELECT `provinsi`,`kabupaten`, SUM(`jml_user_pl`) AS jumlah FROM `mart_prov_kab_usr_regdt`
        WHERE tgl_daftar BETWEEN '$tgl_awal' AND '$tgl_akhir' AND `provinsi`='$res_kabupaten'
        GROUP BY `provinsi`,`kabupaten`) AS a
        LEFT JOIN 
        (SELECT * FROM `mart_province_user_stg_id_map`) AS b
        ON a.`provinsi`= b.`provinsi` ORDER BY jumlah desc
        ;");
         return $query->result_array();
    }

}
?>