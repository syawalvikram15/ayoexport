<?php
class Sertifikasi_model extends CI_Model
{

    public function __construct()
    {
        // $this->load->database();
        parent::__construct();
        $this->db = $this
            ->load
            ->database('default', true);

    }
    public function add_transaction_sertifikasi($data)
    {
        // $query = $this->db->insert('sertifikasi',$data);
        $hasil = $this
            ->db
            ->query("INSERT into sertifikasi (email,	nama_produk,	jumlah_produk,	asal_produk,	tujuan_pengiriman,	tanggal_pengiriman,status) 
            values (?,?,?,?,?,?,?);", array(
            $data['email'],
            $data['nama_produk'],
            $data['jumlah_produk'],
            $data['asal_produk'],
            $data['tujuan_pengiriman'],
            $data['tanggal_pengiriman'],
            $data['status']
        ));
        if (!$hasil)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $hasil;
        }
    }
    public function get_transaction_sertifikasi()
    {

        $sql = "SELECT t1.id_sertifikasi, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk,t1.asal_produk,t1.tujuan_pengiriman, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM sertifikasi t1 , user t2 WHERE t1.email = t2.email AND (t1.status = 'on-proggress' OR t1.status = 'offer');";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_transaction_sertifikasi_done()
    {

        $sql = "SELECT t1.id_sertifikasi, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk,t1.asal_produk,t1.tujuan_pengiriman, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM sertifikasi t1 , user t2 WHERE t1.email = t2.email AND t1.status = 'done';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_transaction_sertifikasi_reject()
    {

        $sql = "SELECT t1.id_sertifikasi, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk,t1.asal_produk,t1.tujuan_pengiriman, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM sertifikasi t1 , user t2 WHERE t1.email = t2.email AND t1.status = 'reject';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_detail_sertifikasi($id)
    {

        $sql = "SELECT t1.id_sertifikasi, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk,t1.asal_produk,t1.tujuan_pengiriman, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM sertifikasi t1 , user t2 WHERE t1.email = t2.email AND t1.id_sertifikasi = $id ;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }

    public function get_action_sertifikasi($id, $status)
    {

        $sql = "UPDATE sertifikasi set status = '$status' where id_sertifikasi = $id;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return true;
        }

    }
    // public function get_action_sertifikasi($id, $status)
    // {

    //     $sql = "UPDATE sertifikasi_respon set status = '$status', date_accepted = now() where id_respon = $id;";
    //     $query = $this
    //         ->db
    //         ->query($sql);
    //     if (!$query)
    //     {
    //         return $error = $this
    //             ->db
    //             ->error();
    //     }
    //     else
    //     {
    //         return true;
    //     }

    // }
    public function send_respond_sertifikasi($user_id, $respon, $status, $date, $id_sertifikasi)
    {
        $sql = "INSERT INTO sertifikasi_respon (id_user, respon,status,date, id_sertifikasi) values ( '$user_id', '$respon' , '$status' , '$date','$id_sertifikasi' );";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return true;
        }

    }

    // USER SERTIFIKASI
    public function get_own_transaction_sertifikasi($email)
    {

        $sql = "SELECT id_sertifikasi, email, nama_produk, jumlah_produk,asal_produk,tujuan_pengiriman, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM sertifikasi  WHERE email = '$email'  AND (status = 'on-proggress' OR status = 'offer');";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_own_transaction_sertifikasi_done($email)
    {

        $sql = "SELECT id_sertifikasi, email, nama_produk, jumlah_produk,asal_produk,tujuan_pengiriman, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM sertifikasi  WHERE email = '$email'  AND status = 'done';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_own_transaction_sertifikasi_reject($email)
    {

        $sql = "SELECT id_sertifikasi, email, nama_produk, jumlah_produk,asal_produk,tujuan_pengiriman, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM sertifikasi  WHERE email = '$email'  AND status = 'reject';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function update_own_transaction_sertifikasi($id, $nama_produk, $jumlah_produk, $asal_produk, $tujuan_pengiriman)
    {
        $hasil = $this
            ->db
            ->query("UPDATE sertifikasi  set	nama_produk = '$nama_produk',	jumlah_produk = '$jumlah_produk',	asal_produk = '$asal_produk',	tujuan_pengiriman = '$tujuan_pengiriman' 
            WHERE id_sertifikasi = '$id';");

        if (!$hasil)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $hasil;
        }
    }

    public function get_respon_sertifikasi($email)
    {
        $sql = "SELECT * from sertifikasi_respon  WHERE id_user = '$email' ORDER BY 1 DESC;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    public function get_respon_sertifikasi_admin()
    {
        $sql = "SELECT * from sertifikasi_respon ORDER BY 1 DESC;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    

    public function delete_own_transaction_sertifikasi($id_sertifikasi)
    {
        $this
            ->db
            ->where('id_sertifikasi', $id_sertifikasi);
        $query = $this
            ->db
            ->delete('sertifikasi');
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query;
        }
    }

    public function get_own_detail_sertifikasi($id)
    {

        $sql = "SELECT id_sertifikasi, email, nama_produk,  jumlah_produk,asal_produk,tujuan_pengiriman, date(tanggal_pengiriman) as tanggal_pengiriman, nama_gambar, status  FROM sertifikasi WHERE id_sertifikasi = $id ;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    public function confirm_offer_sertifikasi($id, $respon)
    {

        $sql = "UPDATE sertifikasi_respon SET accepted = '$respon' , date_accepted = now() where id_respon = $id;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query;
        }
    }

}

