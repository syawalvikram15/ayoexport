<?php
class Asuransi_model extends CI_Model
{

    public function __construct()
    {
        // $this->load->database();
        parent::__construct();
        $this->db = $this
            ->load
            ->database('default', true);

    }
    public function add_transaction_asuransi($data)
    {
        // $query = $this->db->insert('asuransi',$data);
        $hasil = $this
            ->db
            ->query("INSERT into asuransi (email,	nama_produk,	jumlah_produk,	volume_produk,	satuan_ukuran,	asal_produk,	tujuan_pengiriman,	estimasi_budget_pengiriman,	kemungkinan_risiko, tanggal_pengiriman,status) 
            values (?,?,?,?,?,?,?,?,?,?,?);", array(
            $data['email'],
            $data['nama_produk'],
            $data['jumlah_produk'],
            $data['volume_produk'],
            $data['satuan_ukuran'],
            $data['asal_produk'],
            $data['tujuan_pengiriman'],
            $data['estimasi_budget_pengiriman'],
            $data['kemungkinan_risiko'],
            $data['tanggal_pengiriman'],
            $data['status']
        ));
        if (!$hasil)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $hasil;
        }
    }
    public function get_transaction_asuransi()
    {

        $sql = "SELECT t1.id_asuransi, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk, t1.volume_produk,t1.satuan_ukuran,t1.asal_produk,t1.tujuan_pengiriman,t1.estimasi_budget_pengiriman,t1.kemungkinan_risiko, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM asuransi t1 , user t2 WHERE t1.email = t2.email AND (t1.status = 'on-progress' OR t1.status = 'offer');";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_transaction_asuransi_done()
    {

        $sql = "SELECT t1.id_asuransi, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk, t1.volume_produk,t1.satuan_ukuran,t1.asal_produk,t1.tujuan_pengiriman,t1.estimasi_budget_pengiriman, t1.kemungkinan_risiko, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM asuransi t1 , user t2 WHERE t1.email = t2.email AND t1.status = 'done';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_transaction_asuransi_reject()
    {

        $sql = "SELECT t1.id_asuransi, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk, t1.volume_produk,t1.satuan_ukuran,t1.asal_produk,t1.tujuan_pengiriman,t1.estimasi_budget_pengiriman, t1.kemungkinan_risiko, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM asuransi t1 , user t2 WHERE t1.email = t2.email AND t1.status = 'reject';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_detail_asuransi($id)
    {

        $sql = "SELECT t1.id_asuransi, t1.email, t1.nama_produk,t2.nama,  t1.jumlah_produk, t1.volume_produk,t1.satuan_ukuran,t1.asal_produk,t1.tujuan_pengiriman,t1.estimasi_budget_pengiriman, t1.kemungkinan_risiko, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM asuransi t1 , user t2 WHERE t1.email = t2.email AND t1.id_asuransi = $id ;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }

    public function get_action_asuransi($id, $status)
    {

        $sql = "UPDATE asuransi set status = '$status' where id_asuransi = $id;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return true;
        }

    }
    // public function get_action_asuransi($id, $status)
    // {

    //     $sql = "UPDATE asuransi_respon set status = '$status', date_accepted = now() where id_respon = $id;";
    //     $query = $this
    //         ->db
    //         ->query($sql);
    //     if (!$query)
    //     {
    //         return $error = $this
    //             ->db
    //             ->error();
    //     }
    //     else
    //     {
    //         return true;
    //     }

    // }
    public function send_respond_asuransi($user_id, $respon, $status, $date, $id_asuransi)
    {
        $sql = "INSERT INTO asuransi_respon (id_user, respon,status,date, id_asuransi) values ( '$user_id', '$respon' , '$status' , '$date','$id_asuransi' );";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return true;
        }

    }

    // USER asuransi
    public function get_own_transaction_asuransi($email)
    {

        $sql = "SELECT id_asuransi, email, nama_produk, jumlah_produk, volume_produk,satuan_ukuran,asal_produk,tujuan_pengiriman,estimasi_budget_pengiriman,kemungkinan_risiko, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM asuransi  WHERE email = '$email'  AND (status = 'on-progress' OR status = 'offer');";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_own_transaction_asuransi_done($email)
    {

        $sql = "SELECT id_asuransi, email, nama_produk, jumlah_produk, volume_produk,satuan_ukuran,asal_produk,tujuan_pengiriman,estimasi_budget_pengiriman,kemungkinan_risiko, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM asuransi  WHERE email = '$email'  AND status = 'done';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_own_transaction_asuransi_reject($email)
    {

        $sql = "SELECT id_asuransi, email, nama_produk, jumlah_produk, volume_produk,satuan_ukuran,asal_produk,tujuan_pengiriman,estimasi_budget_pengiriman,kemungkinan_risiko, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM asuransi  WHERE email = '$email'  AND status = 'reject';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function update_own_transaction_asuransi($id, $nama_produk, $jumlah_produk, $volume_produk, $satuan_ukuran, $asal_produk, $tujuan_pengiriman, $estimasi_budget_pengiriman, $kemungkinan_risiko)
    {
        $hasil = $this
            ->db
            ->query("UPDATE asuransi  set	nama_produk = '$nama_produk',	jumlah_produk = '$jumlah_produk',	volume_produk = '$volume_produk',	satuan_ukuran = '$satuan_ukuran',	asal_produk = '$asal_produk',	tujuan_pengiriman = '$tujuan_pengiriman',	estimasi_budget_pengiriman = '$estimasi_budget_pengiriman', kemungkinan_risiko = '$kemungkinan_risiko' 
            WHERE id_asuransi = '$id';");

        if (!$hasil)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $hasil;
        }
    }

    public function get_respon_asuransi($email)
    {
        $sql = "SELECT * from asuransi_respon  WHERE id_user = '$email' ORDER BY 1 DESC;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    public function get_respon_asuransi_admin()
    {
        $sql = "SELECT * from asuransi_respon ORDER BY 1 DESC;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    

    public function delete_own_transaction_asuransi($id_asuransi)
    {
        $this
            ->db
            ->where('id_asuransi', $id_asuransi);
        $query = $this
            ->db
            ->delete('asuransi');
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query;
        }
    }

    public function get_own_detail_asuransi($id)
    {

        $sql = "SELECT id_asuransi, email, nama_produk,  jumlah_produk, volume_produk,satuan_ukuran,asal_produk,tujuan_pengiriman,estimasi_budget_pengiriman,kemungkinan_risiko, date(tanggal_pengiriman) as tanggal_pengiriman, nama_gambar, status  FROM asuransi WHERE id_asuransi = $id ;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    public function confirm_offer_asuransi($id, $respon)
    {

        $sql = "UPDATE asuransi_respon SET accepted = '$respon' , date_accepted = now() where id_respon = $id;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query;
        }
    }

}

