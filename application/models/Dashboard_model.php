<?php
class Dashboard_model extends CI_Model
{

    public function __construct()
    {
        // $this->load->database();
        parent::__construct();
        $this->db = $this
            ->load
            ->database('default', true);

    }
    public function get_card_logistik()
    {
        $email_user = $this->session->email;

        $sql = "SELECT (select count(status) from logistik where status = 'done' and email = '$email_user') as done, 
        (select count(status) from logistik where status = 'reject' and email = '$email_user') as reject ,
         (select count(status) from logistik where status = 'on-proggress' OR status = 'on-progress' and email = '$email_user') as 'on_proggress' 
         FROM `logistik`  where email = '$email_user' LIMIT 1";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result();
        }
    }

    public function get_card_konsultasi()
    {
        $email_user = $this->session->email;

        $sql = "SELECT (select count(status) from konsultasi where status = 'done' and email = '$email_user') as done, 
        (select count(status) from konsultasi where status = 'reject' and email = '$email_user') as reject ,
         (select count(status) from konsultasi where status = 'on-proggress' OR status = 'on-progress' and email = '$email_user') as 'on_proggress' 
         FROM `konsultasi`  where email = '$email_user' LIMIT 1";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result();
        }
    }



    public function get_card_asuransi()
    {
        $email_user = $this->session->email;

        $sql = "SELECT (select count(status) from asuransi where status = 'done' and email = '$email_user') as done, 
        (select count(status) from asuransi where status = 'reject' and email = '$email_user') as reject ,
         (select count(status) from asuransi where status = 'on-proggress' OR status = 'on-progress' and email = '$email_user') as 'on_proggress' 
         FROM `asuransi`  where email = '$email_user' LIMIT 1";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result();
        }
    }

    public function get_card_pembiayaan()
    {
        $email_user = $this->session->email;

        $sql = "SELECT (select count(status) from pembiayaan where status = 'done' and email = '$email_user') as done, 
        (select count(status) from pembiayaan where status = 'reject' and email = '$email_user') as reject ,
         (select count(status) from pembiayaan where status = 'on-proggress' OR status = 'on-progress' and email = '$email_user') as 'on_proggress' 
         FROM `pembiayaan`  where email = '$email_user' LIMIT 1";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result();
        }
    }

    public function get_card_marketanalisis()
    {
        $email_user = $this->session->email;

        $sql = "SELECT (select count(status) from marketanalisis where status = 'done' and email = '$email_user') as done, 
        (select count(status) from marketanalisis where status = 'reject' and email = '$email_user') as reject ,
         (select count(status) from marketanalisis where status = 'on-proggress' OR status = 'on-progress' and email = '$email_user') as 'on_proggress' 
         FROM `marketanalisis`  where email = '$email_user' LIMIT 1";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result();
        }
    }


    public function get_card_regulasi()
    {
        $email_user = $this->session->email;

        $sql = "SELECT (select count(status) from regulasi where status = 'done' and email = '$email_user') as done, 
        (select count(status) from regulasi where status = 'reject' and email = '$email_user') as reject ,
         (select count(status) from regulasi where status = 'on-proggress' OR status = 'on-progress' and email = '$email_user') as 'on_proggress' 
         FROM `regulasi`  where email = '$email_user' LIMIT 1";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result();
        }
    }


    public function get_card_sales()
    {
        $email_user = $this->session->email;

        $sql = "SELECT (select count(status) from sales where status = 'done' and email = '$email_user') as done, 
        (select count(status) from sales where status = 'reject' and email = '$email_user') as reject ,
         (select count(status) from sales where status = 'on-proggress' OR status = 'on-progress' and email = '$email_user') as 'on_proggress' 
         FROM `sales`  where email = '$email_user' LIMIT 1";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result();
        }
    }


    public function get_card_sertifikasi()
    {
        $email_user = $this->session->email;

        $sql = "SELECT (select count(status) from sertifikasi where status = 'done' and email = '$email_user') as done, 
        (select count(status) from sertifikasi where status = 'reject' and email = '$email_user') as reject ,
         (select count(status) from sertifikasi where status = 'on-proggress' OR status = 'on-progress' and email = '$email_user') as 'on_proggress' 
         FROM `sertifikasi`  where email = '$email_user' LIMIT 1";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result();
        }
    }

    public function get_line_chart()
    {
        $email_user = $this->session->email;

        $sql = "SELECT tanggal_pengiriman, MAX(CASE WHEN status = 'done' THEN totalan else 0 END) `done`, 
        MAX(CASE WHEN status = 'reject' 
        THEN totalan else 0 END) `reject`, MAX(CASE WHEN status = 'on-progress' OR status = 'on-proggress' 
        THEN totalan else 0 END) `on_progress` FROM (select tanggal_pengiriman, status, sum(`value`) as totalan 
        from (SELECT date(`tanggal_pengiriman`) as tanggal_pengiriman, status as status, count(`tanggal_pengiriman`) as value FROM sales 
        WHERE email = '$email_user' GROUP BY tanggal_pengiriman) s GROUP BY tanggal_pengiriman, status ) s GROUP BY tanggal_pengiriman";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result();
        }
    }


}

