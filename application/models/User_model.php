<?php
class User_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function get_email($id)
	{
		$query = $this->db->query("SELECT email FROM user WHERE id = $id;");
		return $query->row_array();
	}

	public function login($data)
	{
		$query = $this->db->query("select nama,hp,email,level,unit from user where active=1 and email=? and password=md5(?);", array($data['user'], $data['pass']));
		return $query->row_array();
	}
	public function register($f)
	{
		$query = $this->db->query("insert into user(nama,email,hp,password,alamat,level,active) values(?,?,?,md5(?),?,?,0)", array($f['nama'], $f['email'], $f['hp'], $f['password'], $f['alamat'], $f['level']));
		//return $query->row_array();
	}
	public function getMenu()
	{
		$query = $this->db->query("SELECT t0.* FROM menu t0 LEFT JOIN user_menu t1 USING(id_menu) WHERE t1.level=? order by t0.level", array($_SESSION['level']));
		return $query->result_array();
	}
	public function getAllMenu()
	{
		$query = $this->db->query("select * from menu order by level;");
		return $query->result_array();
	}
	public function getAllLevel()
	{
		$query = $this->db->query("SELECT DISTINCT level FROM user order by 1;");
		return $query->result_array();
	}
	public function getAllUserMenu()
	{
		$query = $this->db->query("SELECT level,GROUP_CONCAT(id_menu ORDER BY 1)levels FROM user_menu GROUP BY 1;");
		return $query->result_array();
	}
	public function saveUserMenu($q)
	{
		$this->db->query("truncate user_menu;");
		$this->db->query($q);
	}
}
