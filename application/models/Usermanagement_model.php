<?php
class Usermanagement_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function getAllMenu()
	{
		$query = $this->db->query("select *
									from menu
									-- order by level
									ORDER BY FIELD(id_menu, 1, 15, 12, 8, 14, 6, 4, 13, 10), id_menu asc
								;");
		if (!$query) {
			return $error 	 = $this->db->error();
		} else {
			return $query->result_array();
		}
	}


	public function getAllLevel()
	{
		$query = $this->db->query("SELECT DISTINCT level FROM user;");
		if (!$query) {
			return $error 	 = $this->db->error();
		} else {
			return $query->result_array();
		}
	}


	public function getAllUserMenu()
	{
		$query = $this->db->query("SELECT level,GROUP_CONCAT(id_menu ORDER BY 1)levels FROM user_menu GROUP BY 1;");
		if (!$query) {
			return $error 	 = $this->db->error();
		} else {
			return $query->result_array();
		}
	}


	public function saveUserMenu($q)
	{
		$this->db->query("truncate user_menu;");
		$query = $this->db->query($q);
		if (!$query) {
			return $error 	 = $this->db->error();
		} else {
			return $query;
		}
	}


	public function get_data_user()
	{
		$sql = "select * from user;";
		$query = $this->db->query($sql);
		if (!$query) {
			return $error 	 = $this->db->error();
		} else {
			return $query->result_array();
		}
	}

	public function delete_data_user($username)
	{
		$this->db->where('email', $username);
		$query = $this->db->delete('user');
		if (!$query) {
			return $error 	 = $this->db->error();
		} else {
			return $query;
		}
	}
	public function check_username($username)
	{
		$sql = "select email from user where email = '" . $username . "'";
		$query = $this->db->query($sql);
		$query->num_rows();
		if (!$query) {
			return $error 	 = $this->db->error();
		} else {
			// return $query->num_rows();
			if ($query->num_rows() > 0) {
				return false;
			} else {
				return true;
			}
		}
	}


	public function check_data_username($username)
	{
		$sql = "select * from user where email = '" . $username . "'";
		$query = $this->db->query($sql);
		if (!$query) {
			return $error 	 = $this->db->error();
		} else {
			return $query->row();
		}
	}


	public function store_data_user($data)
	{
		$hasil = $this->db->insert('user', $data);
		if (!$hasil) {
			return $error 	 = $this->db->error();
		} else {
			return $hasil;
		}
	}


	public function update_data_user($data)
	{
		$hasil = $this->db->replace('user', $data);
		if (!$hasil) {
			return $error 	 = $this->db->error();
		} else {
			return $hasil;
		}
	}
	// 	public function get_user_level()
	// {
	// 	$query = $this->db->query("SELECT * FROM user_level;");
	// 	if (!$query) {
	// 		return $error 	 = $this->db->error();
	// 	} else {
	// 		return $query->result_array();
	// 	}
	// }
	public function add_level($level_name,$level_display)
	{
		$query = $this->db->query("INSERT INTO user_level (level, title) values ('$level_display','$level_name')");
		$query;
	}

	
}
