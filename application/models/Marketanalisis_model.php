<?php
class Marketanalisis_model extends CI_Model
{

    public function __construct()
    {
        // $this->load->database();
        parent::__construct();
        $this->db = $this
            ->load
            ->database('default', true);

    }
    public function add_transaction_marketanalisis($data)
    {
        // $query = $this->db->insert('marketanalisis',$data);
        $hasil = $this
            ->db
            ->query("INSERT into marketanalisis (email,	nama_produk,	kapasitas_produksi,	asal_produk,	target_pasar, tanggal_pengiriman,status) 
            values (?,?,?,?,?,?,?);", array(
            $data['email'],
            $data['nama_produk'],
            $data['kapasitas_produksi'],
            $data['asal_produk'],
            $data['target_pasar'],
            $data['tanggal_pengiriman'],
            $data['status']
        ));
        if (!$hasil)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $hasil;
        }
    }
    public function get_transaction_marketanalisis()
    {

        $sql = "SELECT t1.id_marketanalisis, t1.email, t1.nama_produk,t2.nama,  t1.kapasitas_produksi, t1.asal_produk,t1.target_pasar, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM marketanalisis t1 , user t2 WHERE t1.email = t2.email AND (t1.status = 'on-progress' OR t1.status = 'offer');";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_transaction_marketanalisis_done()
    {

        $sql = "SELECT t1.id_marketanalisis, t1.email, t1.nama_produk,t2.nama,  t1.kapasitas_produksi, t1.asal_produk,t1.target_pasar, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM marketanalisis t1 , user t2 WHERE t1.email = t2.email AND t1.status = 'done';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_transaction_marketanalisis_reject()
    {

        $sql = "SELECT t1.id_marketanalisis, t1.email, t1.nama_produk,t2.nama,  t1.kapasitas_produksi, t1.asal_produk,t1.target_pasar, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM marketanalisis t1 , user t2 WHERE t1.email = t2.email AND t1.status = 'reject';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_detail_marketanalisis($id)
    {

        $sql = "SELECT t1.id_marketanalisis, t1.email, t1.nama_produk,t2.nama,  t1.kapasitas_produksi, t1.asal_produk,t1.target_pasar, date(t1.tanggal_pengiriman) as tanggal_pengiriman,t1.nama_gambar,t1.status  FROM marketanalisis t1 , user t2 WHERE t1.email = t2.email AND t1.id_marketanalisis = $id ;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }

    public function get_action_marketanalisis($id, $status)
    {

        $sql = "UPDATE marketanalisis set status = '$status' where id_marketanalisis = $id;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return true;
        }

    }
    // public function get_action_marketanalisis($id, $status)
    // {

    //     $sql = "UPDATE marketanalisis_respon set status = '$status', date_accepted = now() where id_respon = $id;";
    //     $query = $this
    //         ->db
    //         ->query($sql);
    //     if (!$query)
    //     {
    //         return $error = $this
    //             ->db
    //             ->error();
    //     }
    //     else
    //     {
    //         return true;
    //     }

    // }
    public function send_respond_marketanalisis($user_id, $respon, $status, $date, $id_marketanalisis)
    {
        $sql = "INSERT INTO marketanalisis_respon (id_user, respon,status,date, id_marketanalisis) values ( '$user_id', '$respon' , '$status' , '$date','$id_marketanalisis' );";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return true;
        }

    }

    // USER marketanalisis
    public function get_own_transaction_marketanalisis($email)
    {

        $sql = "SELECT id_marketanalisis, email, nama_produk, kapasitas_produksi, asal_produk,target_pasar, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM marketanalisis  WHERE email = '$email'  AND (status = 'on-progress' OR status = 'offer');";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_own_transaction_marketanalisis_done($email)
    {

        $sql = "SELECT id_marketanalisis, email, nama_produk, kapasitas_produksi,asal_produk,target_pasar, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM marketanalisis  WHERE email = '$email'  AND status = 'done';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function get_own_transaction_marketanalisis_reject($email)
    {

        $sql = "SELECT id_marketanalisis, email, nama_produk, kapasitas_produksi,asal_produk,target_pasar, date(tanggal_pengiriman) as tanggal_pengiriman,nama_gambar, status  FROM marketanalisis  WHERE email = '$email'  AND status = 'reject';";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }

    }
    public function update_own_transaction_marketanalisis($id, $nama_produk, $kapasitas_produksi, $asal_produk, $target_pasar)
    {
        $hasil = $this
            ->db
            ->query("UPDATE marketanalisis  set	nama_produk = '$nama_produk',	kapasitas_produksi = '$kapasitas_produksi',	asal_produk = '$asal_produk',	target_pasar = '$target_pasar' 
            WHERE id_marketanalisis = '$id';");

        if (!$hasil)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $hasil;
        }
    }

    public function get_respon_marketanalisis($email)
    {
        $sql = "SELECT * from marketanalisis_respon  WHERE id_user = '$email' ORDER BY 1 DESC;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    public function get_respon_marketanalisis_admin()
    {
        $sql = "SELECT * from marketanalisis_respon ORDER BY 1 DESC;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    

    public function delete_own_transaction_marketanalisis($id_marketanalisis)
    {
        $this
            ->db
            ->where('id_marketanalisis', $id_marketanalisis);
        $query = $this
            ->db
            ->delete('marketanalisis');
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query;
        }
    }

    public function get_own_detail_marketanalisis($id)
    {

        $sql = "SELECT id_marketanalisis, email, nama_produk,  kapasitas_produksi,asal_produk,target_pasar, date(tanggal_pengiriman) as tanggal_pengiriman, nama_gambar, status  FROM marketanalisis WHERE id_marketanalisis = $id ;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query->result_array();
        }
    }

    public function confirm_offer_marketanalisis($id, $respon)
    {

        $sql = "UPDATE marketanalisis_respon SET accepted = '$respon' , date_accepted = now() where id_respon = $id;";
        $query = $this
            ->db
            ->query($sql);
        if (!$query)
        {
            return $error = $this
                ->db
                ->error();
        }
        else
        {
            return $query;
        }
    }

}

