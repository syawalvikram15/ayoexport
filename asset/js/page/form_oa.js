$("#tbl1").DataTable({
    paging: false,
    searching: false,
    info: false
});

Highcharts.chart('chart1', {
    chart: {
        type: 'variablepie'
    },
    title: {
        text: ''
    },
    credits:{
        enabled : false
    },
    tooltip: {
        headerFormat: ''
        //pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
        //    'Area (square km): <b>{point.y}</b><br/>' +
        //    'Population density (people per square km): <b>{point.z}</b><br/>'
    },
    series: [{
        minPointSize: 10,
        innerSize: '30%',
        name: 'Demografi',
        data: [{
            name: 'Laki-Laki',
            y: 505370
        }, {
            name: 'Perempuan',
            y: 551500
        }]
    }]
});

function updateChart(){
    
}