// Script bersama
var base_url = window.location.origin;



function update_card(){
    //GET CARD Logistik
    $.ajax({
        type: "POST",
        url: base_url + "/ayoexport/dashboard/get_card_logistik",
        success: function (response) {
        data = JSON.parse(response);

        if (response == "[]") {
            $("#card-logistik-done").html("0");
            $("#card-logistik-reject").html("0");
            $("#card-logistik-on-progress").html("0");
        } else {
        for (var car of data) {
            var done = parseInt(car.done)
                .toString()
                .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $("#card-logistik-done").html(done);

            var reject = parseInt(car.reject)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
             $("#card-logistik-reject").html(reject);

            var on_progress = parseInt(car.on_proggress)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $("#card-logistik-on-progress").html(on_progress);
            }
        }
        },
    });

    //GET CARD asuransi
$.ajax({
    type: "POST",
    url: base_url + "/ayoexport/dashboard/get_card_asuransi",
    success: function (response) {
    data = JSON.parse(response);

    if (response == "[]") {
        $("#card-asuransi-done").html("0");
        $("#card-asuransi-reject").html("0");
        $("#card-asuransi-on-progress").html("0");
    } else {
    for (var car of data) {
        var done = parseInt(car.done)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        $("#card-asuransi-done").html(done);

        var reject = parseInt(car.reject)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
         $("#card-asuransi-reject").html(reject);

        var on_progress = parseInt(car.on_proggress)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        $("#card-asuransi-on-progress").html(on_progress);
        }
    }
    },
});

//GET CARD konsultasi
$.ajax({
    type: "POST",
    url: base_url + "/ayoexport/dashboard/get_card_konsultasi",
    success: function (response) {
    data = JSON.parse(response);

    if (response == "[]") {
        $("#card-konsultasi-done").html("0");
        $("#card-konsultasi-reject").html("0");
        $("#card-konsultasi-on-progress").html("0");
    } else {
    for (var car of data) {
        var done = parseInt(car.done)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        $("#card-konsultasi-done").html(done);

        var reject = parseInt(car.reject)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
         $("#card-konsultasi-reject").html(reject);

        var on_progress = parseInt(car.on_proggress)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        $("#card-konsultasi-on-progress").html(on_progress);
        }
    }
    },
});

//GET CARD marketanalisis
$.ajax({
    type: "POST",
    url: base_url + "/ayoexport/dashboard/get_card_marketanalisis",
    success: function (response) {
    data = JSON.parse(response);

    if (response == "[]") {
        $("#card-marketanalisis-done").html("0");
        $("#card-marketanalisis-reject").html("0");
        $("#card-marketanalisis-on-progress").html("0");
    } else {
    for (var car of data) {
        var done = parseInt(car.done)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, "."); 
        $("#card-marketanalisis-done").html(done);

        var reject = parseInt(car.reject)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
         $("#card-marketanalisis-reject").html(reject);

        var on_progress = parseInt(car.on_proggress)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        $("#card-marketanalisis-on-progress").html(on_progress);
        }
    }
    },
});


//GET CARD pembiayaan
$.ajax({
    type: "POST",
    url: base_url + "/ayoexport/dashboard/get_card_pembiayaan",
    success: function (response) {
    data = JSON.parse(response);

    if (response == "[]") {
        $("#card-pembiayaan-done").html("0");
        $("#card-pembiayaan-reject").html("0");
        $("#card-pembiayaan-on-progress").html("0");
    } else {
    for (var car of data) {
        var done = parseInt(car.done)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        $("#card-pembiayaan-done").html(done);

        var reject = parseInt(car.reject)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
         $("#card-pembiayaan-reject").html(reject);

        var on_progress = parseInt(car.on_proggress)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        $("#card-pembiayaan-on-progress").html(on_progress);
        }
    }
    },
});

//GET CARD regulasi
$.ajax({
    type: "POST",
    url: base_url + "/ayoexport/dashboard/get_card_regulasi",
    success: function (response) {
    data = JSON.parse(response);

    if (response == "[]") {
        $("#card-regulasi-done").html("0");
        $("#card-regulasi-reject").html("0");
        $("#card-regulasi-on-progress").html("0");
    } else {
    for (var car of data) {
        var done = parseInt(car.done)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        $("#card-regulasi-done").html(done);

        var reject = parseInt(car.reject)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
         $("#card-regulasi-reject").html(reject);

        var on_progress = parseInt(car.on_proggress)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        $("#card-regulasi-on-progress").html(on_progress);
        }
    }
    },
});


//GET CARD sales
$.ajax({
    type: "POST",
    url: base_url + "/ayoexport/dashboard/get_card_sales",
    success: function (response) {
    data = JSON.parse(response);

    if (response == "[]") {
        $("#card-sales-done").html("0");
        $("#card-sales-reject").html("0");
        $("#card-sales-on-progress").html("0");
    } else {
    for (var car of data) {
        var done = parseInt(car.done)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        $("#card-sales-done").html(done);

        var reject = parseInt(car.reject)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
         $("#card-sales-reject").html(reject);

        var on_progress = parseInt(car.on_proggress)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        $("#card-sales-on-progress").html(on_progress);
        }
    }
    },
});

//GET CARD sertifikasi
$.ajax({
    type: "POST",
    url: base_url + "/ayoexport/dashboard/get_card_sertifikasi",
    success: function (response) {
    data = JSON.parse(response);

    if (response == "[]") {
        $("#card-sertifikasi-done").html("0");
        $("#card-sertifikasi-reject").html("0");
        $("#card-sertifikasi-on-progress").html("0");
    } else {
    for (var car of data) {
        var done = parseInt(car.done)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        $("#card-sertifikasi-done").html(done);

        var reject = parseInt(car.reject)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
         $("#card-sertifikasi-reject").html(reject);

        var on_progress = parseInt(car.on_proggress)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        $("#card-sertifikasi-on-progress").html(on_progress);
        }
    }
    },
});

//GET LINE CHART
$.ajax({
    type: 'POST',
    url: base_url + "/ayoexport/dashboard/get_line_chart",
    async: false, //execute script synchronously
    success: function (response) {
        data = JSON.parse(response);
        const categories = []
        const done = [];
        const reject = [];
        const on_progress = [];
    
        // console.log(data);
        for (var car of data) {
            categories.push(car.tanggal_pengiriman)
            done.push(parseFloat(car.done))
            reject.push(parseFloat(car.reject))
            on_progress.push(parseFloat(car.on_progress))
        }
        chart_line_chart(categories,done,reject,on_progress)
        var sum_done = done.reduce(function(a, b){ return a + b;}, 0)
        var sum_reject = reject.reduce(function(a, b){ return a + b;}, 0)
        var sum_on_progress = on_progress.reduce(function(a, b){ return a + b;}, 0)
      
        chart_pie_chart(sum_done,sum_reject,sum_on_progress)
    }
});
}

update_card()





// Expand/CollapseStructureasuransi
// Expand/CollapseStructureasuransi_respon
// Expand/CollapseStructureclient
// Expand/CollapseStructuredma_facebook
// Expand/CollapseStructuredma_fb_campaign
// Expand/CollapseStructuredma_summary
// Expand/CollapseStructurekonsultasi
// Expand/CollapseStructurekonsultasi_responHide
// Expand/CollapseStructurelogistik
// Expand/CollapseStructurelogistik_respon
// Expand/CollapseStructuremarketanalisis
// Expand/CollapseStructuremarketanalisis_respon
// Expand/CollapseStructuremenu
// Expand/CollapseStructuremenu_utama_ekonomi
// Expand/CollapseStructurepembiayaan
// Expand/CollapseStructurepembiayaan_respon
// Expand/CollapseStructureregulasi
// Expand/CollapseStructureregulasi_respon
// Expand/CollapseStructuresales
// Expand/CollapseStructuresales_respon
// Expand/CollapseStructuresertifikasi
// Expand/CollapseStructuresertifikasi_respon



function chart_line_chart(categories,done,reject,on_progress){
    Highcharts.chart('container-line-chart', {
    
        title: {
            text: 'Total Sales'
        },
    
        subtitle: {
            text: ''
        },
    
        yAxis: {
            title: {
                text: 'Total'
            }
        },
        tooltip: {
            shared: true
        },
    
        xAxis: {
            categories: categories,
            crosshair: true,
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
    
        series: [{
            name: 'Done',
            data: done
        },{
            name: 'Reject',
            data: reject
        },{
            name: 'On-progress',
            data: on_progress
        }],
    
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    
    });
    }


function  chart_pie_chart(sum_done,sum_reject,sum_on_progress){
    Highcharts.chart('container-pie-chart', {
        chart: {
            type: 'variablepie'
        },
        title: {
            text: 'Compare of Sales'
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
                'Total: <b>{point.y}</b><br/>'
        },
        series: [{
            minPointSize: 10,
            innerSize: '20%',
            zMin: 0,
            name: 'countries',
            data: [{
                name: 'Done',
                y: sum_done,
                z: 100
            }, {
                name: 'Rejected',
                y: sum_reject,
                z: 100
            }, {
                name: 'On-progress',
                y: sum_on_progress,
                z: 100
            }]
        }]
    });
    }