var base_url = window.location.origin;
// $('#form-update').submit(function() { 
function add_transaction() {

    var nama_produk = document.getElementById("nama_produk").value;
    var jumlah_produk = document.getElementById("jumlah_produk").value;
    var asal_produk = document.getElementById("asal_produk").value;
    var tujuan_pengiriman = document.getElementById("tujuan_pengiriman").value;
    var estimasi_modal = document.getElementById("estimasi_modal").value;

    if (![nama_produk, jumlah_produk, asal_produk, tujuan_pengiriman, estimasi_modal].every(Boolean)) {
        swal("Oops!", "Pastikan seluruh input terisi!", "warning");
    } else {
        $.ajax({
            type: "POST",
            url: base_url + "/ayoexport/pembiayaan/add_transaction_pembiayaan",
            async: true, //execute script synchronously,
            data: {
                nama_produk: nama_produk,
                jumlah_produk: jumlah_produk,
                asal_produk: asal_produk,
                tujuan_pengiriman: tujuan_pengiriman,
                estimasi_modal: estimasi_modal

            },
            beforeSend: function() {
                // Show LOADING icon
                $("#loader").show();
            },
            success: function(response) {
                swal("Selamat", "Transaksi anda berhasil ditambahkan. Silahkan menunggu konfirmasi di kolom Announcement", "success");
                $('#tambah-transaksi').modal('hide');
                document.getElementById('close-modal-tambah').click();
            },
            complete: function() {
                // Show LOADING icon
                $("#loader").hide();
                document.getElementById("formTambah").reset();
                setInterval(function(){
                    window.location.reload();
                },2000);
            },
        });
    }
};
var table_data;
var current_id = 0;

load_data()

function load_data() {
    table_data = $("#data-table").DataTable({
        responsive: true,
        paging: true,
        info: true,
        searching: true,
        order: [
            [1, "desc"]
        ],
        pageLength: 5,
        lengthMenu: [5, 10, 25, 50, 75],
        columns: [{
                mRender: function(data, type, row) {
                    var id = row.id_pembiayaan;
                    return (
                        '<button  class="btn btn-xs btn-danger m-b-10" onclick="delete_transaction(' +
                        id + ');" title="Delete"><i class="fa fa-user-times"></i></button> &nbsp;' +
                        '<button  class="btn btn-xs btn-warning" onclick="show_edit_transaction(' + id + ')" data-toggle="modal" data-target="#modal-edit" title="Edit"><i class="fa fa-eye"></i></button>'
                    );
                },
                title: "Action",
            },
             {
                data: "id_pembiayaan",
                title: "Id Transaksi"
            },
            {
                data: "nama_produk",
                title: "Nama Produk"
            },
            {
                data: "tujuan_pengiriman",
                title: "Tujuan Pengiriman"
            },
            {
                data: "tanggal_pengiriman",
                title: "Tanggal"
            }
        ],
        processing: true,
    });

    table_data2 = $("#data-table2").DataTable({
        responsive: true,
        paging: true,
        info: true,
        searching: true,
        order: [
            [1, "desc"]
        ],
        pageLength: 5,
        lengthMenu: [5, 10, 25, 50, 75],
        columns: [{
                mRender: function(data, type, row) {
                    var id = row.id_pembiayaan;
                    return (

                        '<button  class="btn btn-xs btn-warning" onclick="show_detail_transaction(' + id + ')" data-toggle="modal" data-target="#modal-edit" title="Edit"><i class="fa fa-eye"></i></button>'
                    );
                },
                title: "Action",
            },
             {
                data: "id_pembiayaan",
                title: "Id Transaksi"
            },
            {
                data: "nama_produk",
                title: "Nama Produk"
            },
            {
                data: "tujuan_pengiriman",
                title: "Tujuan Pengiriman"
            },
            {
                data: "tanggal_pengiriman",
                title: "Tanggal"
            }
        ],
        processing: true,
    });

    table_data3 = $("#data-table3").DataTable({
        responsive: true,
        paging: true,
        info: true,
        searching: true,
        order: [
            [1, "desc"]
        ],
        pageLength: 5,
        lengthMenu: [5, 10, 25, 50, 75],
        columns: [{
                mRender: function(data, type, row) {
                    var id = row.id_pembiayaan;
                    return (
                        '<button  class="btn btn-xs btn-warning" onclick="show_detail_transaction(' + id + ')" data-toggle="modal" data-target="#modal-edit" title="Edit"><i class="fa fa-eye"></i></button>'
                    );
                },
                title: "Action",
            },
             {
                data: "id_pembiayaan",
                title: "Id Transaksi"
            },
            {
                data: "nama_produk",
                title: "Nama Produk"
            },
            {
                data: "tujuan_pengiriman",
                title: "Tujuan Pengiriman"
            },
            {
                data: "tanggal_pengiriman",
                title: "Tanggal"
            }
        ],
        processing: true,
    });
    get_dataTable();
};



function get_dataTable() {
    var data;
    var total_response = 0;
    var data2;
    var total_response2 = 0;
    var data3;
    var total_response3 = 0;
    $.ajax({
        url: base_url + "/ayoexport/pembiayaan/get_own_transaction_pembiayaan",
        type: "get",
        beforeSend: function() {
            // Show LOADING icon
            $("#loader").show();
        },
        success: function(response) {
            total_response = response.length;
            data = JSON.parse(response);
        },
        complete: function() {
            // Show LOADING icon
            $("#loader").hide();
        },
    }).done(function() {
        if (total_response !== 0) {
            table_data.clear().draw();
            table_data.rows.add(data);
            table_data.columns.adjust().draw();
        } else {
            table_data.clear().draw();
        }
    });
    ///////
    $.ajax({
        url: base_url + "/ayoexport/pembiayaan/get_own_transaction_pembiayaan_done",
        type: "get",
        success: function(response) {
            total_response2 = response.length;
            data2 = JSON.parse(response);
        }
    }).done(function() {
        if (total_response2 !== 0) {
            table_data2.clear().draw();
            table_data2.rows.add(data2);
            table_data2.columns.adjust().draw();
        } else {
            table_data2.clear().draw();
        }
    });
    ////
    $.ajax({
        url: base_url + "/ayoexport/pembiayaan/get_own_transaction_pembiayaan_reject",
        type: "get",
        success: function(response) {
            total_response3 = response.length;
            data3 = JSON.parse(response);
        }
    }).done(function() {
        if (total_response3 !== 0) {
            table_data3.clear().draw();
            table_data3.rows.add(data3);
            table_data3.columns.adjust().draw();
        } else {
            table_data3.clear().draw();
        }
    });
}


function show_edit_transaction(id) {
    current_id = id;
    $('#edit-transaksi').modal('show');
    $.ajax({
        url: base_url + "/ayoexport/pembiayaan/get_own_detail_pembiayaan",
        type: "POST",
        data: {
            id: id
        },
        beforeSend: function() {
            // Show LOADING icon
            $("#loader").show();
        },
        success: function(response) {
            data = JSON.parse(response);
            for (i of data) {
                $("#e_nama_produk").attr("value", i.nama_produk);
                $("#e_jumlah_produk").attr("value", i.jumlah_produk);
                $("#e_asal_produk").attr("value", i.asal_produk);
                $("#e_tujuan_pengiriman").attr("value", i.tujuan_pengiriman);
                $("#e_estimasi_modal").attr("value", i.estimasi_modal);
            }
        },
        complete: function() {
            // Show LOADING icon
            $("#loader").hide();
        },
    })
}

function update_transaction() {

    var nama_produk = document.getElementById("e_nama_produk").value;
    var jumlah_produk = document.getElementById("e_jumlah_produk").value;
    var asal_produk = document.getElementById("e_asal_produk").value;
    var tujuan_pengiriman = document.getElementById("e_tujuan_pengiriman").value;
    var estimasi_modal = document.getElementById("e_estimasi_modal").value;

    $.ajax({
        url: base_url + "/ayoexport/pembiayaan/update_own_transaction_pembiayaan",
        type: "POST",
        data: {
            id: current_id,
            nama_produk: nama_produk,
            jumlah_produk: jumlah_produk,
            asal_produk: asal_produk,
            tujuan_pengiriman: tujuan_pengiriman,
            estimasi_modal: estimasi_modal
        },
        beforeSend: function() {
            // Show LOADING icon
            $("#loader").show();
        },
        success: function(response) {
            document.getElementById('close-modal-edit').click();
            swal("Selamat", "Transaksi berhasil diperbaharui", "success");
        },
        complete: function() {
            // Show LOADING icon
            $("#loader").hide();
            setInterval(function(){
                window.location.reload();
            },2000);
        },
    })

}

function delete_transaction(id) {
    current_id = id;
    $('#modal-delete-transaksi').modal('show');
}

function confirm_delete_transaction() {

    $.ajax({
        url: base_url + "/ayoexport/pembiayaan/delete_own_transaction_pembiayaan",
        type: "POST",
        data: {
            id: current_id
        },
        beforeSend: function() {
            // Show LOADING icon
            $("#loader").show();
        },
        success: function(response) {
            document.getElementById('close-modal-edit').click();
            swal("Selamat", "Transaksi berhasil dihapus", "success");
        },
        complete: function() {
            // Show LOADING icon
            $("#loader").hide();
            setInterval(function(){
                window.location.reload();
            },2000);
        },
    })
}

get_respond()

function get_respond() {
    $.ajax({
        url: base_url + "/ayoexport/pembiayaan/get_respon_pembiayaan",
        type: "POST",
        success: function(response) {
            data = JSON.parse(response);
            console.log("han" + response);
            if (response == '[]') {
                var html = ['<div class="row">',
                    '<div class="col-md-12">',
                    '<div style="custom-panel">',
                    '<p class="text-center img-restrict">',
                    '<img src="http://shyamtha.com.np/admin/assets/nodatafound.png"   height="350"> ',
                    '</p>',
                  
                    '</div>',
                    '</div>',
                    '</div>'
                ].join("\n");

                $("#notification-content").append(html);
            } else {
                for (i of data) {

                    var id_pembiayaan_respon = i.id_pembiayaan;
                    var id_respon = i.id_respon;
                    var status_current = i.status;
                    var status_respon = i.respon;
                    var date_notif = i.date;
                    var status_accepted = i.accepted;

                    if (status_current == 'done') {
                        var color = 'green';
                        var status_notif = "APPROVED";
                        var message_respon = 'Selamat, transaksi Anda dengan nomor ' + id_pembiayaan_respon + ' sudah selesai.';
                        var link_respon = '#2';
                        var button_detail = '<button class="button__bluesmall" onClick="javascript:show_detail_transaction(' + id_pembiayaan_respon + ')"> Detail </button>';
                    } else if (status_current == 'reject') {
                        var color = 'red';
                        var status_notif = "REJECTED";
                        var message_respon = 'Maaf, Transaksi pembiayaan dengan id ' + id_pembiayaan_respon + ' ditolak.';
                        var link_respon = '#3';
                        var button_detail = '<button class="button__bluesmall" onClick="javascript:show_detail_transaction(' + id_pembiayaan_respon + ')"> Detail </button>';
                    } else if (status_current == 'offer') {
                        var color = 'blue';
                        var status_notif = "OFFER";
                        var message_respon_detail = "'" + status_respon + "'";
                        var link_respon = '#4';

                        if (status_accepted == null) {
                            var message_respon = 'Anda mendapatkan penawaran baru untuk request dengan ID ' + id_pembiayaan_respon;
                            var button_detail = '<button class="button__bluesmall" onClick="javascript:show_detail_offer(' + id_respon + ',' + message_respon_detail + ',' + 0 + ')"> Detail </button>';
                        } else {
                            if (status_accepted == "accept") {
                                var message_respon = 'Mohon menunggu, team kami akan segera memproses permintaan Anda';
                                status_accepted_val = "Sudah disetujui";
                            } else if (status_accepted == "reject") {
                                var message_respon = 'Terima kasih atas konfirmasi Anda.';
                                status_accepted_val = "Sudah ditolak";
                            }
                            var button_detail = '<button class="button__bluesmall" style="background-color: green !important" onClick="javascript:show_detail_offer(' + id_respon + ',' + message_respon_detail + ',' + 1 + ')"> ' + status_accepted_val + '</button>';
                        }
                    }
                    var html = ['<div class="row border-notif">',
                        '<div class="col-md-3 p-0 bg-blue">',
                        '<div class="date-notification" style="background-color: #45619f;padding: 15px;color: #fff;border-bottom-left-radius: 10px;border-top-left-radius: 10px;">',
                        '<span>' + date_notif + '</span>',
                        '</div>',
                        '</div>',
                        '<div class="col-md-2">',
                        '<div class="body-notif" style="font-weight:bold;text-align: center; color:' + color + ';">',
                        '<p class="p-0 text-status-notif">' + status_notif + '</p>',
                        '</div>',
                        '</div>',
                        '<div class="col-md-5">',
                        '<div class="body-notif">',
                        '<a>' + message_respon + '</a>',
                        '</div>',
                        '</div>',
                        '<div class="col-md-2">',
                        '<div class="detail-notif">' + button_detail +
                        '</div>',
                        '</div>',
                        '</div>'
                    ].join("\n");

                    $("#notification-content").append(html);
                }
            }
        },
    })

}

function show_detail_transaction(id) {
    $('#show_detail').modal('show');
    $.ajax({
        url: base_url + "/ayoexport/pembiayaan/get_own_detail_pembiayaan",
        type: "POST",
        data: {
            id: id
        },
        beforeSend: function() {
            // Show LOADING icon
            $("#loader").show();
        },
        success: function(response) {
            data = JSON.parse(response);
            for (i of data) {
                $("#detail_nama_produk").text(i.nama_produk);
                $("#detail_jumlah_produk").text(i.jumlah_produk);
                $("#detail_asal_produk").text(i.asal_produk);
                $("#detail_tujuan_pengiriman").text(i.tujuan_pengiriman);
                $("#detail_estimasi_modal").text(i.estimasi_modal);
                $("#detail_tanggal_pengiriman").text(i.tanggal_pengiriman);
            }
        },
        complete: function() {
            // Show LOADING icon
            $("#loader").hide();
        },
    })
}
var current_id_offer = 0;

function show_detail_offer(id_respon, message_respon, nullable) {
    current_id_offer = id_respon;
    if (nullable == 0) {
        document.getElementById('footer-notif-offer').style.display = '';
    } else if (nullable == 1) {
        document.getElementById('footer-notif-offer').style.display = 'none';
    }
    $('#show_detail_offer').modal('show');
    $("#text-detail-offer").text(message_respon);
}


function confirm_offer(respon) {
    $.ajax({
        url: base_url + "/ayoexport/pembiayaan/confirm_offer_pembiayaan",
        type: "POST",
        data: {
            'id': current_id_offer,
            'respon': respon
        },
        beforeSend: function() {
            // Show LOADING icon
            $("#loader").show();
        },
        success: function(response) {
            data = JSON.parse(response);
            document.getElementById('close-modal-detail-offer').click();
            swal("Selamat", "Transaksi berhasil di " + respon, "success");
        },
        complete: function() {
            // Show LOADING icon
            $("#loader").hide();
            setInterval(function(){
                window.location.reload();
            },2000);
        },
    })
}


$(function() {
    $('.floating-wpp').floatingWhatsApp({
        phone: '+6283832328001',
        popupMessage: 'Please type your text below.',
        showPopup: true,
        message: 'Hello ayoexport, I want to make transaction sales for ....',
        headerTitle: 'Sales Transaction'
    });
});

const msgerForm = get(".msger-inputarea");
const msgerInput = get(".msger-input");
const msgerChat = get(".msger-chat");


// Icons made by Freepik from www.flaticon.com
const BOT_IMG = "https://image.flaticon.com/icons/svg/327/327779.svg";
const PERSON_IMG = "https://image.flaticon.com/icons/svg/145/145867.svg";
const BOT_NAME = "CoronaBot";
const PERSON_NAME = "You";

msgerForm.addEventListener("submit", event => {
    event.preventDefault();

    const msgText = msgerInput.value;
    if (!msgText) return;

    appendMessage(PERSON_NAME, PERSON_IMG, "right", msgText);
    msgerInput.value = "";
    botResponse(msgText);
});

function appendMessage(name, img, side, text) {
    //   Simple solution for small apps
    const msgHTML = `
 <div class="msg ${side}-msg">
   <div class="msg-img" style="background-image: url(${img})"></div>

   <div class="msg-bubble">
     <div class="msg-info">
       <div class="msg-info-name">${name}</div>
       <div class="msg-info-time">${formatDate(new Date())}</div>
     </div>

     <div class="msg-text">${text}</div>
   </div>
 </div>
 `;

    msgerChat.insertAdjacentHTML("beforeend", msgHTML);
    msgerChat.scrollTop += 500;
}

function botResponse(rawText) {

    // Bot Response
    $.get("https://cors-anywhere.herokuapp.com/http://34.126.80.48:7000/get", {
        msg: rawText
    }).done(function(data) {
        console.log(rawText);
        console.log(data);
        const msgText = data;
        appendMessage(BOT_NAME, BOT_IMG, "left", msgText);

    });

}
// Utils
function get(selector, root = document) {
    return root.querySelector(selector);
}

function formatDate(date) {
    const h = "0" + date.getHours();
    const m = "0" + date.getMinutes();

    return `${h.slice(-2)}:${m.slice(-2)}`;
}