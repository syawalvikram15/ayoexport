var chart1 =  Highcharts.chart('chartActiveUser', {
    chart: {
        type: 'line'
    },
    credits:{
        enabled : false
    },
    title: {
        text: ''
    },
    yAxis: {
        title: {
            text: 'Active User'
        }
    },
    legend: false,

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            }
        }
    },
    xAxis: {
        categories: ['']
    },
    series: [
        {
        name: '-',
        data: [0]
    },
        {
        name: '-',
        data: [0]
    },
        {
        name: '-',
        data: [0]
    }
],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }
});
function comp(no,d){
    $("#jml_"+no).text(d.jumlah),
    $("#jml_"+no).attr("title",d.tanggal),
    $("#persen_"+no).text(d.persen),
    (parseInt(d.jumlah)>0)?$("#row_"+no).attr("class","fa fa-arrow-up"):$("#row_"+no).attr("class","fa fa-arrow-down");
    (parseInt(d.jumlah)>0)?$("#color_"+no).attr("class","ijo"):$("#color_"+no).attr("class","merah");
    $("#persen_"+no).text(d.persen+"%")
}
function updateChart(){
    fetch('/api/aktif_1728',{
        method: "GET",
        mode: "same-origin",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json"
        }
    })
    .then(response=>response.json())
    .then( response => {
        comp("1",response.data.d1[0]),
        comp("7",response.data.d7[0]),
        comp("28",response.data.d28[0])
    }),
    fetch('/api/active_user',{
        method: "GET",
        mode: "same-origin",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json"
        }
    })
    .then(response=>response.json())
    .then( response => {
        console.log(response),
        chart1.series[0].update(response.chart1.data[0],false),
        chart1.series[1].update(response.chart1.data[1],false),
        chart1.series[2].update(response.chart1.data[2],false),
        chart1.xAxis[0].setCategories(response.chart1.categories)
    })
};