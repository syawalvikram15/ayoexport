$("#tblAnd").DataTable({
    paging: false,
    searching: false,
    info: false
});
var chart2 =  Highcharts.chart('piePlatform', {
    chart: {
        type: 'variablepie'
    },
    title: {
        text: ''
    },
    credits:{
        enabled : false
    },
    tooltip: {
        headerFormat: ''
    },
    series: [{
        minPointSize: 10,
        innerSize: '30%',
        name: 'Demografi',
        data: [{
            name: 'Android',
            y: 505370
        }, {
            name: 'iOS',
            y: 551500
        }]
    }]
});

function updateChart(){
    fetch('/api/platform',{
        method: "GET",
        mode: "same-origin",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json"
        }
    })
    .then(response=>response.json())
    .then( response => {
        chart2.series[0].update({data: response.pc_platform}, true),
        $("#tblAnd").DataTable({
            destroy: true,
            paging: false,
            searching: false,
            info: false,
            data : response.tbl_platform,
            columns : [
                {data : "name"},
                {data : "jumlah"},
                {data : "persen"}
            ]
        })
    })
}