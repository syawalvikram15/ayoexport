var chart1 = Highcharts.chart('chart1', {
    credits:{
        enabled : false
    },
    title: {
        text: ''
    },
    yAxis: {
        title: {
            text: 'Active User'
        }
    },
    legend: {
        enabled:false
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            }
        }
    },

    series: [{
        name: '',
        data: [0]
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }
});

function updateChart(){
    fetch('/api/retensi',{
        method: "GET",
        mode: "same-origin",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json"
        }
    })
    .then(response=>response.json())
    .then( response => {
        chart1.series[0].update(response.chart1.data[0],false),
        chart1.xAxis[0].setCategories(response.chart1.categories),
        $("#tblKohor").DataTable({
            destroy: true,
            paging: false,
            searching: false,
            info: false,
            data : response.kohor,
            columns : [
                {data : "tanggal"},
                {data : "jumlah"},
                {data : "day_0"},
                {data : "day_1"},
                {data : "day_2"},
                {data : "day_3"},
                {data : "day_4"},
                {data : "day_5"},
                {data : "day_6"}
            ],
            createdRow: function ( row, data, index ) {
                var d = Object.keys(data);
                var i = 2;
                d.forEach(function(v){
                    console.log(v);
                    console.log(data[v]);
                    if(v.substr(0,3)=='day'){
                        $('td', row).eq(i).css("background","rgba(47,94,196,"+data[v]/100+")");
                        if(data[v]<25)
                            $('td', row).eq(i).css("color","black");
                        else
                            $('td', row).eq(i).css("color","white");
                        i++;
                    }
                });
            }
        })
    })
}