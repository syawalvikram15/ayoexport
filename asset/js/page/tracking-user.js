var start;
var end;
var data_table;
var data_table_mac;
$(document).ready(function(){    
    start = moment().subtract(13, 'days');
    end = moment();
    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Last 5 Days': [moment().subtract(4, 'days'), moment()],
            'Last Week': [moment().subtract(6, 'days'), moment()],
            'Last 2 Weeks': [moment().subtract(13, 'days'), moment()],        
        }
    }, cb);
    cb(start, end);
    
    

    $(document).ajaxStart(function(){
        $("#loading").html("<img src='"+BASE_URL+"asset/plugins/images/busy.gif' width='12' /> Loading...");
    });
    $(document).ajaxComplete(function(){        
        $("#loading").text("Submit");        
    });

    data_table = $('#data-table').DataTable({
		responsive: true,		
		"paging":   true,
		"info":     false,
		"order": [[ 0, "desc" ]],
        'pageLength' : 5,
        'lengthMenu' : [5,10,25,50,75],
		searching: true,
		columns:[
			{data: 'tanggal',           title: 'Tanggal'},
			{data: 'nama_or_device',    title: 'Name'},
			{data: 'no_hp_contacted',   title: 'No HP Contacted'},
			{data: 'status_contacted',  title: 'Status Contacted'},
			{data: 'jml_kontak',        title: 'Jumlah Kontak'},
			
		],
		processing: true,			
    } );

    data_table_mac = $('#data-table-mac').DataTable({
		responsive: true,		
		"paging":   true,
		"info":     false,
		"order": [[ 0, "desc" ]],
        'pageLength' : 5,
        'lengthMenu' : [5,10,25,50,75],
		searching: true,
		columns:[
			{data: 'tanggal',           title: 'Tanggal'},
			{data: 'nama_or_device',    title: 'MAC Address'},
			{data: 'no_hp_contacted',   title: 'No HP Contacted'},
			{data: 'status_contacted',  title: 'Status Contacted'},
			{data: 'jml_kontak',        title: 'Jumlah Kontak'},
			
		],
		processing: true,			
    } );
        
    initialSubmit()
});

function cb(sd, ed) {   
    start = sd;
    end = ed 
    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
}


$("#submit-search").click(function(e){
    e.preventDefault();
    initialSubmit()
});

function initialSubmit() {
    startDate = start.format('YYYYMMDD')
    endDate = end.format('YYYYMMDD')
    
    var disabled =$('#submit-search').hasClass('disabled');
    if(!disabled){
        nohp = $("#search-nohp").val();        
        if(nohp.substring(0, 1) === "8"){ nohp = nohp.replace("8", "+628")}
        if(nohp.substring(0, 2) === "08"){ nohp = nohp.replace("08", "+628")}
        if(nohp.substring(0, 3) === "628"){ nohp = nohp.replace("628", "+628")}        
        handleProfile(startDate, endDate, nohp);
        handletable(startDate, endDate, nohp);
        handletableMac(startDate, endDate, nohp);
        kelurahan(startDate, endDate, nohp);
        map(startDate, endDate, nohp);
		forceGraph(startDate, endDate, nohp);
    }
}

const forceGraph = (start, end, mobile_number) => {
	$.post('/api2/getLinkNode',{tgl_start:start,tgl_end:end,no_hp:mobile_number})
    .done( 
      data => 
    {
     const elem = document.getElementById('graph');

     const Graph = ForceGraph()(elem)
       .graphData(data)
       .nodeLabel( //'first_contacted'
		node => 
		{ 
			var str ='';
			str = "<table class='table table-sm'><tbody style='color:#fff'> \
				<tr><td>Nama</td><td>"+node.nama+"</td></tr> \
				<tr><td>No Hp</td><td>"+node.no_hp+"</td></tr>";
			if(node.jml_kontak!=null){
				str += "<tr><td>Jumlah Kontak</td><td>"+node.jml_kontak+"</td></tr> \
				<tr><td>Kontak Pertama</td><td>"+node.first_contacted+"</td></tr> \
				<tr><td>Kontak Terakhir</td><td>"+node.last_contacted+"</td></tr> \
				<tr><td>Lama Kontak</td><td>"+node.lama_kontak+"</td></tr></tbody>";
			}
			str +="</table>";
			return str;
		}
	   )
       .width(390)
       .height(386)
       .nodeAutoColorBy('group')
       .linkDirectionalParticles(2)
       .nodeColor(node => {
         switch(node.status){
           case 'NOT POSITIVE': return "#4F81BC"; 
           case 'POSITIVE': return "#FE0000"; 
           case null : return '#92D14F'; 
           case 'PDP' : return '#FFC000'; 
           case 'ODP' : return '#FFFF00'; 
         }
       })
       .linkDirectionalParticleWidth(1.4)
       /*.onNodeHover(node => {
          if(node!=null){
            $('#nama').html(node.nama);
            $('#hp').html(node.no_hp);
            $('#status').html(node.status);
            $('#mac').html(node.mac_address);
            $('#tanggal_daftar').html(node.tgl_terdaftar);
            $('#jml_kontak').html(node.jml_kontak);
            $('#first_contacted').html(node.first_contacted);
            $('#last_contacted').html(node.last_contacted);
            $('#lama_kontak').html(node.lama_kontak);
          }
       })*/
       .onNodeClick(node => {
         Graph.centerAt(node.x, node.y, 500);
         Graph.zoom(8, 2000);
       });
    });
}

const handleProfile = (start, end, mobile_number) => {
    $.ajax({        
        url  : BASE_URL+"api/tracking-user/get-profile",
        type : 'post',
        data: {start:start, end: end, mobile_number : mobile_number},
        success: function (response, textStatus, xhr) {
            data = JSON.parse(response);            
            if(data){
                $("#nohp").text(data.mobile_number)
                $("#nama").text(data.full_name)
                $("#status").text(data.status)
                // $("#lokasi").text(data.lokasi)
                $("#tanggal-terdaftar").text(data.created_at_local)
            }else{
                $("#nohp").html("<span style='color:red;'>No Data</span>")
                $("#nama").html("<span style='color:red;'>No Data</span>")
                $("#status").html("<span style='color:red;'>No Data</span>")
                // $("#lokasi").text("No Data")
                $("#tanggal-terdaftar").html("<span style='color:red;'>No Data</span>")
            }

            //lokasi by api
            var zonasi = handleZonasiAPI(data.latitude, data.longitude);
            zonasi.done(function(res) {
                if (!res.status) {                    
                    $("#lokasi").text(res[0].kabupaten+" - "+res[0].kelurahan)
                } else {
                    $("#lokasi").html("<span style='color:red;'>No Data</span>")
                }
            })
            .fail(function(x) {
                // Tell the user something bad happened
            });
        }
    });
}

const handleZonasiAPI = (lat, lng) => {    
    return  $.post( "http://covid19-api-dev.vsan-apps.playcourt.id/zonasi2.php", 
            { 
                lat:lat, 
                lng:lng, 
                key: 'd29bd7e4-f5fc'
            }).done(function( json ) {
                data = json		
            });    
}

function handleZonasiAPI2(longlat) {
    // var longlat = {0 : '106.8836020 -6.24312580000', 1 : '106.8818210 -6.24110910000'}    
    return  $.post( "http://covid19-api-prod.vsan-apps.playcourt.id/kota.php",
    {
        longlat: longlat,
    }).done(function( json ) {
        json
    });
}

function kelurahan(start, end, mobile_number){
    // var arrKategory = []
    var totalJumlah = []
    var longlat = {}
    var jumlah = []
    $.ajax({
        url  : BASE_URL+"api/tracking-user/get-kelurahan",
        type : 'post',
        data: {start:start, end: end, mobile_number : mobile_number},
        success: function (response, textStatus, xhr) {            
            data = JSON.parse(response);            
            data.map((val, idx) => {
                longlat[idx] = val.longitude+" "+val.latitude;
                totalJumlah.push(parseInt(val.jumlah));
            });
            var zonasi = handleZonasiAPI2(longlat)
            var color;
            zonasi.done(function(resKat) {
                if (!resKat.status) {
                    resKat.zona.map((v, i) => {                        
                        if(v === "1"){
                            color="green";
                        } 
                        if(v === "2"){
                            color="yellow";
                        }
                        if(v === "3"){
                            color="red";
                        }
                        if(v === ""){
                            color="green";
                        } 
                        jumlah.push({y: totalJumlah[i], color:color});
                    })                    
                   _init_chart('kelurahan', resKat.label, jumlah)
                } else {                   
                }
            })
            .fail(function(x) {
                // Tell the user something bad happened
            });
        }
    });
}

const handletable = (start, end, mobile_number) => {    
    var data;
	var total_response = 0;
    $.ajax({        
        url  : BASE_URL+"api/tracking-user/get-data-table",
        type : 'post',
        data: {start:start, end: end, mobile_number : mobile_number},
        success: function (response, textStatus, xhr) {
            total_response = response.length;
			data = JSON.parse(response);            
        }
    }).done(function(){
        if(total_response !== 0)
        {
            data_table.clear().draw();
            data_table.rows.add(data);
            data_table.columns.adjust().draw();            
        }else{
            data_table.clear().draw();
        }
    });
}

const handletableMac = (start, end, mobile_number) => {    
    var data;
	var total_response = 0;
    $.ajax({        
        url  : BASE_URL+"api/tracking-user/get-data-table-mac",
        type : 'post',
        data: {start:start, end: end, mobile_number : mobile_number},
        success: function (response, textStatus, xhr) {
            total_response = response.length;
			data = JSON.parse(response);            
        }
    }).done(function(){
        if(total_response !== 0)
        {
            data_table_mac.clear().draw();
            data_table_mac.rows.add(data);
            data_table_mac.columns.adjust().draw();            
        }else{
            data_table_mac.clear().draw();
        }
    });
}


function _init_chart(container, arrKategory, totalJumlah){
    Highcharts.setOptions({
        chart: {
            style: {
                fontFamily: '"Roboto",sans-serif'
            }
        },
        lang: {
            thousandsSep: ',',
			noData: "No Data to show!"
        },
		credits: {
			enabled: false
		},
    });
    Highcharts.chart('container-'+container, {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: arrKategory,
            crosshair: true
        },        
        yAxis: {
            min: 0,
            title: {
                text: 'Number'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,  
            useHTML: true
        },
        // plotOptions: {
        //     column: {
        //         pointPadding: 0.2,
        //         borderWidth: 0
        //     }
        // },
        plotOptions: {
            column: {
                showInLegend: true,
                dataLabels: {
                    enabled: true
                },
			},
        },    
        series: [{
            name: container, 
            // data: [
            //     {y: 49.9, color: 'red'}, 
            //     {y: 71.5, color: 'yellow'}, 
            //     {y: 106.4, color: 'green'},
            // ]
            data: totalJumlah    
        }]       
    });
}





function map(start, end, mobile_number) {
    longlat = {}
    dataModif = [];
    $.ajax({        
        url  : BASE_URL+"api/tracking-user/get-latlong",
        type : 'post',
        data: {start: start, end: end, mobile_number : mobile_number},
        success: function (response, textStatus, xhr) {            
            data = JSON.parse(response);            
            // initMap(data)            
            data.map((val, idx) => {
                longlat[idx] = val.longitude+" "+val.latitude;
            });            
            var zonasi = handleZonasiAPI2(longlat)
            var color;
            zonasi.done(function(res) {                
                if (!res.status) {                    
                    res.zona.map((v, i) => {                        
                            
                        if(v === "1"){
                            color="green";
                        } 
                        if(v === "2"){
                            color="yellow";
                        }
                        if(v === "3"){
                            color="red";
                        }
                        if(v === ""){
                            color="green";
                        }                        
                        dataModif.push({
                            // latitude: data[i].latitude,
                            // longitude: data[i].longitude,
                            latitude: res.latitude[i],
                            longitude: res.longitude[i],
                            desa_kelurahan: res.label[i],
                            zone_class: color,
                        });
                    })                    
                    initMap(dataModif)                    
                } else {                   
                }
            })
            .fail(function(x) {
                // Tell the user something bad happened
            });
        }
    });
}

function initMap(data){
    document.getElementById('weathermap').innerHTML = "<div id='map' class='map'></div>";
    locations = [];
    wp = [];
    data.map((val, idx) => {
        locations.push([val.desa_kelurahan, val.zone_class, val.latitude, val.longitude]);
        wp.push([val.latitude, val.longitude])
    });    
        
    var map = L.map('map').setView([-2.6000285, 118.015776], 4);
    mapLink =
    '<a href="http://openstreetmap.org">OpenStreetMap</a>';
    L.tileLayer(
    'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; ' + mapLink + ' Contributors',
        maxZoom: 18,
    }).addTo(map);
          
    for (var i = 0; i < locations.length; i++) {
        var Icon = new L.Icon({
            iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-'+locations[i][1]+'.png',
            shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            shadowSize: [41, 41]
        });

        marker = new L.marker([locations[i][2], locations[i][3]], {icon: Icon})
            .bindPopup(`(Rute ${i+1}) ${locations[i][0]} (${locations[i][1]} zone)`)
            .addTo(map);        
    }  

    var Control = L.Routing.control({
        waypoints: wp ,
        createMarker: function(i, waypoints, n){ 
            return marker
        },
        show: false
    }).addTo(map);
}