$("#tblScreen").DataTable({
    paging: false,
    searching: false,
    info: false
});
var d = JSON.parse('[{"name":"Custom Vars","data":[[1575910800,1]]},{"name":"discover","data":[[1573750800,2],[1574182800,1]]},{"name":"Main screen","data":[[1575910800,10]]},{"name":"main-activity","data":[[1574787600,11],[1574874000,30],[1574960400,27],[1575046800,22],[1575133200,2],[1575219600,163],[1575306000,54],[1575392400,183],[1575478800,309],[1575565200,752],[1575651600,620],[1575738000,642],[1575824400,1034],[1575910800,1834]]},{"name":"main-screen","data":[[1574787600,1]]},{"name":"Matamu","data":[[1575910800,1]]},{"name":"TEst Android","data":[[1575910800,11]]}]');
var chart1 =  Highcharts.chart('chartScreen_c', {
    chart: {
        type: 'line'
    },
    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2010
        }
    },
    credits:{
        enabled : false
    },
    xAxis: {
        type: 'datetime'
    },
    tooltip: {
        xDateFormat: '%Y-%m-%d',
        shared: true
    },
    series: d
});

function updateChart(){
    fetch('/api/screen_a',{
        method: "GET",
        mode: "same-origin",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json"
        }
    })
    .then(response=>response.json())
    .then( response => {
        //console.log(response);
        $("#tblScreen").DataTable({
            destroy: true,
            data : response.tbl,
            columns : [
                {data : "tgl"},
                {data : "name"},
                {data : "lama"}
            ]
        })
        //chart1.series[0].setData(response.data)
        //chart1.xAxis[0].setCategories(response.chart1.categories)
    })
}
