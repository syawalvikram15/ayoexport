$("#tblPerangkat").DataTable({
    paging: false,
    searching: false,
    info: false
});
var chart2 =  Highcharts.chart('chartPieUser', {
    chart: {
        type: 'variablepie'
    },
    title: {
        text: ''
    },
    credits:{
        enabled : false
    },
    tooltip: {
        headerFormat: ''
        //pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
        //    'Area (square km): <b>{point.y}</b><br/>' +
        //    'Population density (people per square km): <b>{point.z}</b><br/>'
    },
    series: [{
        minPointSize: 10,
        innerSize: '30%',
        name: 'Demografi',
        data: [{
            name: 'Laki-Laki',
            y: 505370
        }, {
            name: 'Perempuan',
            y: 551500
        }]
    }]
});
Highcharts.chart('chartBarUsia', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: ['18-24', '25-30','31-34','35-40'],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: '',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' millions'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Laki-Laki',
        data: [107, 31,34,50]
    }, {
        name: 'Perempuan',
        data: [133, 156,160,100]
    }]
});
var map = Highcharts.mapChart('map', {
    legend: {
        enabled:false
    },

    title: {
        text: ''
    },
    credits: {
        enabled: false
    },
    series: [{
        data: [],
        mapData: Highcharts.maps['custom/world'],
        joinBy: ['iso-a2', 'code']
    }]
});
map.series[0].addPoint({
    code: 'ID',
    y: 10
});
function updateChart(){
    fetch('/api/demografi',{
        method: "GET",
        mode: "same-origin",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json"
        }
    })
    .then(response=>response.json())
    .then( response => {
        $("#tblLokasi").DataTable({
            order: [[ 2, "desc" ]],
            destroy: true,
            data : response.tblDemografi,
            columns : [
                {data : "negara"},
                {data : "jumlah"},
                {data : "persen"}
            ]
        }),
        $("#tblPerangkat").DataTable({
            destroy: true,
            data : response.tblModel,
            columns : [
                {data : "kota"},
                {data : "model"},
                {data : "persen"}
            ]
        })
        
    })
}