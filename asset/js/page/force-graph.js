var table2 = $('#user').DataTable( {
	//dom: 'Bfrtip',
	responsive: true,
	"processing": true,
  "serverSide": true,
	"ajax": {
        url: '/api/tblUser',
        type: 'POST'
    },
    columns: [
      {data : 0},
      {data : 1,render: (data) => "<button class='btn btn-secondary' onClick='updateChart($(\"#tgl\").val(),\""+data+"\")'>"+data+'</button>'},
      {data : 2},
      {data : 3},
      {data : 4,render:(data)=>{
        let color='';
        switch(data){
          case 'NOT POSITIVE': color="#4F81BC"; break;
          case 'POSITIVE': color="#FE0000"; break;
          case null : color='#92D14F'; break;
          case 'PDP' : color='#FFC000'; break;
          case 'ODP' : color='#FFFF00'; break;
        }
        return "<i class='fa fa-certificate' style='color:"+color+"'></i> " + data;
      }},
      {data : 5}
    ]
});
function updateChart(tgl,user){
  $.post('/api/getLinkNode',{tanggal:tgl,user_id:user})
    .done( 
      data => 
    {
     const elem = document.getElementById('graph');

     const Graph = ForceGraph()(elem)
       .graphData(JSON.parse(data))
       .nodeLabel( //'first_contacted'
		node => 
		{ 
			if(node.jml_kontak!=null){
				return "<table class='table table-sm'><tbody style='color:#fff'> \
				<tr><td>Jumlah Kontak</td><td>"+node.jml_kontak+"</td></tr> \
				<tr><td>Kontak Pertama</td><td>"+node.first_contacted+"</td></tr> \
				<tr><td>Kontak Terakhir</td><td>"+node.last_contacted+"</td></tr> \
				<tr><td>Lama Kontak</td><td>"+node.lama_kontak+"</td></tr></tbody> \
				</table>";
			}
		}
	   )
       .width(673)
       .height(350)
       .nodeAutoColorBy('group')
       .linkDirectionalParticles(2)
       .nodeColor(node => {
         switch(node.status){
           case 'NOT POSITIVE': return "#4F81BC"; 
           case 'POSITIVE': return "#FE0000"; 
           case null : return '#92D14F'; 
           case 'PDP' : return '#FFC000'; 
           case 'ODP' : return '#FFFF00'; 
         }
       })
       .linkDirectionalParticleWidth(1.4)
       .onNodeHover(node => {
          if(node!=null){
            $('#nama').html(node.nama);
            $('#hp').html(node.no_hp);
            $('#status').html(node.status);
            $('#mac').html(node.mac_address);
            $('#tanggal_daftar').html(node.tgl_terdaftar);
            $('#jml_kontak').html(node.jml_kontak);
            $('#first_contacted').html(node.first_contacted);
            $('#last_contacted').html(node.last_contacted);
            $('#lama_kontak').html(node.lama_kontak);
          }
       })
       .onNodeClick(node => {
         // Center/zoom on node
         Graph.centerAt(node.x, node.y, 500);
         Graph.zoom(8, 2000);
       });
    });
    $("html, body").animate({
      scrollTop: 0
  }, 1000); 
}

