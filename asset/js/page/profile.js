var base_url = window.location.origin;
get_data_profile()

function get_data_profile(){
$.ajax({
    type: "POST",
    url: base_url + "/ayoexport/profile/get_profile",
    async: true, //execute script synchronously,
    beforeSend: function() {
        // Show LOADING icon
        $("#loader").show();
    },
    success: function(response) {
        data = JSON.parse(response);
        for (i of data) {
            $("#nama").text(i.nama);
            $("#input_nama").attr("value",i.nama );
            $("#nama2").text(i.nama);
            $("#email").text(i.email);
            $("#input_email").attr("value",i.email);
            $("#hp").text(i.hp);
            $("#input_hp").attr("value",i.hp );
            $("#website").text(i.website);
            $("#input_website").attr("value",i.website );
            $("#alamat1").text(i.alamat + ' ' +i.city + ' ' + i.province);
            $("#input_alamat").attr("value",i.alamat );
            $("#input_city").attr("value",i.city );
            $("#input_province").attr("value",i.province );
            $("#alamat2").text(i.alamat + ' ' +i.city + ' ' + i.province);
            $("#jenis_usaha").text(i.jen_usaha);
            document.getElementById('input_jenis_usaha').value=i.jen_usaha;
            $("#jenis_produk").text(i.jen_produk);
            $("#input_jenis_produk").attr("value",i.jen_produk );
            $("#daftar_produk").text(i.daftar_produk);
            $("#input_daftar_produk").attr("value",i.daftar_produk );
            $("#kapasitas_produk").text(i.kapasitas_produk + i.satuan_produk);
            $("#input_kapasitas_produk").attr("value",i.kapasitas_produk );
            $("#input_satuan_produk").attr("value",i.satuan_produk );
            if (i.gudang == 0){
                var gudang = "Tidak";
            }   else {
                var gudang = "Ya";
            }
            if (i.pabrik == 0){
                var pabrik = "Tidak";
            }   else {
                var pabrik = "Ya";
            }
            if (i.ekspor == 0){
                var ekspor = "Tidak";
            }   else {
                var ekspor = "Ya";
            }
            if (i.gudang == 0){
                var gudang = "Tidak";
            }   else {
                var gudang = "Ya";
            }
            if (i.oem == 0){
                var oem = "Tidak";
            }   else {
                var oem = "Ya";
            }
            if (i.logistik == 0){
                var logistik = "Tidak";
            }   else {
                var logistik = "Ya";
            }
            if (i.verified == 0){
                $("#alert_profile").show();
                document.getElementById("alert_profile").classList.add('alert-danger');
                $("#notif_biodata").text('Akun anda belum terverifikasi. Silahkan lengkapi profil anda.');
                $("#button-edit-profile").show();
            } else {
                $("#alert_profile").show()
                document.getElementById("alert_profile").classList.add('alert-success');
                $("#notif_biodata").text('Selamat. Akun anda sudah terverfifikasi.'); 
                $("#button-edit-profile").hide();
                
            }
            $("#gudang").text(gudang);
            document.getElementById('input_gudang').value=i.gudang;
            $("#pabrik").text(pabrik);
            document.getElementById('input_pabrik').value=i.pabrik;
            $("#lama_produk").text(i.lama_produk + " Minggu");
            $("#input_lama_produk").attr("value",i.lama_produk );
            $("#kendala").text(i.kendala);
            $("#input_kendala").attr("value",i.kendala );
            $("#tahun_berdiri").text(i.tahun_berdiri);
            $("#input_tahun_berdiri").attr("value",i.tahun_berdiri );
            $("#ekspor").text(ekspor);
            document.getElementById('input_ekspor').value=i.ekspor;
            $("#negara_tujuan").text(i.negara_tujuan);
            $("#input_negara_tujuan").attr("value",i.negara_tujuan );
            $("#jum_karyawan").text(i.jml_karyawan);
            $("#input_jumlah_karyawan").attr("value",i.jml_karyawan );
            $("#sertifikasi").text(i.sertifikasi);
            $("#input_sertifikasi").attr("value",i.sertifikasi );
            $("#oem").text(oem);
            document.getElementById('input_oem').value=i.oem;
            $("#term_pembayaran").text(i.term_pembayaran);
            $("#input_term_pembayaran").attr("value",i.term_pembayaran );
            $("#term_harga").text(i.term_harga);
            $("#input_term_harga").attr("value",i.term_harga );
            $("#logistik").text(logistik);
            document.getElementById('input_logistik').value=i.logistik;
        }
    },
    complete: function() {
        // Show LOADING icon
        $("#loader").hide();
    },
  });
}




// $('#form-update').submit(function() { 
function update_profile(){

var input_nama = document.getElementById("input_nama").value;
var input_hp = document.getElementById("input_hp").value;
var input_email = document.getElementById("input_email").value;
var input_website = document.getElementById("input_website").value;
var input_alamat = document.getElementById("input_alamat").value;
var input_city = document.getElementById("input_city").value;
var input_province = document.getElementById("input_province").value;
var input_jenis_usaha = document.getElementById("input_jenis_usaha").value;
var input_jenis_produk = document.getElementById("input_jenis_produk").value;
var input_daftar_produk = document.getElementById("input_daftar_produk").value;
var input_kapasitas_produk = document.getElementById("input_kapasitas_produk").value;
var input_satuan = document.getElementById("input_satuan_produk").value;
var input_gudang= document.getElementById("input_gudang").value;
var input_pabrik = document.getElementById("input_pabrik").value;
var input_ekspor = document.getElementById("input_ekspor").value;
var input_lama_produk = document.getElementById("input_lama_produk").value;
var input_kendala = document.getElementById("input_kendala").value;
var input_tahun_berdiri = document.getElementById("input_tahun_berdiri").value;
var input_negara_tujuan = document.getElementById("input_negara_tujuan").value;
var input_jumlah_karyawan = document.getElementById("input_jumlah_karyawan").value;
var input_sertifikasi = document.getElementById("input_sertifikasi").value
var input_oem = document.getElementById("input_oem").value;
var input_term_pembayaran = document.getElementById("input_term_pembayaran").value;
var input_term_harga = document.getElementById("input_term_harga").value;
var input_logistik = document.getElementById("input_logistik").value;

if(![input_nama,input_email,input_hp,input_website,input_alamat,input_city,input_province,input_jenis_usaha,input_jenis_produk,input_daftar_produk,input_kapasitas_produk,input_satuan,input_gudang,input_pabrik,input_ekspor,input_lama_produk,input_kendala,input_tahun_berdiri,input_jumlah_karyawan,input_sertifikasi,input_oem,input_term_pembayaran,input_term_harga,input_logistik].every(Boolean))
{
    swal("Oops!", "Pastikan seluruh input terisi!", "warning");
} else {
$.ajax({
    type: "POST",
    url: base_url + "/ayoexport/profile/edit_profile",
    async: true, //execute script synchronously,
    beforeSend: function() {
        // Show LOADING icon
        $("#loader").show();
    },
    data: { 
        input_nama : input_nama,
        input_email : input_email,
        input_hp : input_hp,
        input_website : input_website,
        input_alamat : input_alamat,
        input_city : input_city,
        input_province : input_province,
        input_jenis_usaha : input_jenis_usaha,
        input_jenis_produk : input_jenis_produk,
        input_daftar_produk : input_daftar_produk,
        input_kapasitas_produk : input_kapasitas_produk,
        input_satuan : input_satuan,
        input_gudang: input_gudang,
        input_pabrik : input_pabrik,
        input_ekspor : input_ekspor,
        input_lama_produk : input_lama_produk,
        input_kendala : input_kendala,
        input_tahun_berdiri : input_tahun_berdiri,
        input_negara_tujuan : input_negara_tujuan,
        input_jumlah_karyawan : input_jumlah_karyawan,
        input_sertifikasi : input_sertifikasi,
        input_oem : input_oem,
        input_term_pembayaran : input_term_pembayaran,
        input_term_harga : input_term_harga,
        input_logistik : input_logistik
    },
    success: function(response) {
        swal("Selamat", "Profil anda berhasil diperbaharui.", "success");
        document.getElementById('close-modal-edit').click();
        get_data_profile()
    },
    complete: function() {
        // Show LOADING icon
        $("#loader").show();
    },
  });
  
  document.getElementById("alert_profile").classList.replace('alert-danger','alert-success');
}
};
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
