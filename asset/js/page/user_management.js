var table_data;
var base_url = window.location.origin;
$(document).ready(function () {
	table_data = $("#data-table").DataTable({
		responsive: true,
		paging: true,
		info: true,
		searching: true,
		order: [[1, "desc"]],
		pageLength: 5,
		lengthMenu: [5, 10, 25, 50, 75],
		// scrollX: true,
		// scrollCollapse: true,
		columns: [
			{
				mRender: function (data, type, row) {
					var kode = row.email;
					return (
						'<button  class="btn btn-xs btn-danger m-b-10" onclick="delete_dataUser(\'' +
						kode +
						'\');" title="Delete"><i class="fa fa-user-times"></i></button> &nbsp;' +
						'<button  class="btn btn-xs btn-warning" onclick="edit_dataUser(\'' +
						kode +
						'\');" data-toggle="modal" data-target="#modal-edit" title="Edit"><i class="fa fa-eye"></i></button>'
					);
				},
				title: "Action",
			},
			{ data: "nama", title: "Nama" },
			{ data: "email", title: "Email" },
			{ data: "level", title: "Level" },
			{ data: "unit", title: "Unit" },
			{
				data: "active",
				title: "Active",
				mRender: function (data, type, row) {
					if (row.active === "0") {
						return "No";
					}
					if (row.active === "1") {
						return "Yes";
					}
				},
			},
			// { data: "active", title: "Active" },
			{ data: "alamat", title: "Alamat" },
			{ data: "hp", title: "HP" },
		],
		processing: true,
	});
	get_dataTable();

	//----------------------------
	$("#add-user").click(function () {
		init_formValidator("quickForm");
	});
});

// function init_formValidator(idForm) {
// 	// alert(idForm)
// 	$.validator.setDefaults({
// 		submitHandler: function () {
// 			//   alert( "Form successful submitted!" );
// 			if (idForm === "quickForm") {
// 				store_datauser();
// 			}
// 			if (idForm === "quickFormEdit") {
// 				update_datauser();
// 			}
// 		},
// 	});
// 	jQuery.validator.addMethod(
// 		"passwordAddMethod",
// 		function (value, element) {
// 			var result =
// 				this.optional(element) ||
// 				(value.length >= 8 && /\d/.test(value) && /[a-z]/i.test(value));
// 			return result;
// 		},
// 		"Your password must be at least 8 characters long and contain at least one number and one character."
// 	);
// 	var validator = $("#" + idForm).validate({
// 		rules: {
// 			username: {
// 				required: true,
// 				remote: {
// 					url: base_url + "/ayoexport/user_management/check_username",
// 					type: "post",
// 				},
// 			},
// 			name: {
// 				required: true,
// 			},
// 			email: {
// 				required: true,
// 				email: true,
// 			},
// 			level: {
// 				required: true,
// 			},
// 			active: {
// 				required: true,
// 			},
// 			terms: {
// 				required: true,
// 			},
// 		},
// 		messages: {
// 			username: {
// 				required: "Please enter a username",
// 				remote: "Username already in use!",
// 			},
// 			name: {
// 				required: "Please enter a name",
// 			},
// 			email: {
// 				required: "Please enter a email address",
// 				email: "Please enter a vaild email address",
// 			},
// 			password2: {
// 				required: "Please retype the password",
// 			},
// 			level: {
// 				required: "Please provide a level",
// 			},
// 			active: {
// 				required: "Please provide a active status",
// 			},

// 			terms: "Please accept our terms",
// 		},
// 		highlight: function (element) {
// 			$(element).closest(".form-group").addClass("has-error");
// 		},
// 		unhighlight: function (element) {
// 			$(element).closest(".form-group").removeClass("has-error");
// 		},
// 		errorElement: "span",
// 		errorClass: "help-block",
// 		errorPlacement: function (error, element) {
// 			if (element.parent(".input-group").length) {
// 				error.insertAfter(element.parent());
// 			} else {
// 				error.insertAfter(element);
// 			}
// 		},
// 	});

// 	//---------------------------------
// 	$(".reset").click(function () {
// 		validator.resetForm();
// 		$("#quickForm")[0].reset();

// 		$("#bumn-form").removeClass("col-md-6");
// 		$("#bumn-form").html("");
// 	});
// }


// function view_password_edit() {
// 	var type = $("#password-edit").attr("type");
// 	// alert(type)
// 	if (type === "password") {
// 		$("#view-password-edit").removeClass("fa-eye");
// 		$("#view-password-edit").addClass("fa-eye-slash");
// 		$("#password-edit").attr("type", "text");
// 	} else {
// 		$("#view-password-edit").removeClass("fa-eye-slash");
// 		$("#view-password-edit").addClass("fa-eye");
// 		$("#password-edit").attr("type", "password");
// 	}
// }


function edit_dataUser(username) {
	// init_formValidator("quickFormEdit");
	// alert(username);
	$.ajax({
		url: base_url + "/ayoexport/user_management/check_data_username",
		type: "post",
		data: { username: username },
		beforeSend: function() {
			// Show LOADING icon
			$("#loader").show();
		},
		success: function (response) {
			data = JSON.parse(response);
			$("#email-edit").text(data.email);
			
			$("#level-edit").text(data.level);
			//  $("#active-edit").text(data.active);
			 $("#nama-edit").text(data.nama);
			 $("#alamat-edit").text(data.alamat);

			 $("#hp-edit").text(data.hp);
			 $("#city-edit").text(data.city);
			 $("#province-edit").text(data.province);
			 $("#website-edit").text(data.website);
			 $("#jen-usaha-edit").text(data.jen_usaha);
			 $("#jen-produk-edit").text(data.jen_produk);
			 $("#daftar-produk-edit").text(data.daftar_produk);
			 $("#kapasitas-produk-edit").text(data.kapasitas_produk);
			 $("#satuan-produk-edit").text(data.satuan_produk);
			 $("#gudang-edit").text(data.gudang);
			 $("#pabrik-edit").text(data.pabrik);
			 $("#lama-produk-edit").text(data.lama_produk);
			 $("#kendala-edit").text(data.kendala);
			 $("#tahun-berdiri-edit").text(data.tahun_berdiri);
			 $("#ekspor-edit").text(data.ekspor);
			 $("#negara-tujuan-edit").text(data.negara_tujuan);
			 $("#jumlah-karyawan-edit").text(data.jml_karyawan);
			 $("#sertifikasi-edit").text(data.sertifikasi);
			 $("#oem-edit").text(data.oem);
			 $("#term-pembayaran-edit").text(data.term_pembayaran);
			 $("#term-harga-edit").text(data.term_harga);
			 $("#logistik-edit").text(data.logistik);

		},
		complete: function() {
			// Show LOADING icon
			$("#loader").hide();
		},
	});
}
function update_datauser() {
	var username = $("#username-edit").val();
	// var password = $("#password-edit").val();
	var level = $("#level-edit").val();
	var unit = level.toUpperCase();
	// if (level === "bumn") {
	// 	unit = $("#bumn-edit").val();
	// }
	var active = $("#active-edit").val();
	var name = $("#name-edit").val();
	var address = $("#address-edit").val();
	var nohp = $("#nohp-edit").val();

	swal({
		title: "Yakin Meng-Update User dengan username " + username + " ?",
		text: "Jika di-update, Pastikan untuk mengingat PASSWORD!",
		icon: "warning",
		buttons: true,
		dangerMode: true,
	}).then((willDelete) => {
		if (willDelete) {
			$.ajax({
				url: base_url + "/ayoexport/user_management/update_data_user",
				type: "post",
				data: {
					username: username,
					// password: password,
					level: level,
					unit: unit,
					active: active,
					name: name,
					address: address,
					unit: unit,
					nohp: nohp,
				},
				success: function (response) {
					var data = JSON.parse(response);
					if (data) {
						swal("Success! User telah di-update!", {
							icon: "success",
						});
					} else {
						swal({
							title: "Error",
							text: "User gagal di-update!",
							icon: "error",
							button: "Back",
						});
					}
				},
			}).done(function () {
				get_dataTable();
				$("#modal-edit").modal("hide");
				$("#quickFormEdit")[0].reset();
			});
		} else {
			swal("User dengan username " + username + " tidak di-update!");
		}
	});
}

function store_datauser() {
	var username = $("#username").val();
	// var password = $("#password").val();
	var level = $("#level").val();
	var unit = level.toUpperCase();
	if (level === "bumn") {
		unit = $("#bumn").val();
	}
	var active = $("#active").val();
	var name = $("#name").val();
	var address = $("#address").val();
	var nohp = $("#nohp").val();
	$.ajax({
		url: base_url + "/ayoexport/user_management/store_data_user",
		type: "post",
		data: {
			username: username,
			// password: password,
			level: level,
			unit: unit,
			active: active,
			name: name,
			address: address,
			nohp: nohp,
		},
		success: function (response) {
			data = JSON.parse(response);
			if (data) {
				swal("Selamat!", "User Baru berhasil ditambahkan...", "success");
				get_dataTable();
				$("#modal-secondary").modal("hide");
				$("#quickForm")[0].reset();
				$("#bumn-form").removeClass("col-md-12");
				$("#bumn-form").html("");
			} else {
				alert("User Baru gagal ditambahkan...");
			}
		},
	});
}

function delete_dataUser(username) {
	swal({
		title: "Yakin Menghapus User dengan username " + username + " ?",
		text: "Jika dihapus, Tidak bisa dikembalikan lagi!",
		icon: "warning",
		buttons: true,
		dangerMode: true,
	}).then((willDelete) => {
		if (willDelete) {
			$.ajax({
				url: base_url + "/ayoexport/user_management/delete_data_user",
				type: "post",
				data: { username: username },
				success: function (response) {
					var data = JSON.parse(response);
					if (data) {
						swal("Success! User telah dihapus!", {
							icon: "success",
						});
					} else {
						swal({
							title: "Error",
							text: "User gagal dihapus!",
							icon: "error",
							button: "Back",
						});
					}
				},
			}).done(function () {
				get_dataTable();
			});
		} else {
			swal("User dengan username " + username + " tidak dihapus!");
		}
	});
}

function get_dataTable() {
	var data;
	var total_response = 0;
	$.ajax({
		url: base_url + "/ayoexport/user_management/get_data_user",
		type: "get",
		success: function (response) {
			total_response = response.length;
			data = JSON.parse(response);
		},
	}).done(function () {
		if (total_response !== 0) {
			table_data.clear().draw();
			table_data.rows.add(data);
			table_data.columns.adjust().draw();
		} else {
			table_data.clear().draw();
		}
	});
}

$(document).ready(function(){
    $('[id^=nohp]').keypress(validateNumber);
});

function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
    } else if ( key < 48 || key > 57 ) {
        return false;
    } else {
    	return true;
    }
};

function update_user_level() {
	var level_name = document.getElementById("user_level").value;
	var level_display = level_name.toLowerCase();
	if (level_name != ""){
		$.ajax({
			type: 'POST',
			url: base_url + "/ayoexport/user_management/add_level",
			data: {
				lev_name: level_name,
				lev_display: level_display,
			},
			success: function () {
				init_level_select()
				document.getElementById("user_level").value = "";
				alert("Level baru berhasil ditambahkan...");
			}
		});
	}
}
