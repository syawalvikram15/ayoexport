var table_data;
var base_url = window.location.origin;
var current_id = 0;
var current_email = 0
var email = 0;
load_data()

function load_data() {
    table_data = $("#data-table").DataTable({
        responsive: true,
        paging: true,
        info: true,
        searching: true,
        order: [
            [1, "desc"]
        ],
        pageLength: 5,
        lengthMenu: [5, 10, 25, 50, 75],
        columns: [{
                mRender: function(data, type, row) {
                    var id = row.id_regulasi;
                    email = "'" + row.email + "'";
                    var cur_status = "'on-proggress'";
                    return (
                        '<button  class="btn btn-xs btn-warning" onclick="show_detail_transaction(' + cur_status + ',' + id + ',' + email + ')" data-toggle="modal" data-target="#modal-edit" title="Edit"><i class="fa fa-eye"></i></button>'
                    );
                },
                title: "Action",
            },
            {
                data: "id_regulasi",
                title: "Id Transaksi"
            },
            {
                data: "email",
                title: "Email"
            },
            {
                data: "nama",
                title: "Nama Perusahaan"
            },
            {
                data: "nama_produk",
                title: "Nama produk"
            },
            {
                data: "asal_produk",
                title: "Asal Produk"
            },
            {
                data: "tujuan_pengiriman",
                title: "Tujuan Pengiriman"
            },
            {
                data: "tanggal_pengiriman",
                title: "Tanggal"
            }
        ],
        processing: true,
    });

    table_data2 = $("#data-table2").DataTable({
        responsive: true,
        paging: true,
        info: true,
        searching: true,
        order: [
            [1, "desc"]
        ],
        pageLength: 5,
        lengthMenu: [5, 10, 25, 50, 75],
        columns: [{
                mRender: function(data, type, row) {
                    var id = row.id_regulasi;
                    email = "'" + row.email + "'";
                    var cur_status = "'done'";
                    return (

                        '<button  class="btn btn-xs btn-warning" onclick="show_detail_transaction(' + cur_status + ',' + id + ',' + email + ')" data-toggle="modal" data-target="#modal-edit" title="Edit"><i class="fa fa-eye"></i></button>'
                    );
                },
                title: "Action",
            },
            {
                data: "id_regulasi",
                title: "Id Transaksi"
            },
            {
                data: "email",
                title: "Email"
            },
            {
                data: "nama",
                title: "Nama Perusahaan"
            },
            {
                data: "nama_produk",
                title: "Nama produk"
            },
            {
                data: "asal_produk",
                title: "Asal Produk"
            },
            {
                data: "tujuan_pengiriman",
                title: "Tujuan Pengiriman"
            },
            {
                data: "tanggal_pengiriman",
                title: "Tanggal"
            },
        ],
        processing: true,
    });

    table_data3 = $("#data-table3").DataTable({
        responsive: true,
        paging: true,
        info: true,
        searching: true,
        order: [
            [1, "desc"]
        ],
        pageLength: 5,
        lengthMenu: [5, 10, 25, 50, 75],
        columns: [{
                mRender: function(data, type, row) {
                    var id = row.id_regulasi;
                    email = "'" + row.email + "'";
                    var cur_status = "'reject'";
                    return (
                        '<button  class="btn btn-xs btn-warning" onclick="show_detail_transaction(' + cur_status + ',' + id + ',' + email + ')" data-toggle="modal" data-target="#modal-edit" title="Edit"><i class="fa fa-eye"></i></button>'
                    );
                },
                title: "Action",
            },
            {
                data: "id_regulasi",
                title: "Id Transaksi"
            },
            {
                data: "email",
                title: "Email"
            },
            {
                data: "nama",
                title: "Nama Perusahaan"
            },
            {
                data: "nama_produk",
                title: "Nama produk"
            },
            {
                data: "asal_produk",
                title: "Asal Produk"
            },
            {
                data: "tujuan_pengiriman",
                title: "Tujuan Pengiriman"
            },
            {
                data: "tanggal_pengiriman",
                title: "Tanggal"
            },
        ],
        processing: true,
    });
    get_dataTable();
};



function get_dataTable() {
    var data;
    var total_response = 0;
    var data2;
    var total_response2 = 0;
    var data3;
    var total_response3 = 0;
    $.ajax({
        url: base_url + "/ayoexport/regulasi/get_transaction_regulasi",
        type: "get",
        beforeSend: function() {
            // Show LOADING icon
            $("#loader").show();
        },
        success: function(response) {
            total_response = response.length;
            data = JSON.parse(response);
        },
        complete: function() {
            // Show LOADING icon
            $("#loader").hide();
        },
    }).done(function() {
        if (total_response !== 0) {
            table_data.clear().draw();
            table_data.rows.add(data);
            table_data.columns.adjust().draw();
        } else {
            table_data.clear().draw();
        }
    });
    ///////
    $.ajax({
        url: base_url + "/ayoexport/regulasi/get_transaction_regulasi_done",
        type: "get",
        success: function(response) {
            total_response2 = response.length;
            data2 = JSON.parse(response);
        }
    }).done(function() {
        if (total_response2 !== 0) {
            table_data2.clear().draw();
            table_data2.rows.add(data2);
            table_data2.columns.adjust().draw();
        } else {
            table_data2.clear().draw();
        }
    });
    ////
    $.ajax({
        url: base_url + "/ayoexport/regulasi/get_transaction_regulasi_reject",
        type: "get",
        success: function(response) {
            total_response3 = response.length;
            data3 = JSON.parse(response);
        }
    }).done(function() {
        if (total_response3 !== 0) {
            table_data3.clear().draw();
            table_data3.rows.add(data3);
            table_data3.columns.adjust().draw();
        } else {
            table_data3.clear().draw();
        }
    });
}

function show_detail_transaction(cur_status, id, email) {
    current_id = id;
    // console.log(current_id)
    current_email = email;

    if (cur_status == 'done' || cur_status == 'reject') {
        var divOne = document.getElementById('modal-footer');
        divOne.style.display = 'none';
    } else {
        var divOne = document.getElementById('modal-footer');
        divOne.style.display = 'block';
    }

    $('#show_detail').modal('show');
    $.ajax({
        url: base_url + "/ayoexport/regulasi/get_detail_regulasi",
        type: "POST",
        data: {
            id: id
        },
        beforeSend: function() {
            // Show LOADING icon
            $("#loader").show();
        },
        success: function(response) {
            data = JSON.parse(response);
            for (i of data) {
                $("#detail_nama").text(i.nama);
                $("#detail_nama_produk").text(i.nama_produk);
                $("#detail_jumlah_produk").text(i.jumlah_produk);
                $("#detail_asal_produk").text(i.asal_produk);
                $("#detail_tujuan_pengiriman").text(i.tujuan_pengiriman);
                $("#detail_tanggal_pengiriman").text(i.tanggal_pengiriman);
            }
        },
        complete: function() {
            // Show LOADING icon
            $("#loader").hide();
        },
    })
}

function transaction_action(status) {
    var action = status;
    if (action == 'done') {
        var respon = "Transaksi anda telah kami tinjau dan sesuai dengan ekspektasi kami. Kami akan menghubungi anda melalui telvon untuk informasi lebih lanjut";
        var action_message = 'disetujui';
        var message = "Transaksi Regulasi dengan id " + current_id + " berhasil di " + action_message;
    } else if (action == 'offer') {
        var respon = document.getElementById("input_offer").value;
        var message = "Penawaran anda berhasil dikirim";
    } else if (action == 'reject') {
        var respon = document.getElementById("input_reject").value;
        var action_message = 'ditolak';
        var message = "Transaksi Regulasi dengan id " + current_id + " berhasil di " + action_message;
    }

    send_respond(respon, action, current_id);
    

    $.ajax({
        url: base_url + "/ayoexport/regulasi/get_action_regulasi",
        type: "POST",
        data: {
            action: action,
            id: current_id
        },
        beforeSend: function() {
            // Show LOADING icon
            $("#loader").show();
        },
        success: function(response) {

            document.getElementById('close-modal-detail').click();
            swal("Selamat", message, "success");

        },
        complete: function() {
            // Show LOADING icon
            $("#loader").hide();
        },
    })
}

function send_respond(respon, status, current_id) {

    $.ajax({
        url: base_url + "/ayoexport/regulasi/send_respond_regulasi",
        type: "POST",
        data: {
            id_user: current_email,
            respon: respon,
            status: status,
            id_regulasi: current_id
        },
        success: function(response) {
           
        },
        complete: function() {
            setInterval(function(){
                 window.location.reload()
               
            },2000);
            
        }
    })

}


get_notification()

function get_notification() {
    $.ajax({
        url: base_url + "/ayoexport/regulasi/get_respon_regulasi_admin",
        type: "POST",
        success: function(response) {
            data = JSON.parse(response);
         
            if (response == '[]') {
                var html = ['<div class="row">',
                    '<div class="col-md-12">',
                    '<div style="custom-panel">',
                    '<p class="text-center img-restrict">',
                    '<img src="http://shyamtha.com.np/admin/assets/nodatafound.png"   height="350"> ',
                    '</p>',
                  
                    '</div>',
                    '</div>',
                    '</div>'
                ].join("\n");

                $("#notification-content").append(html);
            } else {
                for (i of data) {

                    var id_regulasi_respon = i.id_regulasi;
                    var id_respon = i.id_respon;
                    var id_user = i.id_user;
                    var status_current = i.status;
                    var status_respon = i.respon;
                    var date_notif = i.date_accepted;
                    var status_accepted = i.accepted;

                    if (status_accepted != null) {
                        if (status_accepted == 'accept') {
                            var color = 'green';
                            var status_notif = "OFFER ACCEPTED";
                            var message_respon = status_respon;
                            var message_respon_detail = "'" + status_respon + "'";
                            var link_respon = '#2';
                            var button_detail = '<button class="button__bluesmall" onClick="javascript:show_detail_offer(' + id_respon + ',' + message_respon_detail + ')"> Detail </button>';
                        } else if (status_accepted == 'reject') {
                            var color = 'red';
                            var status_notif = "OFFER REJECTED";
                            var message_respon = status_respon;
                            var message_respon_detail = "'" + status_respon + "'";
                            var link_respon = '#3';
                            var button_detail = '<button class="button__bluesmall" onClick="javascript:show_detail_offer(' + id_respon + ',' + message_respon_detail + ')"> Detail </button>';
                        }



                        var html = ['<div class="row border-notif">',
                            '<div class="col-md-3 p-0">',
                            '<div class="date-notification" style="background-color: #45619f;padding: 15px;color: #fff;border-bottom-left-radius: 10px;border-top-left-radius: 10px;">',
                            '<span>' + date_notif + '</span>',
                            '</div>',
                            '</div>',
                            '<div class="col-md-2">',
                            '<div class="body-notif" style="font-weight:bold;text-align: center; color:' + color + ';">',
                            '<p class="p-0 text-status-notif">' + status_notif + '</p>',
                            '</div>',
                            '</div>',
                            '<div class="col-md-5">',
                            '<div class="body-notif">',
                            '<a>' + id_user + '</a>',
                            '</div>',
                            '</div>',
                            '<div class="col-md-2">',
                            '<div class="detail-notif">' + button_detail +
                            '</div>',
                            '</div>',
                            '</div>'
                        ].join("\n");

                        $("#notification-admin").append(html);
                    }
                }
            }
        },
    })

}

var current_id_offer = 0;
function show_detail_offer(id_respon, message_respon) {
    current_id_offer = id_respon;
    $('#show_detail_offer').modal('show');
    $("#text-detail-offer").text(message_respon);
}