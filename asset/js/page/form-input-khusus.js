var data;
var table_data;

$(document).ready(function() {
    table_data = $('#data-table').DataTable({
		responsive: true,		
		"paging":   true,
		"info":     false,
		"order": [[ 0, "desc" ]],
        'pageLength' : 5,
        'lengthMenu' : [5,10,25,50,75],
		searching: true,
		columns:[
			{data: 'mobile_number', title: 'HP'},
			{data: 'full_name',     title: 'Nama'},
			{data: 'status',        title: 'Status'},
			{data: 'mac_address',   title: 'MAC Address'},
            
		],
		processing: true,			
    } );
    get_dataTable()

    initialSubmit()
})

 $("#submit-search").click(function(e){
    e.preventDefault();
    initialSubmit();
 });

 function initialSubmit() {
    var disabled =$('#submit-search').hasClass('disabled');
    if(!disabled){
        nohp = $("#search-nohp").val();
        if(nohp.substring(0, 1) === "8"){ nohp = nohp.replace("8", "+628")}
        if(nohp.substring(0, 2) === "08"){ nohp = nohp.replace("08", "+628")}
        if(nohp.substring(0, 3) === "628"){ nohp = nohp.replace("628", "+628")}
        handleNohp(nohp)
    }
 }


const handleNohp = (mobile_number) => {
    $("#loading").html("<img src='"+BASE_URL+"asset/plugins/images/busy.gif' width='12' /> Loading...");
    $.ajax({
        url  : BASE_URL+"api/form-input-khusus/get-nohp",
        type : 'post',
        data: {mobile_number : mobile_number},
        success: function (response, textStatus, xhr) {
            data = JSON.parse(response);                        
            $("#nohp").val(data.mobile_number)
            $("#nama").val(data.full_name)            
            $("#mac-address").val(data.mac_address)
            
            var selectedP = "", selectedNP= "", selectedODP="", selectedPDP="";
            if(data.status === "POSITIVE"){     selectedP = "selected" }
            if(data.status === "NOT POSITIVE"){ selectedNP = "selected" }
            if(data.status === "PDP"){          selectedPDP = "selected" }
            if(data.status === "ODP"){          selectedODP = "selected" }
            option = "   <option value=''>Select Status</option>"+
                        "<option value='POSITIVE'       "+selectedP+">POSITIVE</option>"+
                        "<option value='NOT POSITIVE'   "+selectedNP+">NOT POSITIVE</option>"+
                        "<option value='PDP'            "+selectedPDP+">PDP</option>"+
                        "<option value='ODP'            "+selectedODP+">ODP</option>";
            $("#status").html(option)

            $("#loading").text("Submit");
        }
    });
}

$("#update").click(function(e){
    e.preventDefault();

    var disabled =$('#update').hasClass('disabled');
    if(!disabled){
        
        var nohp = $("#nohp").val()
        var full_name = $("#nama").val()
        var status = $("#status").val()
        var mac_address = $("#mac-address").val()        
        cekDataTabelUpdate(nohp, status, mac_address)             
    }
 });

 function cekDataTabelUpdate(mobile_number, status, mac_address){
    $("#loading-update").html("<img src='"+BASE_URL+"asset/plugins/images/busy.gif' width='12' /> Loading...");
     $.ajax({
        url  : BASE_URL+"api/form-input-khusus/cek-data-tabel-update",
        type : 'post',
        data: {mobile_number : mobile_number},
        success: function (response, textStatus, xhr) {
            res = JSON.parse(response);
            console.log("cek data table update =>", res)

            if(res){                
                 swal({
                    title: "Apakah anda yakin untuk update data?",
                    text: "Data dengan no "+mobile_number+ " sudah ada",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                  })
                  .then((willUpdate) => {
                    if (willUpdate) {
                        handleUpdate(status, mac_address)  
                      ;
                    } else {
                      swal("Dibatalkan!");
                    }
                  });
            }else{                
                handleUpdate(status, mac_address)                
            }              
        }
     })
 }

 function handleUpdate(status, mac_address){    
    data.status = status
    data.mac_address = mac_address
    $.ajax({
        url  : BASE_URL+"api/form-input-khusus/update",
        type : 'post',
        data: {data : data},
        success: function (response, textStatus, xhr) {             
            data = JSON.parse(response);
            if(data){
                 swal("Selamat!","Data berhasil di update...","success")
                $("#loading-update").text("Update");
                initialSubmit();
                get_dataTable()
            }else{
                alert('Data gagal di update...')
            }
        }
     })
 }



 function get_dataTable(){	
	var data;
	var total_response = 0;
	$.ajax({
        url  : BASE_URL+"api/form-input-khusus/get-data-table",
        type : 'get',
        success: function (response) { 
			total_response = response.length;
			data = JSON.parse(response);
        }
    }).done(function(){
        if(total_response !== 0)
        {
            table_data.clear().draw();
            table_data.rows.add(data);
            table_data.columns.adjust().draw();            
        }else{
            table_data.clear().draw();
        }
    });
}